#ifndef NTAU_MULTIPADCANVAS__H
#define NTAU_MULTIPADCANVAS__H

#include "TCanvas.h"
#include "TPad.h"
#include <memory> 
#include <vector> 

/// The MultiPadCanvas is a helper class used in drawing operations.
/// It wraps a canvas and an aribtrary number of sub-pads within the canvas. 
class MultiPadCanvas{
public:
    /// empty multi pad canvas - not very interesting 
    MultiPadCanvas() = default;    
    /// Initializes a multi pad canvas from a TCanvas
    /// and a vector of nested pads. 
    MultiPadCanvas(       TCanvas* c, 
                    const std::vector<TPad*>& pads);

    /// Same as above, but using shared pointers. 
    MultiPadCanvas(       std::shared_ptr<TCanvas> c, 
                    const std::vector< std::shared_ptr<TPad>>& pads);
    
    /// Get the main canvas
    std::shared_ptr<TCanvas> getCanvas()      const;
    /// Get a specific pad
    /// @arg p: Index in the vector 
    std::shared_ptr<TPad>    getPad(size_t p) const;

private:
    std::shared_ptr<TCanvas>            m_can {nullptr}; 
    std::vector<std::shared_ptr<TPad>>  m_pads{}; 
};



#endif  //  NTAU_MULTIPADCANVAS__H