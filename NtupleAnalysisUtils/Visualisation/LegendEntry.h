#ifndef NTAU__LEGENDENTRY__H
#define NTAU__LEGENDENTRY__H

#include <string> 
#include "TObject.h"
#include "NtupleAnalysisUtils/Plot/Plot.h"

/// This header defines an extremely primitive 
/// struct to wrap an entry of a TLegend. 
/// Intended to be a more lightweight substitute
/// for a full-fledged Plot object
 
struct LegendEntry{
    LegendEntry(      TObject* o, 
                const std::string & title , 
                const std::string & opt);
    template <class H> LegendEntry (const Plot<H> & p);
    TObject*    obj     {nullptr};
    std::string title   {""}; 
    std::string drawOpt {""};
};

#include "LegendEntry.ixx"

#endif //  NTAU__LEGENDENTRY__H