#ifndef NTAU__MULTIPAGEPDF__HANDLE__H
#define NTAU__MULTIPAGEPDF__HANDLE__H

/// This is used for creating multi-page pdf files

#include <string> 
#include "TCanvas.h"

/// A helper struct representing a handle to a multi-page PDF. 
/// Creating it via startMultiPagePdfFile will open the file, 
/// you can save canvases to the file using "saveCanvas",
/// and the file will be correctly closed when the handle goes
/// out of scope. Used to easily generate multi-page documents
///
/// Usually, the user will *not* manually instantiate this,
/// but use the PlotUtils::startMultiPagePdfFile method 
/// defined in the IOFunctions.h header instead. 

class MultiPagePdfHandle{
    public: 
        /// Create, with the desired file name.
        /// Note that this name will not be further modified,
        /// for "clever" name extension with folders etc, 
        /// please use the PlotUtils methods
        MultiPagePdfHandle(const std::string & fname); 
        void saveCanvas(TCanvas* can) const; 
        void saveCanvas(std::shared_ptr<TCanvas> can) const; 
        ~MultiPagePdfHandle(); 
    protected: 
        std::string m_fname; 
};



#endif