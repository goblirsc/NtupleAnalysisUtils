
template <class H> LegendEntry::LegendEntry (const Plot<H> & p){
    obj = p();
    title = p.plotFormat().LegendTitle(); 
    drawOpt = p.plotFormat().LegendOption(); 
}
