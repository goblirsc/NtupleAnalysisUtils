#ifndef NTAU_Visualisation__INCLUDES
#define NTAU_Visualisation__INCLUDES

/// Top level header for the headers within the Visualisation part of NTAU

#include "NtupleAnalysisUtils/Visualisation/AtlasStyle.h"
#include "NtupleAnalysisUtils/Visualisation/DefaultPlotting.h"

#endif