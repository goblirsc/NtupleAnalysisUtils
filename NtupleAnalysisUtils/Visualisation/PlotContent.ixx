
template <class H> PlotContent<H>::PlotContent(
            const std::vector<Plot<H>> & plots,
            const std::vector<std::string> & labels,
            const std::string & fname,
            std::shared_ptr<MultiPagePdfHandle> multiPagePdfHandle,
            const CanvasOptions & opts):
    m_plots(plots),
    m_labels(labels),
    m_fname(fname),
    m_multiPagePdfHandle(multiPagePdfHandle),
    m_canvasOpts(opts){
        /// in this case, we build a default 
        /// set of ratios, comparing each
        /// plot to the first of the list. 
        buildDefaultRatios();
}        

template <class H> PlotContent<H>::PlotContent(
            const std::vector<Plot<H>> & plots,
            const std::vector<RatioEntry> & ratios,
            const std::vector<std::string> & labels,
            const std::string & fname,
            std::shared_ptr<MultiPagePdfHandle> multiPagePdfHandle,
            const CanvasOptions & opts):
    m_plots(plots),
    m_ratios(ratios),
    m_labels(labels),
    m_fname(fname),
    m_multiPagePdfHandle(multiPagePdfHandle),
    m_canvasOpts(opts){
}

template <class H> Plot<H> & PlotContent<H>::getPlot(size_t i) {
    return m_plots.at(i);
}
template <class H> std::vector<Plot<H>> & PlotContent<H>::getPlots(){
    return m_plots;
}
template <class H> size_t PlotContent<H>::getNplots() const {
    return m_plots.size();
}
template <class H> std::vector<RatioEntry> & PlotContent<H>::getRatioEntries(){
    return m_ratios;
}

template <class H> void PlotContent<H>::buildDefaultRatios(){
    for (size_t num = 1; num < m_plots.size(); ++num){
        m_ratios.push_back(RatioEntry(num, 0, PlotUtils::defaultErrors));
    }
}
template <class H> std::vector<std::string> & PlotContent<H>::getLabels(){
    return m_labels;
}
template <class H> std::string PlotContent<H>::getFileName() const{
    return m_fname;
}
template <class H> void PlotContent<H>::setFileName(const std::string & fname){
    m_fname = fname;
}
template <class H> std::shared_ptr<MultiPagePdfHandle> PlotContent<H>::getMultiPagePdfHandle() const {
    return m_multiPagePdfHandle;
}
template <class H> void PlotContent<H>::setMultiPagePdfHandle(std::shared_ptr<MultiPagePdfHandle> theHandle) {
    m_multiPagePdfHandle = theHandle;
}
template <class H> const CanvasOptions  & PlotContent<H>::getCanvasOptions() const {
    return m_canvasOpts;
}
template <class H> CanvasOptions  & PlotContent<H>::getCanvasOptions() {
    return m_canvasOpts;
}
template <class H> void PlotContent<H>::setCanvasOptions(CanvasOptions opt) {
    m_canvasOpts = opt;
}

template <class H> void PlotContent<H>::populateAll(bool doApplyFormat){
    if (!m_isPopulated){
        for (auto & p : m_plots) {
            p.populate(doApplyFormat);
        }
        m_isPopulated = true;
    }
}

template <class H> std::vector<Plot<ratioType<H>>> PlotContent<H>::getRatios(){
    std::vector<Plot<ratioType<H>>> ratios; 
    for (auto & r : getRatioEntries()){
        ratios.push_back(r.getRatio(*this));
        // also apply the numerator formatting by default, if no specific format has been specified
        ratios.back().setPlotFormat(r.plotFormat().value_or(m_plots.at(r.getNumIndex()).plotFormat()));
        ratios.back().applyFormat();
    }
    return ratios;
}

template <class In> Plot<ratioType<In>> RatioEntry::getRatio(PlotContent<In> & pc) const {
    if (m_num < pc.getNplots() && m_den < pc.getNplots()){
        return PlotUtils::getRatio(pc.getPlot(m_num), pc.getPlot(m_den), m_errorStrat);
    }
    else {
        std::cerr <<" Requested entries "<<m_num<<" and "<<m_den<<" for a ratio, while the PlotContent only has "<<pc.getNplots()<<" entries!"<<std::endl;
        return Plot<ratioType<In>>();
    }
}

template <class H> std::vector<float> PlotContent<H>::getQualityTests(std::string testname){
    std::vector<float> probs;
    for (auto & r : getRatioEntries()){
        probs.push_back(r.getQualityTest(*this, testname));
    }
    return probs;
}

template <class In> float RatioEntry::getQualityTest(PlotContent<In> & pc, std::string testname) const { 
    if (m_num < pc.getNplots() && m_den < pc.getNplots()){
        return PlotUtils::getQualityTest(pc.getPlot(m_num), pc.getPlot(m_den), testname);
    }
    else {
        std::cerr << "Requested entries "<<m_num<<" and "<<m_den<<" for quality test, while the PlotContent only has "<<pc.getNplots()<<" entries!"<<std::endl;
	return 0;
    }
} 
