#pragma once
#include <mutex> 
#include <iostream> 
#include <sstream> 
#include <atomic> 
#include <sys/syscall.h>

namespace NTAU_DBG{
    static std::mutex g_mx_NTAU_DEBUG; 
}

#define NTAU_DEBUG_PRINT(args) \
    do {\
        std::lock_guard g (NTAU_DBG::g_mx_NTAU_DEBUG);  \
        pid_t thread = syscall(SYS_gettid); \
        std::stringstream str; \
        str << __FILE__<<":"<<__LINE__<<" @ thread "<<thread<<": " << args; \
        std::cout << str.str()<<std::endl; \
    } while (false);  

#define NTAU_DEBUG_LINE \
do {\
    std::lock_guard g (NTAU_DBG::g_mx_NTAU_DEBUG); \
    pid_t thread = syscall(SYS_gettid);\
    std::stringstream str; \
    str << __FILE__<<":"<<__LINE__<<" @ thread "<<thread; \
    std::cout << str.str()<<std::endl; \
} while (false);  

