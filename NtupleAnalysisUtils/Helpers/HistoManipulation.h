#ifndef NTAU__HISTOMANIPULATION__H
#define NTAU__HISTOMANIPULATION__H

#include <memory> 

#include "TH1.h"
#include <TH3D.h>
#include "TEfficiency.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "NtupleAnalysisUtils/Plot/Plot.h"
#include "NtupleAnalysisUtils/Helpers/RatioTypes.h"
#include "NtupleAnalysisUtils/Helpers/Concepts.h"
#include "TGraphSmooth.h"

/// This header declares a set of helper methods which manipulate or convert histograms. 
/// Used extensively in the remaining tools of the package. 



namespace PlotUtils{

  /// This helper enum provides a few possible error definitions 
  /// for ratios. 
   typedef enum {
        defaultErrors = 0,          /// simple error propagation
        efficiencyErrors = 1,       /// binomial efficiency errors, assume num and den correlated
        numeratorOnlyErrors=2,      /// propagate only error on numerator
        denominatorOnlyErrors=3     /// propagate only error on denominator 
    } EfficiencyMode;


    /// normalises histogram bin contents to target_width. 
    /// If negative, normalise to first bin width
    /// Method returns the bin width that is used for normalisation 
    /// Histogram is modified in-place.
    double normaliseToBinWidth(TH1* h, double target_width = -1);

    /// normalise the passed input histogram to a target integral. 
    /// @arg target_integral: Integral after normalisation
    /// @arg multiplyByWidht: Multiply each bin by its width during integration 
    /// @arg includeOverflow: include or exclude under- and overflow bins 
    /// Histogram is modified in-place.
    void normaliseToIntegral(TH1* h, double target_integral = 1, 
                            bool multiplyByWidth=false, 
                            bool includeOverflow=false);

    /// Shifts the under/overflows into the first / last bins. 
    /// Histogram is modified in-place
    void shiftOverflows(TH1* h);

    /// Returns whether the global bin in a histogram is in the overflow or underflow
    bool isOverflowBin(const TH1 *Histo, int bin);
    /// Returns whether the bin number is zero or greater than the number of bins along this axis;
    bool isOverflowBin(const TAxis *a, int bin);

    /// Returns the total number of bins of a histogram
    ///  i.e. per dimension GetNbins() + 2
    unsigned int getNbins(const TH1 *Histo);
    
    
    /// Returns whether alpha numeric bin-labels have been set
    bool isAlphaNumeric(const TH1 *histo);
    bool isAlphaNumeric(const TAxis* axis);
    
    /// Integrate a 1D histogram into a 1D running integral. 
    /// Each bin content is equal to the integral from the first to
    /// the current bin in the original histogram. 
    /// Returns a new histogram, leaving the original invariant. 
    TH1* toCDF (const TH1* differential); 

    /// This will convert a "data" histogram to a TGraphAsymmErrors with approximate 
    /// Poisson error bands. Useful for low-stat data/MC plots. 
    TGraphAsymmErrors* toTGAE(const TH1* h_data); 

    /// =====================================================
    /// Methods for ratio calculation 
    /// =====================================================

    /// These methods allow to obtain a Ratio of two histograms. 
    /// The errmode argument determines how the errors are calculated.
    /// The return type depends on the input. 
    /// Usually, the same type as the input numerator is returned. 
    /// Exceptions exist for TEfficiency ratios, which resolve to TH1, 
    /// as well as for TProfile ratios, which resolve to plain histograms of the same dimension.
    /// First, a fallback for unsupported types. Specialisation for supported types below. 
    /// This fallback will try to perform a TH1 ratio if both args are convertible, 
    /// otherwise it will return a null pointer. 
    template <class T> requires (!std::is_base_of<TH1,T>::value) && (!populatable<T>)
      inline ratioType<T>* getRatio(const T* num, const T* den, EfficiencyMode errors = defaultErrors);

    /// Specialisation for ratios between TH1-alikes
    template <typename TH1type> requires (std::is_base_of<TH1,TH1type>::value ) 
    inline ratioType<TH1type>* getRatio(const TH1type* num, const TH1type* den, EfficiencyMode errmode = defaultErrors);

    /// Template for working with Plot objects
    /// Note that it is likely cleaner to instead use a populator to obtain the ratio! 
    template <typename H> inline Plot<ratioType<H>> getRatio(Plot<H> & num, Plot<H> & den, EfficiencyMode errmode = defaultErrors);

    /// Specialisations for ratios between efficiencies and profiles 
    template <>  inline ratioType<TEfficiency>* getRatio<TEfficiency>(const TEfficiency* num, const TEfficiency* den, EfficiencyMode errmode);
    template <>  inline ratioType<TProfile>* getRatio<TProfile>(const TProfile* num, const TProfile* den, EfficiencyMode errmode);
    template <>  inline ratioType<TProfile2D>* getRatio<TProfile2D>(const TProfile2D* num, const TProfile2D* den, EfficiencyMode errmode);


    /// Specialisation for ratios between graph-alikes
    template <>  inline ratioType<TGraph>* getRatio<TGraph>(const TGraph* num, const TGraph* den, EfficiencyMode errmode);
    template <>  inline ratioType<TGraphErrors>* getRatio<TGraphErrors>(const TGraphErrors* num, const TGraphErrors* den, EfficiencyMode errmode);
    template <>  inline ratioType<TGraphAsymmErrors>* getRatio<TGraphAsymmErrors>(const TGraphAsymmErrors* num, const TGraphAsymmErrors* den, EfficiencyMode errmode);

    /// helper to implement the graph ratios 
    void helpGraphRatio_central(TGraph* ratio, const TGraph* num, const TGraph* den); 

    // find closest neighbour points on the left and right of an X value in a sorted graph
    std::pair<int, int> findNeighbours(const TGraph* g, double x);
    // linear interpolation
    double linearInterpolate(double xTarget, double x1, double x2, double y1, double y2);

    template <class GraphType> void helpGraphRatio_errors(ratioType<GraphType>* ratio, const GraphType* num, const GraphType* den,EfficiencyMode e=defaultErrors); 

    void populateRatio(TH1* ratio, const TH1* num, const TH1* den,  PlotUtils::EfficiencyMode errmode);

    /// helper for the ratio between two point values 
    double calcRatioError(double num, double den, double dNum, double dDen, PlotUtils::EfficiencyMode errMode); 


    /// This will generate a TH1D filled with the efficiency returned by the 
    /// TEfficiency argument. Histogram errors correspond to the average
    /// of the up- and down-errors reported by the TEfficiency. 
    std::shared_ptr<TH1D> extractEfficiency(const TEfficiency* eff); 

    /// This will convert a sorted graph to a TH1D, using a variable binning. 
    /// The first and last bins will by symmetrically extended around their center (the graph X location)
    /// empty graphs lead to an empty hist, and single-point graphs to a single-bin hist
    std::shared_ptr<TH1D> graphToHist(std::shared_ptr<TGraph> graph); 

    /// =====================================================
    /// Methods for Chi2/KS probability calculation 
    /// =====================================================
    /// These methods allow to obtain Chi2/KS probability of two histograms. 
    /// A check determining how similar two histograms are in terms of underlying distribution and shape.
    /// Returns the p-value
    /// p-value < 0.05/0.1 ==>> very unlikely that two histograms comes from same underlying distribution

    template <typename H> float getQualityTest(Plot<H> & num, Plot<H> & den, std::string testname);

    float getQualityTest(const TEfficiency* hother, const TEfficiency* href, std::string testname);

    float getQualityTest(const TProfile* hother, const TProfile* href, std::string testname);

    float getQualityTest(const TH1* hother, const TH1* href, std::string testname);
    
    float Chi2Test(const TH1* h1, const TH1* h2, const char* mode = "");
    float KSTest(const TH1* h1, const TH1* h2, const char* mode = "");

    /// TGraph-related helpers 
    template <class G> void setPointError(G* g, int i, double ex, double ey);
    template <> void setPointError<TGraphErrors>(TGraphErrors* g, int i, double ex, double ey);
    template <> void setPointError<TGraphAsymmErrors>(TGraphAsymmErrors* g, int i, double ex, double ey);

    /// Run SuperSmoother 

    /// @brief Catch-all case for unsupported types
    /// @tparam T: Type of the object
    /// @param theHist: histo to smooth
    /// @param smoothness: Smoothness flag of the smoother. 0...10, 10 is maximum smoothness
    /// @param span: Span flag of the smoother. ROOT recommends for small (n < 40) samples to use a span > 0, between 0.2 and 0.4  
    /// @param isPeriodic: periodic flag for the smoother. If set, the x values are assumed to be in [0, 1] and of period 1.
    /// @return smoothed object (or the original if unsupported)
    template <class T, typename std::enable_if<!std::is_base_of<TH1,T>::value, int>::type aux = 2 >  
      T* runSuperSmoother(const T* theHist, 
                          double smoothness= 1,
                        double span = 0,
                        bool isPeriodic = false);
    /// Specialisation for  TH1-alikes
    template <typename TH1type, typename std::enable_if<std::is_base_of<TH1,TH1type>::value, int>::type aux = 2>
    TH1type* runSuperSmoother(const TH1type* theHist, 
                        double smoothness= 1,
                        double span = 0,
                        bool isPeriodic = false);

    /// Specialisation for  graph-alikes
    TGraph* runSuperSmoother(const TGraph* theHist, 
                        double smoothness= 1,
                        double span = 0,
                        bool isPeriodic = false);

    template <typename htype> std::shared_ptr<htype> CloneHist(std::shared_ptr<htype> in, const std::string & altName = "");

    /// Returns a single-bin iterative RMS. 
    /// The input histogram represents the distribution of pulls / residuals within a given bin. 
    std::pair<double,double> getIterRMS1D(std::shared_ptr<TH1D> input); 

    /// returns a 1D width histogram using iterative RMS. 
    /// The input histogram's x axis is retained, the y axis is assumed to contain the pulls / residuals 
    std::shared_ptr<TH1D> getIterRMS(std::shared_ptr<TH2D> input); 

    /// returns a 2D width histogram using iterative RMS. 
    /// The input histogram's x and y axes are retained, the z axis is assumed to contain the pulls / residuals 
    std::shared_ptr<TH2D> getIterRMS2D(std::shared_ptr<TH3D> input); 

    /// Returns a single-bin full RMS. 
    /// The input histogram represents the distribution of pulls / residuals within a given bin. 
    std::pair<double,double> getRMS1D(std::shared_ptr<TH1D> input); 

    /// returns a 1D width histogram using full RMS. 
    /// The input histogram's x axis is retained, the y axis is assumed to contain the pulls / residuals 
    std::shared_ptr<TH1D> getRMS(std::shared_ptr<TH2D> input); 

    /// returns a 2D width histogram using full RMS. 
    /// The input histogram's x and y axes are retained, the z axis is assumed to contain the pulls / residuals 
    std::shared_ptr<TH2D> getRMS2D(std::shared_ptr<TH3D> input); 
    /// simple profile to histo conversion
    std::shared_ptr<TH1D> profToTH1(std::shared_ptr<TProfile> input); 

    template<typename H> inline  void doSW2(H* ){}
    template <> inline  void doSW2<TH1>(TH1* h){h->Sumw2();}
}
#include "HistoManipulation.ixx"

#endif // NTAU__HISTOMANIPULATION__H
