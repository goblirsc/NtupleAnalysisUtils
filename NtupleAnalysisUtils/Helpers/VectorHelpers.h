#ifndef NTAU__VECTOR__HELPERS__H
#define NTAU__VECTOR__HELPERS__H

/// This headers adds a few operators for std::vectors that
/// a minion considered useful. 
/// A happy minion is a good minion, so we keep them! 


/// Print a vector into std::cout
template <class T> std::ostream &operator<<(std::ostream &os, const std::vector<T> &v);

/// push-back to a vector using a += operator
template <class T> std::vector<T>& operator+=(std::vector<T>& vec, const T& ele);

/// also concatenate two vectors using a += operator 
template<class T> std::vector<T>& operator+=(std::vector<T>& vec, const std::vector<T>& new_cont);

/// push-back into a vector-copy using a + operator
template <class T> std::vector<T> operator+(const std::vector<T>& vec, const T& ele);

/// concatenate into a vector-copy using a + operator. Ordering within the output corresponds
/// to operand order. 
template <class T> std::vector<T> operator+(const std::vector<T>& a, const std::vector<T>& b);

#include "VectorHelpers.ixx"

#endif // NTAU__VECTOR__HELPERS__H