#pragma once 

/// A set of concepts and traits that we use in NTAU 

/// concept for an object with a ROOT-style addition method 
template <class T> concept addable = requires (T theThing, T* other){
    theThing.Add(other); 
}; 

/// concept for an object that has SetDirectory 
template <class T> concept withSetDirectory = requires (T theThing){
    theThing.SetDirectory(0); 
}; 

/// concept for an object that has Reset 
template <class T> concept resettable = requires (T theThing){
    theThing.Reset(0); 
}; 
/// concept for an object that looks like a Plot  
template <class T> concept populatable = requires (T theThing){
    theThing.populate(); 
}; 
/// check for a valid constructor arg-set
template <typename targetType, typename... Args> 
concept canConstruct = requires(Args... args){
    new targetType(args...); 
};

#define buildTrait(traitName, conceptPass) \
    template <typename T> struct traitName{}; \
    template <conceptPass T> struct traitName<T>{static constexpr bool value = true;}; \
    template <typename T> requires (!conceptPass <T>) struct traitName<T>{static constexpr bool value = false;};

buildTrait(hasAddMethod, addable)
buildTrait(hasReset, resettable)
buildTrait(hasSetDirectory, withSetDirectory)
buildTrait(hasPopulate, populatable)
