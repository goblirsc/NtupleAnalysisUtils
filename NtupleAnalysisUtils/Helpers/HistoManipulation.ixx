
#include "NtupleAnalysisUtils/Plot/BasicPopulators.h"


template <class T> requires (!std::is_base_of<TH1,T>::value) &&  (!populatable<T>)  inline ratioType<T>*  PlotUtils::getRatio(const T*,  const T* , PlotUtils::EfficiencyMode  ){
    /// enforce a resolution failure for TH1 types 
    std::cerr <<" Sorry, no ratio calculation supported for these histogram types :-( " <<std::endl;
    return nullptr;
}

template <typename TH1type>  requires (std::is_base_of<TH1,TH1type>::value) 
inline ratioType<TH1type>* PlotUtils::getRatio(const TH1type* num, const TH1type* den, EfficiencyMode errmode){
    if (num == 0 || den == 0){
        return 0;
    }
    auto ratio = dynamic_cast<ratioType<TH1type>*> (num->Clone(Form("ratioFrom%s_and_%s",num->GetName(), den->GetName())));
    ratio->SetDirectory(0);
    populateRatio(ratio,num,den,errmode); 
    return ratio;
}

template <typename H> inline Plot<ratioType<H>> PlotUtils::getRatio(Plot<H> & num, Plot<H> & den, PlotUtils::EfficiencyMode errmode){
    /// For Plot objects, we generate a new plot with the format of the old one, and perform 
    /// the ratio calculation using the template methods 
    Plot<ratioType<H>> theRatio(CopyExisting<ratioType<H>>(dynamic_cast<ratioType<H>*>(getRatio(num(),den(),errmode))), num.plotFormat());
    theRatio.populate();
    return theRatio; 
}


template <> inline ratioType<TEfficiency>* PlotUtils::getRatio<TEfficiency>(const TEfficiency* num, const TEfficiency* den, EfficiencyMode errmode){
    std::unique_ptr<TH1> num1 (dynamic_cast<TH1*>(num->GetPassedHistogram()->Clone()));
    std::unique_ptr<TH1> num2 (dynamic_cast<TH1*>(den->GetPassedHistogram()->Clone()));
    std::unique_ptr<TH1> den1 (dynamic_cast<TH1*>(num->GetTotalHistogram()->Clone()));
    std::unique_ptr<TH1> den2 (dynamic_cast<TH1*>(den->GetTotalHistogram()->Clone()));

    std::unique_ptr<TH1> effNum (getRatio(num1.get(), den1.get(), PlotUtils::efficiencyErrors));
    std::unique_ptr<TH1> effDen (getRatio(num2.get(), den2.get(), PlotUtils::efficiencyErrors));
    auto res = getRatio(effNum.get(), effDen.get(), errmode);
    if (den1->Chi2Test(den2.get()) > 0.99){
        std::cout << "getRatio<TEfficiency,TEfficiency>: Assuming correlated denominators due to very large p(chi2)"<<std::endl;
        // assume denoms are correlated - then only the numerators contribute at first order
        auto forErrs = getRatio(num1.get(), num2.get(), PlotUtils::efficiencyErrors); 
        for (int x = 0; x < res->GetNbinsX(); ++x){
            double y1 = forErrs->GetBinContent(x+1);
            double dy1 = forErrs->GetBinError(x+1); 
            double y2 = res->GetBinContent(x+1);
            res->SetBinError(x+1, dy1 * y2 / y1);
        }
    }
    return res;

}

template <> inline ratioType<TProfile>* PlotUtils::getRatio<TProfile>(const TProfile* num, const TProfile* den, EfficiencyMode errmode){
    ratioType<TProfile>* ratio = dynamic_cast<ratioType<TProfile>*>(num->ProjectionX()->Clone());
    ratio->Reset();
    ratio->SetDirectory(0);
    populateRatio(ratio,num,den,errmode); 
    return ratio;
}

template <> inline ratioType<TProfile2D>* PlotUtils::getRatio<TProfile2D>(const TProfile2D* num, const TProfile2D* den, EfficiencyMode errmode){
    ratioType<TProfile2D>* ratio = dynamic_cast<ratioType<TProfile2D>*>(num->ProjectionXY()->Clone());
    ratio->Reset();
    ratio->SetDirectory(0);

    populateRatio(ratio,num,den,errmode); 
    return ratio;

}
template <> inline ratioType<TGraph>* PlotUtils::getRatio<TGraph>(const TGraph* num, const TGraph* den, EfficiencyMode ){
    auto theRatio = new ratioType<TGraph>(); 
    helpGraphRatio_central(theRatio, num,den);  
    return theRatio; 
}

template <> inline ratioType<TGraphErrors>* PlotUtils::getRatio<TGraphErrors>(const TGraphErrors* num, const TGraphErrors* den, EfficiencyMode e){
    auto theRatio = new ratioType<TGraphErrors>;
    helpGraphRatio_central(theRatio, num,den); 
    helpGraphRatio_errors(theRatio, num,den,e); 
    return theRatio; 
}
template <> inline ratioType<TGraphAsymmErrors>* PlotUtils::getRatio<TGraphAsymmErrors>(const TGraphAsymmErrors* num, const TGraphAsymmErrors* den, EfficiencyMode e){
    auto theRatio = new ratioType<TGraphAsymmErrors>; 
    helpGraphRatio_central(theRatio, num,den); 
    helpGraphRatio_errors(theRatio, num,den,e); 
    return theRatio; 
}


template <typename H> float PlotUtils::getQualityTest(Plot<H> & num, Plot<H> & den, std::string testname){
    float prob = getQualityTest(num(), den(), testname);
    return prob;

}
// dummy helpers because ROOT's Graph classes are painful! ;(
template <class G> void PlotUtils::setPointError(G* , int , double , double ){}

template <class GraphType> void PlotUtils::helpGraphRatio_errors(ratioType<GraphType>* ratio, const GraphType* num, const GraphType* den, EfficiencyMode e){
    for (int k = 0; k < ratio->GetN(); ++k){
        double x = ratio->GetX()[k];
        // interpolate errors
        double ynum = num->Eval(x);
        double yden = den->Eval(x);

        auto neighbours_num = findNeighbours(num,x);
        double dNum = linearInterpolate(x, num->GetX()[neighbours_num.first], num->GetX()[neighbours_num.second],num->GetY()[neighbours_num.first], num->GetY()[neighbours_num.second]); 

        auto neighbours_den = findNeighbours(den,x);
        double dDen = linearInterpolate(x, den->GetX()[neighbours_den.first], den->GetX()[neighbours_den.second],den->GetY()[neighbours_den.first], den->GetY()[neighbours_den.second]); 

        setPointError(ratio, k, 0., calcRatioError(ynum, yden, dNum, dDen,e));
    }
}   
template <class T, typename std::enable_if<!std::is_base_of<TH1,T>::value, int>::type aux  >  
    T* PlotUtils::runSuperSmoother(const T* theHist, 
                    double ,
                    double ,
                    bool ){
    std::cerr << "no smoothing is supported so far for the supplied inputs! Will return the original"<<std::endl;
    return theHist; 
}
/// Specialisation for  TH1-alikes
template <typename TH1type, typename std::enable_if<std::is_base_of<TH1,TH1type>::value, int>::type aux >
TH1type* PlotUtils::runSuperSmoother(const TH1type* theHist, 
                    double smoothness,
                    double span,
                    bool isPeriodic){

    TGraph auxGraph(dynamic_cast<const TH1*>(theHist));  
    TH1type* output (dynamic_cast<TH1type*>(theHist->Clone())); 
    TGraphSmooth tgs; 
    auto smooth = tgs.SmoothSuper(&auxGraph,"",smoothness,span,isPeriodic); 

    output->Reset(); 
    for (int bx =1; bx < theHist->GetNbinsX()+1; ++bx){
        output->SetBinContent(bx, smooth->Eval(theHist->GetXaxis()->GetBinCenter(bx))); 
        // retain original bin errors
        output->SetBinError(bx,theHist->GetBinError(bx)); 
    }
    return output; 
}
template <typename htype> std::shared_ptr<htype> PlotUtils::CloneHist(std::shared_ptr<htype> in, const std::string & altName){
    return std::shared_ptr<htype>(dynamic_cast<htype*>(in->Clone(altName.c_str()))); 
}
