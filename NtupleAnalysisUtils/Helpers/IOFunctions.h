#ifndef NTAU__IOFUNCTIONS__H
#define NTAU__IOFUNCTIONS__H

#include "TSystem.h"
#include "TTimeStamp.h"
#include <iostream> 
#include <chrono>
#include <ctime>
#include <iomanip>
#include <TFile.h>
#include <TTree.h>
#include <dirent.h>
#include <iostream>
#include <fstream>

#include "NtupleAnalysisUtils/Configuration/CanvasOptions.h"
#include "NtupleAnalysisUtils/Visualisation/MultiPagePdfHandle.h"

/// This header declares a number of utility functions 
/// related to file system operations and I/O. 


namespace PlotUtils{

    /// =====================================================
    /// Methods for histogram I/O
    /// =====================================================

    /// Read a histogram from a file. The name can include an arbitrary path within the file. 
    /// Will return a nullptr in case of failure. 
    /// The returned object will be a clone of the original, and the file will be closed 
    /// after reading. 
    template <class T> T* readFromFile (const std::string fname, const std::string & histoname);

    /// These variants can be used with an already-open file handle. 
    /// Can avoid unnecessary open/close ops if used carefully.
    /// Will also return a clone as above. 
    template <class T> T* readFromFile (std::shared_ptr<TFile> f, const std::string & histoname);
    template <class T> T* readFromFile (TFile* f,                 const std::string & histoname);

    /// Tries to resolve the location of a given path
    /// resolving the environment variables or testing relative paths w.r.t. to the TestArea
    /// If the location cannot be resolved an empty string is returned
    std::string GetFilePath(const std::string &Path);

    
    /// Opens a TFile and packs the File pointer into a smart pointer
    std::shared_ptr<TFile> Open(const std::string &Path);

    /// Creates a TDirectory inside an existing ROOT file/ subdirectory "in_dir".
    /// If the path points to several layers of directories, the entire
    /// hierarchy of directories are made recursively if needed
    /// (like mkdir -p in unix)
    TDirectory* mkdir(const std::string &name, TDirectory *in_dir);

    /// Create a file system directory in case it does not yet exist. 
    /// Does not result in an error for already existing directories. 
    /// returns whether a new directory was made.
    /// Sub-directories are created recursively a la "mkdir -p"
    bool createDirIfNeeded(const std::string & dir);

    /// Filters a file list in terms of validity of each entry meaning that the file
    /// must be openable and contain the required TTree object. Optionally files with
    /// empty trees can be skipped
    std::vector<std::string> getValidFiles(const std::vector<std::string> &in_files, const std::string &tree_name, bool skip_empty = true);
	/// Helper method which scans recursively through the file content and adds the path of each <T> object
	/// to the list
    template <class T> std::vector<std::string> getAllObjectsFromFile(const std::string &file);
    /// Helper method which scans through the content of a TDirectory and adds the path of each <T> object
	/// to the list
	template <class T> std::vector<std::string> getAllObjectsFromDir(TDirectory *dir, const std::string &base_path = "", bool recursive=true);
    template <> std::vector<std::string> getAllObjectsFromDir<TDirectory>(TDirectory* dir, const std::string& base_path, bool recursive);
    /// Helper method which scans the file for all objects of a given type and returns all objects untila certain depth of the 
    /// folder strucuture
    template <class T> std::vector<std::string> getObjectsUntilLayer(const std::string& file, unsigned int layer_max, unsigned int layer_min = 0);

    /// @brief Helper to find all files in a given file system directory and assign them to a Sample object
    /// @arg searchPath: File system directory to search for root files
    /// @arg regex: pattern to match to files to look for. Can for example include the DSID you want 
    /// @arg treeName: If set, only returns files containing a tree of a certain name. 
    /// @arg maxFiles: If set to >= 0, will limit the number of files to return. This can be used for testing with low statistics. 
    std::vector<std::string> findFilesInDir(const std::string & searchPath, const std::string & regex, const std::string & treeName = "", int maxFiles=-1); 
    
    /// ==================================
    /// tools for saving plots to files
    /// ==================================

    /// This will provide the default directory to save plots to.
    /// It defaults - $TestArea/../Plots/<date>/. 
    /// Will also create the dir if needed. 
    /// Hint: Make a symlink $TestArea/../Plots pointing to 
    /// your favourite folder or CernBox! 
    std::string getDefaultPlotDir();
    
    /// Save the provided canvas as an image file. 
    /// Fname is the desired file name without suffix.
    /// It can contain a folder hierarchy, which will be generated on demand. 
    /// The file types to save as (can be several at once), as well as the base 
    /// directory to write to folder, are configured by the CanvasOptions. 
    void saveCanvas(TCanvas*                 can, const std::string & fname, const CanvasOptions & options=CanvasOptions());
    void saveCanvas(std::shared_ptr<TCanvas> can, const std::string & fname, const CanvasOptions & options=CanvasOptions());

    /// Generate a multi-page PDF handle. Uses the specified file name, the "pdf" suffix is automatically added. 
    /// The file name can contain a folder structure, which is generated on demand. 
    /// The base folder is specified in the CanvasOptions. 
    std::shared_ptr<MultiPagePdfHandle> startMultiPagePdfFile(const std::string & fname, const CanvasOptions & options=CanvasOptions());

    /// compose the full file name based on the desired subfolder structure and name plus the starting folder. 
    const std::string composeFileName(const std::string & fname, const std::string & basePlotDir = "");

}

#include "IOFunctions.ixx"

#endif  // NTAU__IOFUNCTIONS__H
