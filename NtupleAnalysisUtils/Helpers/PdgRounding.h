#ifndef NTAU__PDGROUNDING__H
#define NTAU__PDGROUNDING__H

#include <utility> 

/// This header declared helper methods to perform 
/// PDG-style rounding of numbers. 

namespace PlotUtils{
    
    // Helper struct representing a central value with asymmetric errors
    struct valWithAsymmErr{
        double cen; 
        double error_up;
        double error_down;
    };

    /// Will round a central value with asymmetric errors. 
    valWithAsymmErr pdgRound(const valWithAsymmErr & v);
    /// Will round a pair (central_value, error) 
    std::pair<double,double> pdgRound(const std::pair<double,double> & v);

}


#endif // NTAU__PDGROUNDING__H