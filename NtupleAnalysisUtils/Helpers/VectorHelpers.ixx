template <class T> std::ostream &operator<<(std::ostream &os, const std::vector<T> &v){
    os << "{";
    for (const auto& i : v) { os << " " << i; }
    os << " }";
    return os;
}

/// push-back to a vector using a += operator
template <class T> std::vector<T>& operator+=(std::vector<T>& vec, const T& ele){
    vec.push_back(ele);
    return vec;
}

/// also concatenate two vectors using a += operator 
template<class T> std::vector<T>& operator+=(std::vector<T>& vec, const std::vector<T>& new_cont){
    vec.insert(vec.end(),new_cont.begin(), new_cont.end());
    return vec;
}

/// push-back into a vector-copy using a + operator
template <class T> std::vector<T> operator+(const std::vector<T>& vec, const T& ele){
    std::vector<T> new_vec(vec);
    new_vec.push_back(ele);
    return new_vec;
}

/// concatenate into a vector-copy using a + operator. Ordering within the output corresponds
/// to operand order. 
template <class T> std::vector<T> operator+(const std::vector<T>& a, const std::vector<T>& b){
    std::vector<T> new_vec(a);
    new_vec.insert(new_vec.end(),b.begin(),b.end());
    return new_vec;
}

