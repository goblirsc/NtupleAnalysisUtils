#ifndef NTAU__STRINGHELPERS__H
#define NTAU__STRINGHELPERS__H

#include <string> 
#include <vector> 

/// This header defines a set of helpers for string manipulation. 


namespace PlotUtils{

    /// Resolves environment variables ${str}. 
    /// Please note that the curly
    /// brakets are mandatory in this case
    std::string ResolveEnviromentVariables(std::string str);

    /// Replaces all occurences of substring "exp" in the string "str" by 
    /// the string "rep". 
    /// An exception is thrown if the "exp" is substr of the rep string
    std::string ReplaceExpInString(std::string str, const std::string &exp, const std::string &rep);

    // return a copy of the input string in which all occurrences of any character/string in the vector (second arg) have been removed. Useful for sanitizing algebraic operators from autogenerated names. 
    std::string pruneUnwantedCharacters(const std::string & input, const std::vector<std::string> & unwanted = {" ", "[", "]", "(", ")", "{", "}", "/", ":", "#", "@", "^", "&"});

    /// Creates a random of the size s consisting of numbers, small
    /// and capital letters. Useful to make ROOT happy when it 
    /// bothers the user about naming! 
    std::string RandomString(size_t s);
    

}


#endif // NTAU__STRINGHELPERS__H