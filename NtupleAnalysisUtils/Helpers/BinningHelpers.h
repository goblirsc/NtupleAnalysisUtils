#ifndef NTAU__BINNINGHELPERS__H
#define NTAU__BINNINGHELPERS__H

#include <vector> 

/// This is a set of helper methods intended to help us 
/// generate histogram binnings

namespace PlotUtils{

    /// create a loglinear binning in the given interval and with the given number of steps.
    /// Bins will be equidistant in log-space 
    std::vector<double> getLogLinearBinning(double start, double end, int nbins);

    /// create a linear binning in the given interval and with the given number of steps.
    /// Bins will be equidistant in linear space. 
    std::vector<double> getLinearBinning(double start, double end, int nbins);

}

#endif //  NTAU__BINNINGHELPERS__H