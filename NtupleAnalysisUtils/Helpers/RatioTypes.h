#pragma once 
#include "TH1D.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TEfficiency.h"
#include "TProfile.h"
#include "TProfile2D.h"
/// Helper definitions to define which objects resolve to which ratios 

/// Default mapping: Return the same type 
template <class T> struct ratioMap{
    T theType; 
};

/// now specify special cases 
#define MAP_RTYPE(key,val) template <> struct ratioMap<key>{ val theType;}; 

/// Efficiency ratios will be expressed as a TH1D with binomial errors
MAP_RTYPE(TEfficiency,TH1)
/// Ratios between profiles will be expressed by 1D histograms
MAP_RTYPE(TProfile,TH1D)
MAP_RTYPE(TProfile2D,TH2D)

/// convenience typedef
template <class T>
using ratioType = decltype(ratioMap<T>::theType);