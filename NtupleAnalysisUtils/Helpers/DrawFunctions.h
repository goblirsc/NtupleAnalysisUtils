#ifndef NTAU__DRAWFUNCTIONS__H
#define NTAU__DRAWFUNCTIONS__H

#include "NtupleAnalysisUtils/Configuration/DrawObjectFormat.h"
#include "NtupleAnalysisUtils/Visualisation/LegendEntry.h"
#include "NtupleAnalysisUtils/Visualisation/MultiPadCanvas.h"
#include "NtupleAnalysisUtils/Helpers/HistoEvaluation.h"

#include "TLatex.h"
#include "TLine.h"
#include "TMarker.h"
#include "TLegend.h"


/// This file declares a range of helper methods within the 
/// PlotUtils namespace.
/// They are intended for drawing operations, 
/// including both the booking of canvases and 
/// the addition of objects to these canvases. 

namespace PlotUtils{

    /// Get the "official" ATLAS color scale 
    Color_t getAtlasColor(size_t k); 
    std::array<Color_t, 10> getAtlasCols(); 

    ///================================================
    /// Methods to various decorations onto the canvas
    ///================================================
    
    /// This method draws a TLatex on the current canvas, at the (default NDC) 
    /// coordinates x,y. 
    /// The text to draw is the third argument, and the properties 
    /// of the text (font, color, alignment, ...) as well as NDC
    /// or user coordinates can be specified 
    /// in the format argument. See Configuration/DrawObjectFormat.h 
    /// for available options
    TLatex* drawTLatex(double x, double y, 
                       const std::string & text, 
                       const DrawObjectFormat & format = DrawObjectFormat());

    /// This method draws the typical "ATLAS Something" label on the canvas. 
    /// As per convention, "ATLAS" is always rendered in ROOT font 72. 
    /// The status string is used to specify the word after "ATLAS". 
    /// Further text properties can be specified via the format argument. 
    TLatex* drawAtlas( double x, double y, 
                       const std::string & status = "Internal", 
                       const DrawObjectFormat & format = DrawObjectFormat());

    /// This method draws the standard "YY TeV, XXX fb^{-1}" luminosity label 
    /// on the current canvas. All elements of the label can be manually set 
    /// or skipped (leave empty to skip). 
    /// Further text properties can be specified via the format argument. 
    TLatex* drawLumiSqrtS( double x, double y, 
                           const std::string & lumi = "139 fb^{-1}", 
                           const std::string & sqrts = "13 TeV", 
                           const DrawObjectFormat & format = DrawObjectFormat());

    /// Draw a set of labels on the canvas. 
    /// They will be drawn in a vertical stack, starting at (x,y0), with the 
    /// following labels stacked below at a vertical spacing of dy between each neighbour. 
    /// One TLatex is written per entry in the label vector
    /// Further text properties can be specified via the format argument. 
    std::vector<TLatex*> drawMultipleLabels (double x, double y0, double dy, 
                                             const std::vector<std::string> & labels, 
                                             const DrawObjectFormat & format = DrawObjectFormat());

    /// draws the ATLAS, lumi and other (extra_labels) labels in the pad using 
    /// the locations and content specified in the CanvasOptions argument. 
    /// Further text properties can be specified via the format argument. 
    std::vector<TLatex*> drawLabels(const std::vector<std::string> & extra_labels, 
                                    const CanvasOptions & opts=CanvasOptions(), 
                                    const DrawObjectFormat & format=DrawObjectFormat()); 

    /// Draw a single TMarker on the current canvas - by default in user coordinates. 
    /// The marker properties, including colour, style, and NDC coordinates,  
    /// can be specified via the format argument. 
    void drawMarker(double x, double y, const DrawObjectFormat & format = DrawObjectFormat());
   
    /// Draw a single TLine on the current canvas. (x1,y1) is the starting point,
    /// (x2,y2) is the end point - by default in user coordinates. 
    /// The line properties, including colour, style, and NDC coordinates,  
    /// can be specified via the format argument. 
    TLine* drawLine(double x1, double y1, double x2, double y2, const DrawObjectFormat & format = DrawObjectFormat());

    /// This method draws additional "labels" along an axis, in form of a set of TLatex objects. 
    /// Useful in case you use log axes and ROOT refuses to give you nice labels. 
    /// @arg locations: Abscissa values to draw labels for
    /// @arg reference: The axis to add the labels to 
    /// @arg isLog: Whether or not the axis is in log-scale 
    /// @arg dy: Allows to fine-tune the label location up and down if they don't match the existing ones
    void addAxisLabels(std::vector<double> locations, TAxis* reference, bool isLog=false, double dy = 0);
    
    /// Draws arrows pointing up or down for entries in a ratio exceeding the cutoff values provided. 
    /// Used for result plots to indicate out-of-range bins. 
    /// @arg ratio: The ratio to scan for outliers
    /// @arg cutoff_low: Lower edge of the visible y interval, below which to draw downward arrows
    /// @arg cutoff_low: Upper edge of the visible y interval, above which to draw upward arrows
    /// @arg firstbin: First bin for which to draw arrows if needed
    /// @arg lastbin: Last bin for which to draw arrows if needed
    void addArrowsToRatio(TH1* ratio, double cutoff_low = 0.5, double cutoff_up = 1.5, int firstbin = -1, int lastbin = 1e9 );

    ///================================================
    /// Methods to draw legends 
    ///================================================

    /// This will draw a legend onto the current pad. 
    /// An entry will be generated for each element of the 'entries'
    /// vector. The legend's location is steered by the canvas options,
    /// while the fine-tuning of the visual properties can be done using
    /// the format argument. 
                       TLegend* drawLegend(std::vector<LegendEntry> entries,  
                            const CanvasOptions & opts=CanvasOptions(), 
                            const DrawObjectFormat & format = DrawObjectFormat{});

    /// Similar to the above, but takes a vector of Plot objects as the first argument. 
    template <class H> TLegend* drawLegend(std::vector<Plot<H>>     entries,  
                            const CanvasOptions & opts=CanvasOptions(), 
                            const DrawObjectFormat & format = DrawObjectFormat{});

    ///================================================
    /// Methods to draw canvases, possibly with sub-pads 
    ///================================================

    /// This method draws an empty canvas, with its properties and geometry determined 
    /// by the CanvasOptions. A random name is assigned to keep ROOT from 
    /// complaining about repeated names... 
    /// @param opts: CanvasOptions steering the look of this canvas
    /// @param applyMargins: If true, will apply the margins of the canvas options directly to the TCanvas.
    ///                      Default behaviour is to apply them at the TPad level (via prepareTwoPadCanvas etc) 
    std::shared_ptr<TCanvas> prepareCanvas(const CanvasOptions & opts, bool applyMargins=false); 

    /// This will generate the typical "Top pad plus ratio pad" pair of pads within a canvas.
    /// Returns a MultiPadCanvas containing the canvas and its two pads.
    /// The properties of canvas and pads (sice, padding, split between the pads,...) 
    /// are set via the canvas options. 
    MultiPadCanvas prepareTwoPadCanvas   (const CanvasOptions & opts=CanvasOptions());

    /// Comparable to the above but adds two "ratio" sub-pads (a total of three), where
    /// the visible area of the bottom space is split evenly between the two ratio-like 
    /// pads.  
    MultiPadCanvas prepareThreePadCanvas (const CanvasOptions & opts=CanvasOptions());
    
    /// These methods will generate a two-dimensional set of pads. 
    /// The horizontal split location is steered via the canvas options,
    /// while each pad is given the same vertical size. 
    /// The arrangement of pads in the output vector is 
    /// first left-to-right, then top-to-bottom, 
    ///                         1 2 
    ///                         3 4   
    ///                         5 6  etc
    MultiPadCanvas prepare2x2Canvas (const CanvasOptions & opts=CanvasOptions());
    MultiPadCanvas prepare3x2Canvas (const CanvasOptions & opts=CanvasOptions());
    MultiPadCanvas prepare4x2Canvas (const CanvasOptions & opts=CanvasOptions());

    /// These methods draw an appropriate frame into the current pad, which can 
    /// accomodate the drawing of all the user plots.
    /// The first argument is the set of plots intended for drawing - the method
    /// will use these together with the axis config of the CanvasOptions
    /// to determine the appropriate ranges and labeling to use. 

    /// This flavour is intended for any "top" frame (not ratio) 
    template <class H> TH1* drawTopFrame   (std::vector<Plot<H>> & plots, const CanvasOptions & opts=CanvasOptions());
    /// This flavour is intended for ratio frames.
    /// If the top frame is passed, the x boundaries will be copied from it. 
    template <class H> TH1* drawRatioFrame(std::vector<Plot<H>> & plots, const CanvasOptions & opts=CanvasOptions(),TH1* topFrame=nullptr);

    /// This is a helper method to adapt the label sizes and format of a histogram 
    /// based on the CanvasOptions. 
    void adaptLabels (               TH1* hist, const CanvasOptions & opts=CanvasOptions()); 

    /// Same as above, but able to batch-process a series of histograms in one go!  
    void adaptLabels (std::vector<TH1*> histos, const CanvasOptions & opts=CanvasOptions());


    ///================================================
    /// Helper Methods intended for internal use 
    ///================================================

    /// This is a helper methods placing a set of pads within an existing canvas. 
    /// It implements the functionality described in the prepareNx2Canvas methods. 
    /// nrows is the number of vertical rows (always two pads per row). 
    /// The CanvasOptions determine all details of the geometry. 
    std::vector<std::shared_ptr<TPad>> prepareNx2Canvas (int nrows=4, const CanvasOptions & opts=CanvasOptions());

    /// These helper methods format the labels and change titles of the 
    /// frame histogram passed as first argument following the setup 
    /// provided in the canvas options.  
    void updateTopFrame(TH1* frame, const CanvasOptions & opts=CanvasOptions());
    void updateRatioFrame(TH1* frame, const CanvasOptions & opts=CanvasOptions());

    /// Helper method - this translates from the CanvasOption convention and 
    /// Pad coordinates. 
    double toPadCoordsY(double y_internal); 
    double toPadCoordsX(double x_internal); 

    /// helper method to check for alphanumeric axes if supported 
    /// Default behaviour: Assume not supported, do nothing
    template <class T> void adaptAlphaNumAxis(TH1* , std::shared_ptr<T> ){}
    /// specialisation for TH1 family 
    template<> void adaptAlphaNumAxis(TH1* hFrame ,std::shared_ptr<TH1> hFront );

}

#include "DrawFunctions.ixx"

#endif // NTAU__DRAWFUNCTIONS__H
