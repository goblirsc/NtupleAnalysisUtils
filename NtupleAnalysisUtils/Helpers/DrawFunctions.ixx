template <class H> TLegend* PlotUtils::drawLegend(std::vector<Plot<H>> entries,  
                                                    const CanvasOptions & opts, 
                                                    const DrawObjectFormat & format){
    std::vector<LegendEntry> LE;
    /// loop over all the plots 
    for (auto & p : entries){
        // skip non-titled entries - assumed to not be desired as legend entries
        if (!p.plotFormat().LegendTitle.isUserSet() || p.plotFormat().LegendTitle().empty()) continue; 
        auto theTitle = p.plotFormat().LegendTitle(); 
        auto theOption = p.plotFormat().LegendOption(); 
        /// skip non-titled entries, convert titled oens to 
        /// LegendEntry instances
        LE.push_back(LegendEntry(p(), theTitle, theOption));
    }
    /// Draw a legend based on the LegendEntry instances we built
    return drawLegend(LE, opts, format);
}

template <class H> TH1* PlotUtils::drawTopFrame(std::vector<Plot<H>> & plots, const CanvasOptions & opts){

    /// Determine the axis ranges we need 
    std::pair<double, double> yrange = getYRange(plots,opts);
    std::pair<double, double> xrange = getXRange(plots,opts);

    /// Set log-axes appropriately 
    gPad->SetLogx(opts.XAxis().Log());
    gPad->SetLogy(opts.YAxis().Log());
    gPad->SetLogz(opts.ZAxis().Log());

    /// Draw a frame histogram with the identified range 
    TH1* theFrame = gPad->DrawFrame(xrange.first,yrange.first,xrange.second,yrange.second);

    /// Set axis titles 
    theFrame->GetYaxis()->SetTitle(getYtitle(plots.front(), opts).c_str());
    theFrame->GetXaxis()->SetTitle(getXtitle(plots.front(), opts).c_str());

    /// Bin labels may have been set in the plot --> in this case copy them
    adaptAlphaNumAxis(theFrame, std::dynamic_pointer_cast<TH1>(plots.front().getSharedHisto())); 
    /// update the frame formatting 
    updateTopFrame(theFrame,opts);
    /// and return 
    return theFrame; 
}

template <class H> TH1* PlotUtils::drawRatioFrame(std::vector<Plot<H>> & plots, const CanvasOptions & opts,TH1* topFrame){
    
    /// Determine the axis ranges we need 
    std::pair<double, double> yrange = getRatioRange(plots,opts);
    std::pair<double, double> xrange;
    if (topFrame != nullptr) xrange = {topFrame->GetXaxis()->GetXmin(), topFrame->GetXaxis()->GetXmax()};
    else xrange = getXRange(plots,opts);


    /// Set log-axes appropriately 
    gPad->SetLogx(opts.XAxis().Log());
    gPad->SetLogy(opts.RatioAxis().Log());

    
    /// Draw a frame histogram with the identified range 
    TH1* theFrame = gPad->DrawFrame(xrange.first,yrange.first,xrange.second,yrange.second);

    /// Set axis titles 
    theFrame->GetYaxis()->SetTitle(PlotUtils::getRatioTitle(plots.front(), opts).c_str());
    theFrame->GetXaxis()->SetTitle(PlotUtils::getXtitle(plots.front(), opts).c_str());

    /// Bin labels may have been set in the plot --> in this case copy them
    adaptAlphaNumAxis(theFrame, std::dynamic_pointer_cast<TH1>(plots.front().getSharedHisto())); 
    /// update the frame formatting 
    updateRatioFrame(theFrame,opts);
    
    /// and return 
    return theFrame; 
}
