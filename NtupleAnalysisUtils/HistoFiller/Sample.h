#ifndef __SAMPLE__H
#define __SAMPLE__H

#include <string> 
#include <mutex> 
#include <map> 
#include <vector> 
#include "TFile.h"
#include "NtupleAnalysisUtils/HistoFiller/Selection.h"
#include "NtupleAnalysisUtils/HistoFiller/HistoFillerInterfaces.h"

/// Simple class to model a Data/MC Sample. 
/// It collects one or several files to process, with the option of associating cuts (e.g. filters) to specific files.


/// a helper to refine objects we create using the InputFile 
template<class ProcessThis> class TreeRefinement final: public ISortableByID{
    public: 
    TreeRefinement() : ISortableByID(){setID(ObjectID(0));} // trivial case always resolves to same ID
    TreeRefinement(std::function<void(ProcessThis&)> modifier): ISortableByID(), m_refinement(modifier){} 
    void operator()(ProcessThis &t) const {m_refinement(t);}
    private: 
    std::function<void(ProcessThis&)> m_refinement = [](ProcessThis&){}; 
};

/// The basic building block of a sample is one individual "InputFile" (one tree taken from some file). 
template <class ProcessThis> class InputFile final: public IInputFile{
public:
    /// Construct by providing a file location, and a tree name (optional folders accepted). 
    /// optionally, a file-specific selection (for example phase-space cut / outlier weight removal) can be specified.  
    /// The final optional "refinement" parameter is a user-specified function that can be used to further refine the created object (e.g. added configuration). 
    InputFile(const std::string & thefile, const std::string & treename, Selection<ProcessThis> specificCuts=Selection<ProcessThis>(), TreeRefinement<ProcessThis> refinement=TreeRefinement<ProcessThis>());
    /// set the name of the tree to find in the input files. 
    void setTreeName(const std::string & name);
    /// and the name of the file 
    void setFileName(const std::string & name);
    /// allow to replace the sample-specific cuts
    void setSelection(Selection<ProcessThis> sel);
    
    void setRefinement(TreeRefinement<ProcessThis> fcn); 

    /// Getters for the key properties
    const std::string & getFileName() const override final; 
    const std::string & getTreeName() const override final;
    Selection<ProcessThis> getSelection() const;
    TreeRefinement<ProcessThis> getRefinement() const;

    /// This will read the input from the file. 
    ProcessThis getTreeFromFile( std::shared_ptr<TFile> & f ) const;
    std::shared_ptr<TFile> openFile() const override final; 
    
    /// Create a task that the HistoFiller can work with based on this input file.  
    /// Note: this member is implemented in HistoFillerHelpers.ixx, since it relies on includes from there and is only used in conjunction with this operating mode. 
    virtual std::shared_ptr<IHistoFillTask> spawnTask (SelectionToFillableMap & payload, bool splitAndMergeMode) override final;

    /// Create a clone 
    virtual std::shared_ptr<IInputFile> clone() const override final;

    /// Enable weights for the bootstrap method if needed
    virtual void setNBootstrapWeights(size_t n)  override final;
    virtual size_t getNBootstrapWeights() const override final;
    
    void setBootstrapSeed(std::function<ULong_t(ProcessThis&)> seeder);
    ULong_t seedBootstrap(ProcessThis& t);

protected:
    void updateDynamicID(); 
    std::string m_fileName{};
    std::string m_treeName{};
    Selection<ProcessThis> m_selection;
    size_t m_n_bootstrap{0};
    std::function<ULong_t(ProcessThis&)> m_bootstrap_seed{
        [](ProcessThis& t) ->ULong_t{
           return t.getEntries();
    }};
    TreeRefinement<ProcessThis> m_refinement {}; 
    
};

/// the sample class itself can contain several files (each a InputFile), each of which may have its own specific selection / tree name. 
template <class ProcessThis> class Sample final: public ISample{
    public:
        /// start with empty sample to start populating it downstream
        Sample();
        /// Sometimes useful: Quickly package a single file in a Sample object. 
        Sample(const std::string & thefile,  const std::string & treename, Selection<ProcessThis> specificCuts=Selection<ProcessThis>(), TreeRefinement<ProcessThis> refinement = TreeRefinement<ProcessThis>());
        /// Or directly instantiate a sample with a set of files! 
        Sample(std::vector<InputFile<ProcessThis>> files);
        
        /// @brief Constructor to build a Sample object wrapping all the files in a given file system location that match a certain pattern. 
        /// @tparam TreeType: the MiniTree class to wrap in the Sample object 
        /// @arg searchPath: File system directory to search for root files
        /// @arg regex: pattern to match to files to look for. Can for example include the DSID you want 
        /// @arg treeName: name of the tree inside the file to load
        /// @arg maxFiles: If set to >= 0, will limit the number of files to return. This can be used for testing with low statistics. 
        /// @arg refinement: If specified, the supplied function will be called to refine the generated object after construction 
        Sample(const std::string & searchPath, const std::string & regex, const std::string & treeName, int maxFiles=-1, TreeRefinement<ProcessThis> refinement = TreeRefinement<ProcessThis>()); 
        
        /// Optionally, all of the constructors can also be called with a leading PlotFormat 
        template <typename... args> Sample(PlotFormat pf, args... a):
             Sample(a...){m_pf = pf;}


        /// Add a file to this sample. 
        /// You can add cuts specific to this file (for example if you need to filter out some phase space region). 
        /// The second version constructs a InputFile in-place
        void addFile(InputFile<ProcessThis> addMe);
        void addFile(const std::string & fname, 
                     const std::string & treename, 
                     Selection<ProcessThis> specificCuts=Selection<ProcessThis>(), 
                     TreeRefinement<ProcessThis> refinement = TreeRefinement<ProcessThis>());
        // add all files from another sample. 
        void addFilesFrom(const Sample<ProcessThis> & other);

        /// This is used to retrieve all connected files for this sample 
        std::vector<InputFile<ProcessThis>> getInputFiles() const;

        // this returns a version of the above sorted by descending file size, useful for optimising the processing order 
        std::vector<InputFile<ProcessThis>> getSortedInputFiles() const;

        // Open a file and read an input (typically: ntuple)
        // If you pass the file pointer, the method will reuse it in case this is the file matching fname, otherwise it will close 
        // the existing file and set f to the new file. 
        // Passing a nullptr will result in the ptr being set to the newly opened file
        template <class HistClass> static HistClass* getObjectFromFile(const std::string & fname, 
                                                                       const std::string & objectName, 
                                                                       std::shared_ptr<TFile> & f );

        // implement the ISample interface used by the HistoFiller
        virtual std::vector<std::shared_ptr<IInputFile> > getInputFiles();
        virtual std::shared_ptr<ISample> clone() const;
        
        // set bootstrap events to process 
        void setNBootstrapWeights(size_t n);
        size_t getNBootstrapWeights() const;
        void setBootstrapSeed(std::function<ULong_t(ProcessThis&)> seeder);
        PlotFormat & plotFormat() {return m_pf;}

    protected:
        void updateDynamicID(); 
        std::vector<InputFile<ProcessThis>> m_files;
        size_t m_n_bootstrap{0};       
        PlotFormat m_pf; 
    };


    #include "Sample.ixx"

#endif
