#pragma once 

#include "NtupleAnalysisUtils/Ntuple/NtupleBranchAccessByName.h" 
#include "Selection.h" 

template <class TreeType> Selection<TreeType> CutGreater(std::function<double(TreeType)> getCut, double threshold, bool allowEqual=false){
    return Selection<TreeType>([getCut,allowEqual,threshold](TreeType &t){
        double val = getCut(t); 
        return (allowEqual ? val >= threshold : val > threshold); 
    }); 
}
template <class TreeType> Selection<TreeType> CutLessThan(std::function<double(TreeType)> getCut, double threshold, bool allowEqual=false){
    return Selection<TreeType>([getCut,allowEqual,threshold](TreeType &t){
        double val = getCut(t); 
        return (allowEqual ? val <= threshold : val < threshold); 
    }); 
}
template <class TreeType, typename varType> Selection<TreeType> CutEqual(std::function<varType(TreeType)> getCut, varType threshold){
    return Selection<TreeType>([getCut,threshold](TreeType &t){
        varType val = getCut(t); 
        return val == threshold;
    }); 
}
template <class TreeType> Selection<TreeType> CutWindow(std::function<double(TreeType)> getCut, double min,double max, bool allowEqualMin=false,bool allowEqualMax=false){
    return Selection<TreeType>([getCut,allowEqualMin,allowEqualMax,min,max](TreeType &t){
        double val = getCut(t); 
        return (allowEqualMin ? val >= min : val > min) &&  (allowEqualMax ? val <= max : val < max); 
    }); 
}



template <class TreeType> Selection<TreeType> CutGreaterOnList(std::function<double(TreeType,size_t)> getCut, double threshold, bool allowEqual=false){
    return Selection<TreeType>([getCut,allowEqual,threshold](TreeType &t,size_t i){
        double val = getCut(t,i); 
        return (allowEqual ? val >= threshold : val > threshold); 
    }); 
}
template <class TreeType> Selection<TreeType> CutLessThanOnList(std::function<double(TreeType,size_t)> getCut, double threshold, bool allowEqual=false){
    return Selection<TreeType>([getCut,allowEqual,threshold](TreeType &t,size_t i){
        double val = getCut(t,i); 
        return (allowEqual ? val <= threshold : val < threshold); 
    }); 
}
template <class TreeType, typename varType> Selection<TreeType> CutEqualOnList(std::function<varType(TreeType,size_t)> getCut, varType threshold){
    return Selection<TreeType>([getCut,threshold](TreeType &t,size_t i){
        varType val = getCut(t,i); 
        return val == threshold;
    }); 
}
template <class TreeType> Selection<TreeType> CutWindowOnList(std::function<double(TreeType,size_t)> getCut, double min,double max, bool allowEqualMin=false,bool allowEqualMax=false){
    return Selection<TreeType>([getCut,allowEqualMin,allowEqualMax,min,max](TreeType &t,size_t i){
        double val = getCut(t,i); 
        return (allowEqualMin ? val >= min : val > min) &&  (allowEqualMax ? val <= max : val < max); 
    }); 
}
template <class TreeType> Selection<TreeType> PickItemFromList(size_t index){
    return Selection<TreeType>([index](TreeType &,size_t i){
        return (i == index); 
    }); 
}


