

 template <class HistoType, class inputType> RunHistoFiller<HistoType, inputType>::RunHistoFiller(
                 HistoType* refHisto,
                 Sample<inputType> sample,
                 Selection<inputType> selection,
                 PlotFillInstruction<HistoType, inputType> fillInstruction,
                 HistoFiller* filler):
        RunHistoFiller<HistoType,inputType> (std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(refHisto->Clone())),
            sample,
            selection,
            fillInstruction,
            filler){
        refHisto->SetDirectory(0);
        TH1::AddDirectory(kFALSE);
    }

    
template <class HistoType, class inputType> RunHistoFiller<HistoType, inputType>::RunHistoFiller(
                 std::shared_ptr<HistoType> refHisto,
                 Sample<inputType> sample,
                 Selection<inputType> selection,
                 PlotFillInstruction<HistoType, inputType> fillInstruction,
                 HistoFiller* filler):
    m_internalHisto(nullptr),
    m_refHisto(refHisto),
    m_sample(sample),
    m_selection(selection),
    m_fillInstruction(fillInstruction),
    m_filler(filler),
    m_hasBeenFilled(false){

        /// ensure ROOT leaves our histos alone 
        TH1::AddDirectory(kFALSE);

        /// set a composite identifier for use within the filler. 
        /// This ensures combinations of the same sample/selection/fillinstruction/template resolve to the same ID
        /// and thus avoids duplicate filling. 
        ObjectID compositeID(
            {sample.ISortableByID::getID(), 
            selection.ISortableByID::getID(), 
            fillInstruction.ISortableByID::getID(), 
            ObjectID(refHisto->GetName())}
        ); 
        this->ISortableByID::setID(compositeID); 

        /// here we default to the default filler if no specific instance is passed
        if (!filler){
            m_filler = HistoFiller::getDefaultFiller();
        }
        /// register ourselves with the filler
        if (m_filler) {
            registerWithHistoFiller(m_filler);
        }
}
 template <class HistoType, class inputType> RunHistoFiller<HistoType, inputType>::RunHistoFiller(
                 Sample<inputType> sample,
                 Selection<inputType> selection,
                 PlotFillInstructionWithRef<HistoType, inputType> fillInstruction,
                 HistoFiller* filler):
        /// Delegate constructor 
        RunHistoFiller(fillInstruction.getRefHistShared(),
                        sample,
                        selection,
                        fillInstruction,
                        filler){
    }

template <class HistoType, class inputType> RunHistoFiller<HistoType, inputType>::RunHistoFiller(
                 HistoType* refHisto,
                 Sample<inputType> sample,
                 Selection<inputType> selection,
                 std::function<void(HistoType*, inputType &)> fillFunction,
                 HistoFiller* filler):
            /// Delegate constructor
            RunHistoFiller(refHisto,sample,selection,PlotFillInstruction<HistoType,inputType>(fillFunction),filler){
}
template <class HistoType, class inputType> RunHistoFiller<HistoType, inputType>::RunHistoFiller(
                 std::shared_ptr<HistoType> refHisto,
                 Sample<inputType> sample,
                 Selection<inputType> selection,
                 std::function<void(HistoType*, inputType &)> fillFunction,
                 HistoFiller* filler):
            /// Delegate constructor
            RunHistoFiller(refHisto,sample,selection,PlotFillInstruction<HistoType,inputType>(fillFunction),filler){
}

// for the following two cases, we do not re-register ourselves, as an object with this ID will already be connected to the filler 
 template <class HistoType, class inputType> void RunHistoFiller<HistoType, inputType>::operator= (const RunHistoFiller<HistoType, inputType > & other){
    m_internalHisto= nullptr; 
    m_refHisto= other.m_refHisto; 
    m_sample = other.m_sample; 
    m_selection = other.m_selection; 
    m_fillInstruction = other.m_fillInstruction; 
    m_filler = other.m_filler; 
    m_hasBeenFilled = other.m_hasBeenFilled; 
    /// manually update the ID to make sure we have the same as the input plot 
    this->ISortableByID::setID(other.ISortableByID::getID()); 
    if (other.m_internalHisto) m_internalHisto = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>((other.m_internalHisto->Clone()))); 
}


 template <class HistoType, class inputType> RunHistoFiller<HistoType, inputType>::RunHistoFiller(const RunHistoFiller<HistoType, inputType > & other):
    m_internalHisto(nullptr),
    m_refHisto(other.m_refHisto),
    m_sample(other.m_sample),
    m_selection(other.m_selection),
    m_fillInstruction(other.m_fillInstruction),
    m_filler(other.m_filler),
    m_hasBeenFilled(other.m_hasBeenFilled){

    /// manually update the ID to make sure we have the same as the input plot 
    this->ISortableByID::setID(other.ISortableByID::getID()); 
    if (other.m_internalHisto) m_internalHisto = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>((other.m_internalHisto->Clone()))); 
}
    
/// use hasAddMethod for this check.
template <class HistoType, class inputType> bool RunHistoFiller<HistoType, inputType>::canAdd() const {
    return hasAddMethod<HistoType>::value;
}

/// here we try to retrieve 'our' object from the filler and assign it to the passed pointer ref
template <class HistoType, class inputType> std::shared_ptr<HistoType> RunHistoFiller<HistoType, inputType>::populate(){
    if (!m_filler) {
        std::cerr << " Plot failed to retrieve its output - no HistoFiller connected! "<<std::endl;
        return nullptr;
    }
    std::shared_ptr<IFillable> found = m_filler->getOutcome(*this);
    if (!found)  {
        std::cerr << " Plot failed to retrieve its output - no object of matching ID in HistoFiller! "<<std::endl;
        return nullptr;
    }
    std::shared_ptr<RunHistoFiller<HistoType, inputType>> cast = std::dynamic_pointer_cast<RunHistoFiller<HistoType, inputType>>(found);
    if (!cast) {
        std::cerr << " Plot failed to retrieve its output - object of matching ID in HistoFiller does not have the correct type! "<<std::endl;
        return nullptr;
    }
    if (!m_internalHisto) m_internalHisto = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(cast->getSharedHisto()->Clone()));
    return m_internalHisto;
}

/// equality by name
template <class HistoType, class inputType> bool RunHistoFiller<HistoType, inputType>:: equalTo (const std::shared_ptr<IFillable> & other) const {
    auto other_as_RHF = std::dynamic_pointer_cast<RunHistoFiller<HistoType, inputType>>(other); 
    return (other_as_RHF.get() != nullptr && this->ISortableByID::getID() == other_as_RHF->ISortableByID::getID());
}

/// clone method
template <class HistoType, class inputType>  std::shared_ptr<RunHistoFiller<HistoType, inputType>> RunHistoFiller<HistoType, inputType>::cloneBase() const{
    return std::make_shared<RunHistoFiller<HistoType, inputType>>(*this);
}

template <class HistoType, class inputType>  std::shared_ptr<IFillable > RunHistoFiller<HistoType, inputType>::clone() const{
    return cloneBase();
}
template <class HistoType, class inputType>  std::shared_ptr<IPlotPopulator<HistoType>> RunHistoFiller<HistoType, inputType>::clonePopulator() const {
    return cloneBase();
}
template <class HistoType, class inputType> std::shared_ptr<ISelection> RunHistoFiller<HistoType, inputType>::getSelectionClone() const {
    return m_selection.clone();
}
template <class HistoType, class inputType> std::shared_ptr<ISample> RunHistoFiller<HistoType, inputType>::getSampleClone() const{
    return m_sample.clone();
}
template <class HistoType, class inputType> std::shared_ptr<HistoType> RunHistoFiller<HistoType, inputType>::getSharedHisto(){
    return m_internalHisto;
}
template <class HistoType, class inputType> void RunHistoFiller<HistoType, inputType>::initForFill(){
    std::lock_guard g(m_mutex_init); 
    if (!m_internalHisto) {
        m_internalHisto = std::dynamic_pointer_cast<HistoType>(std::shared_ptr<TObject>(m_refHisto->Clone()));
        ResetHisto(m_internalHisto); 
    }
}

template<> inline void ResetHisto<TEfficiency>(std::shared_ptr<TEfficiency> resetThis){
    /// WHY OH WHY DOES TEFFICIENCY NOT SUPPORT A RESET? 
    std::unique_ptr<TH1> passed {resetThis->GetCopyPassedHisto()}; 
    std::unique_ptr<TH1> total {resetThis->GetCopyTotalHisto()}; 
    passed->Reset();
    total->Reset(); 
    /// This is so pointless... 
    resetThis->SetPassedHistogram(*passed,"f");
    resetThis->SetTotalHistogram(*total,"f");
 }


template <class HistoType, class inputType> void RunHistoFiller<HistoType, inputType>::Reset(){
    if (m_internalHisto) ResetHisto(m_internalHisto); 
}

template <class HistoType, class inputType> void RunHistoFiller<HistoType, inputType>::CollectClones(
            std::vector<std::shared_ptr<IFillable > > & clonesToCollect ){
    for (auto cl : clonesToCollect){
        CollectClone(cl); 
    }
}
template <class HistoType, class inputType> void RunHistoFiller<HistoType, inputType>::CollectClone(std::shared_ptr<IFillable > cloneToCollect ){
    initForFill(); 
    std::lock_guard<std::mutex> lock(m_mutex_collect); 
    if (!cloneToCollect) return; 
    std::shared_ptr<RunHistoFiller<HistoType, inputType>> shared = std::dynamic_pointer_cast<RunHistoFiller<HistoType,inputType>>(cloneToCollect);
    if (!shared) return;
    AddHistos(getSharedHisto(), shared->getSharedHisto());
}
template <class HistoType, class inputType>  bool RunHistoFiller<HistoType, inputType>::hasBeenFilled() const {
    return m_hasBeenFilled;
}
template <class HistoType, class inputType>  void RunHistoFiller<HistoType, inputType>::setHasBeenFilled(bool val) { 
    m_hasBeenFilled=val;
}

// here we attach ourselves to a filler
template <class HistoType, class inputType> 
    void RunHistoFiller<HistoType, inputType>::registerWithHistoFiller (HistoFiller* hf){
    if (hf) hf->attach(*this);
}

/// here we just invoke the fill instruction
template <class HistoType, class inputType>  void RunHistoFiller<HistoType, inputType>::Fill(inputType &  input){
    m_fillInstruction.fill(m_internalHisto.get(), input);
}

