#ifndef NTAU__HISTOFILLERHELPER__H
#define NTAU__HISTOFILLERHELPER__H 

#include <NtupleAnalysisUtils/HistoFiller/HistoFillerInterfaces.h>
#include <NtupleAnalysisUtils/HistoFiller/Selection.h>
#include <NtupleAnalysisUtils/HistoFiller/Sample.h>
#include <TStopwatch.h>
#include <mutex>
#include <atomic>

/// Templated helper classes used in the HistoFiller 
/// These implement the hierarchical File --> Selection --> Plot structure
/// used during the event loop


/// Helper class to improve efficiency when handling a large number of 
/// per-file clones of individual plot items by delaying the 
/// cloning and cleaning up clones as soon as feasible. 
/// Has three members: 
/// - a mode flag 
/// - the parent plot instance 
/// - an internal clone instance 
///
/// If the splitAndMerge flag is *false*, this object will act like a wrapper 
/// to the parent (no clones / additions) 
///
/// If splitAndMerge is *true*, we will operate using a clone. 
/// Once the loop is done, the content of the internal instance will be added 
/// to the parent. Then the internal clone will be cleaned up. 
/// This allows to split jobs across files. 

template <class ProcessThis> class FillTaskFragment{
public: 
    FillTaskFragment(std::shared_ptr<IFillableFrom<ProcessThis>> parentPlot, bool splitAndMergeMode); 
    /// call the fill method 
    inline void Fill(ProcessThis &t); 
    /// debug printout 
    void print(const std::string & spacer="");
    /// initialise the internal clone object 
    void initForFill();
    /// recombine with the parent, invalidates the internal clone  
    void recombine(); 
private: 
    bool m_splitAndMerge{false}; 
    std::shared_ptr<IFillableFrom<ProcessThis>> m_parentPlot{nullptr};
    std::shared_ptr<IFillableFrom<ProcessThis>> m_internalPlot{nullptr};
};

/// This small class wraps the set of plots to be filled for a given selection.
/// It will check the selection outcome once and then fill all dependent plots. 
template <class ProcessThis> class SelectionWithPlots{
public:
    SelectionWithPlots(Selection<ProcessThis> sel,  /// selection to apply 
                       const std::vector<FillTaskFragment<ProcessThis>> & toFill);    /// plots to fill
    // here we fill the plots using this selection
    inline void runFill(ProcessThis &t );
    /// debug printout
    void print(const std::string & spacer=""); 
    /// initialise the internal clone objects 
    void initForFill();
    /// recombine all filled objects with their parents, invalidates the internal clones
    void recombine(); 
protected:
    Selection<ProcessThis> m_Selection;
    std::vector<FillTaskFragment<ProcessThis>> m_toFill;
};

/// This class is used as a task queue in the processing chain. 
/// It contains a set of tasks to complete and a method to pop the next 
/// task of the list. 
/// Each task corresponds to one tree to process.
/// The class is able to perform book-keeping to report its status. 
/// It can also validate the input files ahead of the main loop.   
class ProcessingQueue final : public IFillProgressTracker{
public:
    /// Construct by passing it a list of items to work on. 
    ProcessingQueue(const std::vector<std::shared_ptr<IHistoFillTask>> & taskList); 
    /// Try to open all files, check for input integrity, 
    /// and update the expected number of events to process
    bool performInputValidation(bool allowEmptyFiles=false); 

    /// pop the next work item from the list (and remove it from said list). 
    /// Will return a nullptr when no more tasks are left. 
    std::shared_ptr<IHistoFillTask> getNext(); 
    /// Get the number of left tasks
    size_t numberOfTasks() const;
    /// Allows a client to report its progress to the class's book-keeping. 
    /// This will trigger a printout if configured. 
    void reportProgress (Long64_t nFinished) override; 
    void reportFileComplete () override; 

    /// This method configures when to print the progress to the output stream
    void setPrintInterval(int interval) override;

    /// this method allows to enable printing of the progress after an interval determined by the definitions 
    /// of specialPrintInterval (every X percent or not at all)
    void setPrintInterval(specialPrintInterval interval) override;

    void printProgress(); 

    /// debug print
    void print(const std::string & spacer=""); 

private:
    /// list of remaining tasks to carry out
    std::vector<std::shared_ptr<IHistoFillTask>> m_toDo_remaining; 
    /// mutex for accessing the next item off the task queue
    std::mutex m_mx_getNext; 
    /// mutex for updating the progress counters
    std::mutex m_mx_regDone; 
    /// counters for progress reporting 
    size_t m_n_files_total{0}; 
    std::atomic<size_t> m_n_files_done{0};
    std::atomic<Long64_t> m_n_evt_processed{0};
    std::atomic<Long64_t> m_n_evt_total{0};
    std::atomic<Long64_t> m_n_untilPrintout{0}; 
    /// settings for progress reporting 
    Long64_t m_printInterval{10000}; 
    /// stopwatch to allow timing printouts 
    TStopwatch m_sw; 
};

/// Representation of a single task (input tree to be processed). 
/// These objects are queued into the ProcessingQueue above. 

template <class ProcessThis> class HistoFillTask final: public IHistoFillTask{
public:
    /// Construct by providing the input file (and tree) to parse,
    /// the selections with attached plots to process,
    /// and optionally a number of random boot-strap weights to provide
    HistoFillTask (InputFile<ProcessThis> theSE, 
                   std::vector<SelectionWithPlots<ProcessThis>> toDo, 
                   int nBootStrap=-1);

    /// loop over the tree and fill the Plots. 
    /// Will optionally report its progress to the processing queue
    /// provided as argument (for printouts etc). 
    void run(std::shared_ptr<IFillProgressTracker> theQueue=nullptr) override;

    /// This allows to validate the input source. 
    /// It will return the outcome of the attempt, 
    /// and populate the arg with the number of entries 
    /// found. 
    bool validateInput(Long64_t & nevents_found, bool allowEmpty=false) override;
   
    /// This allows to specify the desired reporting interval. 
    /// The task will report its input to the tracker in regular
    /// intervals to provide status reporting for the user. 
    void setReportingInterval(Long64_t interval) override;

    Long64_t getNEvt() const override; 
    
    /// debug printout
    void print(const std::string & spacer="") override; 
protected:
    /// the input to parse 
    InputFile<ProcessThis> m_InputFile;
    /// a list of selections to run, each with its associated plots. 
    std::vector<SelectionWithPlots<ProcessThis>> m_todo;
    /// number of requested bootstrap weights
    int m_n_bootstrap;
    Long64_t m_nEvt{0}; 
    /// user-configured reporting interval (in entries). 
    Long64_t m_reportingInterval{100};
};


#include "HistoFillerHelpers.ixx" 

#endif  //NTAU__HISTOFILLERHELPER__H
