#pragma once 
#include "PlotFillInstruction.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TEfficiency.h"
#include "TProfile.h"
#include "TProfile2D.h"

template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TH1D,TreeType> fillTH1D(
    std::function<double(TreeType &)> xGetter, 
    std::function<double(TreeType &)> weightGetter, 
    args&&  ... histoargs){
        return PlotFillInstructionWithRef<TH1D,TreeType>(
            [xGetter,weightGetter](TH1D* h, TreeType &t){
                h->Fill(xGetter(t),weightGetter(t)); 
            } , histoargs...);
    }
    
template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TH2D,TreeType> fillTH2D(
    std::function<double(TreeType &)> xGetter, 
    std::function<double(TreeType &)> yGetter, 
    std::function<double(TreeType &)> weightGetter, 
    args && ... histoargs){
        return PlotFillInstructionWithRef<TH2D,TreeType>(
            [xGetter,yGetter,weightGetter](TH2D* h, TreeType &t){
                h->Fill(xGetter(t),yGetter(t),weightGetter(t)); 
            }
        ,histoargs...);
    }
    
template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TH3D,TreeType> fillTH3D(
    std::function<double(TreeType &)> xGetter, 
    std::function<double(TreeType &)> yGetter, 
    std::function<double(TreeType &)> zGetter, 
    std::function<double(TreeType &)> weightGetter, 
    args && ... histoargs){
        return PlotFillInstructionWithRef<TH3D,TreeType>(
            [xGetter,yGetter,zGetter,weightGetter](TH3D* h, TreeType &t){
                h->Fill(xGetter(t),yGetter(t),zGetter(t),weightGetter(t)); 
            }
        ,histoargs...);
    }

template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TEfficiency,TreeType> fillEfficiency(
    std::function<bool(TreeType &)> matchGetter, 
    std::function<double(TreeType &)> xGetter, 
    args && ... histoargs){
        return PlotFillInstructionWithRef<TEfficiency,TreeType>(
            [matchGetter,xGetter](TEfficiency* h, TreeType &t){
                h->Fill(matchGetter(t), xGetter(t)); 
            }
        ,histoargs...);
    }
    
template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TEfficiency,TreeType> fillEfficiency2D(
    std::function<bool(TreeType &)> matchGetter, 
    std::function<double(TreeType &)> xGetter, 
    std::function<double(TreeType &)> yGetter,
    args && ... histoargs){
        return PlotFillInstructionWithRef<TEfficiency,TreeType>(
            [matchGetter,xGetter,yGetter](TEfficiency* h, TreeType &t){
                h->Fill(matchGetter(t), xGetter(t), yGetter(t)); 
            }
        ,histoargs...);
    }

template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TProfile,TreeType> fillProfile(
    std::function<double(TreeType &)> xGetter, 
    std::function<double(TreeType &)> yGetter, 
    std::function<double(TreeType &)> weightGetter, 
    args && ... histoargs){
        return PlotFillInstructionWithRef<TProfile,TreeType>(
            [xGetter,yGetter,weightGetter](TProfile* h, TreeType &t){
                h->Fill(xGetter(t), yGetter(t),weightGetter(t)); 
            }
        ,histoargs...);
    }
    
template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TProfile2D,TreeType> fillProfile2D(
    std::function<double(TreeType &)> xGetter, 
    std::function<double(TreeType &)> yGetter, 
    std::function<double(TreeType &)> zGetter, 
    std::function<double(TreeType &)> weightGetter, 
    args && ... histoargs){
        return PlotFillInstructionWithRef<TProfile2D,TreeType>(
            [xGetter,yGetter,weightGetter,zGetter](TProfile2D* h, TreeType &t){
                h->Fill(xGetter(t), yGetter(t),zGetter(t),weightGetter(t)); 
            }
        ,histoargs...);
    }


// //// as above, but for element-wise access

template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TH1D,TreeType> fillTH1DFromList(
    std::function<size_t(TreeType&)> elementCounter, 
    Selection<TreeType> selector, 
    std::function<double(TreeType&, size_t)> xGetter, 
    std::function<double(TreeType&, size_t)> weightGetter, 
    args&&  ... histoargs){
        return PlotFillInstructionWithRef<TH1D,TreeType>(
            elementCounter,
            selector,
            [xGetter,weightGetter](TH1D* h, TreeType &t,size_t i){
                h->Fill(xGetter(t,i),weightGetter(t,i)); 
            }
        , histoargs...);
    }
    
template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TH2D,TreeType> fillTH2DFromList(
    std::function<size_t(TreeType&)> elementCounter, 
    Selection<TreeType> selector, 
    std::function<double(TreeType &, size_t)> xGetter, 
    std::function<double(TreeType &, size_t)> yGetter, 
    std::function<double(TreeType &, size_t)> weightGetter, 
    args && ... histoargs){
        return PlotFillInstructionWithRef<TH2D,TreeType>(
            elementCounter,
            selector,
            [xGetter,yGetter,weightGetter](TH2D* h, TreeType &t,size_t i){
                h->Fill(xGetter(t,i),yGetter(t,i),weightGetter(t,i)); 
            }
        , histoargs...);
    }
    
template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TH3D,TreeType> fillTH3DFromList(
    std::function<size_t(TreeType&)> elementCounter, 
    Selection<TreeType> selector, 
    std::function<double(TreeType &, size_t)> xGetter, 
    std::function<double(TreeType &, size_t)> yGetter, 
    std::function<double(TreeType &, size_t)> zGetter, 
    std::function<double(TreeType &, size_t)> weightGetter, 
    args && ... histoargs){
        return PlotFillInstructionWithRef<TH3D,TreeType>(
            elementCounter,
            selector,
            [xGetter,yGetter,zGetter,weightGetter](TH3D* h, TreeType &t,size_t i){
                h->Fill(xGetter(t,i),yGetter(t,i),zGetter(t,i),weightGetter(t,i)); 
            }
        , histoargs...);

    }

template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TEfficiency,TreeType> fillEfficiencyFromList(
    std::function<size_t(TreeType&)> elementCounter, 
    Selection<TreeType> selector, 
    std::function<bool(TreeType &, size_t)> matchGetter, 
    std::function<double(TreeType &, size_t)> xGetter,
    args && ... histoargs){
        return PlotFillInstructionWithRef<TEfficiency,TreeType>(
            elementCounter,
            selector,
            [matchGetter,xGetter](TEfficiency* h, TreeType &t,size_t i){
                h->Fill(matchGetter(t,i), xGetter(t,i)); 
            }
        , histoargs...);

    }
    
template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TEfficiency,TreeType> fillEfficiency2DFromList(
    std::function<size_t(TreeType&)> elementCounter, 
    Selection<TreeType> selector, 
    std::function<bool(TreeType &, size_t)> matchGetter, 
    std::function<double(TreeType &, size_t)> xGetter, 
    std::function<double(TreeType &, size_t)> yGetter,
    args && ... histoargs){
        return PlotFillInstructionWithRef<TEfficiency,TreeType>(
            elementCounter,
            selector,
            [matchGetter,xGetter,yGetter](TEfficiency* h, TreeType &t,size_t i){
                h->Fill(matchGetter(t,i), xGetter(t,i),yGetter(t,i)); 
            }
        , histoargs...);
    }

template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TProfile,TreeType> fillProfileFromList(
    std::function<size_t(TreeType&)> elementCounter, 
    Selection<TreeType> selector, 
    std::function<double(TreeType &, size_t)> xGetter, 
    std::function<double(TreeType &, size_t)> yGetter, 
    std::function<double(TreeType &, size_t)> weightGetter, 
    args && ... histoargs){
        return PlotFillInstructionWithRef<TProfile,TreeType>(
            elementCounter,
            selector, 
            [xGetter,yGetter,weightGetter](TProfile* h, TreeType &t, size_t i){
                h->Fill(xGetter(t,i), yGetter(t,i),weightGetter(t,i)); 
            }
        ,histoargs...);
    } 
    
template <class TreeType, typename... args> 
PlotFillInstructionWithRef<TProfile2D,TreeType> fillProfile2DFromList(
    std::function<size_t(TreeType&)> elementCounter, 
    Selection<TreeType> selector, 
    std::function<double(TreeType &, size_t)> xGetter, 
    std::function<double(TreeType &, size_t)> yGetter, 
    std::function<double(TreeType &, size_t)> zGetter, 
    std::function<double(TreeType &, size_t)> weightGetter, 
    args && ... histoargs){
        return PlotFillInstructionWithRef<TProfile2D,TreeType>(
            elementCounter,
            selector, 
            [xGetter,yGetter,zGetter,weightGetter](TProfile2D* h, TreeType &t, size_t i){
                h->Fill(xGetter(t,i), yGetter(t,i),zGetter(t,i),weightGetter(t,i)); 
            }
        ,histoargs...);
    } 
