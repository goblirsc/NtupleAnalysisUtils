
inline bool ISortableByID::operator < (const ISortableByID & other) const { 
    return (getID() < other.getID());
} 
inline bool ISortableByID::operator == (const ISortableByID & other) const { 
    return (getID() == other.getID());
} 
inline bool ISortableByID::operator > (const ISortableByID & other) const { 
    return (getID() > other.getID());
} 