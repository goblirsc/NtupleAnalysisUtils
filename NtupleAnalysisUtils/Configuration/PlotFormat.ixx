template <size_t N> ColorPalette::ColorPalette(std::array<Double_t, N> red,
                                   std::array<Double_t, N> green,
                                   std::array<Double_t, N> blue,
                                   std::array<Double_t, N> step_length,
                                   unsigned int color_contours):
    m_contour{color_contours} {

        const Int_t FI = TColor::CreateGradientColorTable(red.size(), step_length.data(), red.data(), green.data(), blue.data(), color_contours);
        m_color_code.resize(color_contours);
        for (unsigned int i = 0; i < color_contours; ++i) m_color_code[i] = FI + i;
    }                            



template <class Histo> void PlotFormat::applyTo(Histo *H) const{
    if (!H) return; 
    
    if (FillStyle.isUserSet()){
        H->SetFillStyle(FillStyle());
        /// The fill color takes precedence over the general color 
        /// if both are set
        const Color_t fillCol = FillColor.isUserSet() ? FillColor() : Color(); 
        if (FillAlpha.isUserSet()) H->SetFillColorAlpha(fillCol, FillAlpha());  
        else H->SetFillColor(fillCol);
    }
    
    const Color_t markerCol = MarkerColor.isUserSet() ? MarkerColor() : Color(); 
    /// The marker color takes precedence over the general color 
    /// if both are set
    if (MarkerAlpha.isUserSet()) H->SetMarkerColorAlpha(markerCol, MarkerAlpha());
    else if(MarkerColor.isUserSet() || Color.isUserSet()) H->SetMarkerColor(markerCol);
    if (MarkerStyle.isUserSet()) H->SetMarkerStyle(MarkerStyle()); 
    if (MarkerScale.isUserSet() || MarkerSize.isUserSet()) H->SetMarkerSize(MarkerScale() * MarkerSize()); 

    const Color_t lineCol = LineColor.isUserSet() ? LineColor() : Color(); 
    /// The line color takes precedence over the general color 
    /// if both are set
    if (LineAlpha.isUserSet()) H->SetLineColorAlpha(lineCol, LineAlpha());
    else if (LineColor.isUserSet() || Color.isUserSet()) H->SetLineColor(lineCol);
    if (LineStyle.isUserSet()) H->SetLineStyle(LineStyle());
    if (LineWidth.isUserSet()) H->SetLineWidth(LineWidth());

}
