template <typename T, typename Parent> UserSetting<T,Parent>::UserSetting(const T& initialVal,Parent* parent): 
    m_isUserSet(false),
    m_val(initialVal),
    m_parent(parent){
}

template <typename T, typename Parent> UserSetting<T,Parent>::UserSetting(const UserSetting<T,Parent> & other,Parent* newParent): 
    m_isUserSet(other.isUserSet()),
    m_val(other()),
    m_parent(newParent){
}

template <typename T, typename Parent> void UserSetting<T,Parent>::copyFrom(const UserSetting<T,Parent> & other,Parent* newParent){
    m_isUserSet = other.isUserSet();
    m_val = other();
    m_parent = newParent; 
}

template <typename T, typename Parent> Parent & UserSetting<T,Parent>::operator()(const T& t){
    m_val = t; 
    m_isUserSet = true; 
    return *m_parent;
}

template <typename T, typename Parent> Parent & UserSetting<T,Parent>::operator()(const UnsetUserSetting& ){
    return reset(); 
}
template <typename T, typename Parent> const T& UserSetting<T,Parent>::operator()() const {
    return m_val;
}

template <typename T, typename Parent> Parent & UserSetting<T,Parent>::reset(){
    m_isUserSet = false; 
    return *m_parent;
}

template <typename T, typename Parent> T& UserSetting<T,Parent>::modify() {
    return m_val;
}
template <typename T, typename Parent> bool UserSetting<T,Parent>::isUserSet() const {
    return m_isUserSet;
}


template <typename T, class Parent> CustomUserSettings<T, Parent>::CustomUserSettings(Parent* parent):
    m_parent(parent){
}

template <typename T, typename Parent> CustomUserSettings<T,Parent>::CustomUserSettings(const CustomUserSettings<T,Parent> & other,Parent* newParent): 
    m_userVars(other.m_userVars),
    m_parent(newParent){
}

template <typename T, typename Parent> void CustomUserSettings<T,Parent>::copyFrom(const CustomUserSettings<T,Parent> & other,Parent* newParent){
    m_userVars = other.m_userVars; 
    m_parent = newParent; 
}

template <typename T, class Parent> Parent & CustomUserSettings<T, Parent>::operator() (const std::string & key, const T & value){
    if (!m_userVars.emplace(key,value).second) m_userVars.at(key) = value; 
    return *m_parent; 
}

template <typename T, class Parent> Parent & CustomUserSettings<T, Parent>::operator()(const std::string & key, UnsetUserSetting){
    m_userVars.erase(key); 
    return *m_parent;
}


template <typename T, class Parent> bool CustomUserSettings<T, Parent>::get(const std::string & key, T & value) const {
    auto found = m_userVars.find(key); 
    if (found != m_userVars.end()) {
        value = found->second; 
        return true; 
    }
    return false; 
} 
