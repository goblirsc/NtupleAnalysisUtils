#ifndef NTAU__DRAWOBJECTFORMAT__H
#define NTAU__DRAWOBJECTFORMAT__H

#include "NtupleAnalysisUtils/Configuration/UserSetting.h" 

/// Config class representing the format of drawn ROOT 
/// objects such as TLatex, TLine, TMarker and friends.
/// 
/// This is passed as an argument to most "on the fly" drawing
/// methods of NtupleAnalysisUtils
class DrawObjectFormat{
    public:
        DrawObjectFormat() = default; 
        DrawObjectFormat(const DrawObjectFormat & other); 
        void operator= (const DrawObjectFormat & other); 
        /// Steers the size of drawn objects.
        /// If not user-configured, when drawing text this 
        /// will be ignored in favour of gStyle->GetTextSize()
        UserSetting<int,    DrawObjectFormat>   Size {1,this}; 
        /// Text font
        UserSetting<int,    DrawObjectFormat>   Font {43,this}; 
        /// Alignment 
        UserSetting<int,    DrawObjectFormat>   Align{11,this}; 
        /// colour 
        UserSetting<Color_t,DrawObjectFormat>   Color{kBlack,this}; 
        /// NDC coordinate option (true: pad coords, false: axis coords)
        UserSetting<bool,   DrawObjectFormat>   NDC  {true,this}; 
        /// object style (for example marker style or line style) 
        UserSetting<int,    DrawObjectFormat>   Style{1,this}; 
        /// line width
        UserSetting<int,    DrawObjectFormat>   Width{2,this}; 
};



#endif // NTAU__DRAWOBJECTFORMAT__H