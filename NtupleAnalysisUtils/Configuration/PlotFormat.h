#ifndef NTAU__PLOTFORMAT__H
#define NTAU__PLOTFORMAT__H 

#include "TColor.h"
#include "TAttLine.h"
#include "TAttFill.h"
#include "TAttMarker.h"
#include "TStyle.h"
#include <string> 
#include <map> 
#include <vector>
#include "NtupleAnalysisUtils/Configuration/UserSetting.h"


/// Small helper class to define a custom Color Palette and pipe it to the PlotFormat
class ColorPalette{
    public:
        /// Tuple which RGB value shall be set a given point on the scala ranging between 0 - 1. Each color is encoded on the interval 0 to 1.
        struct ColorPoint{
            float red{0.f};
            float green{0.f};
            float blue{0.f};
            float location{0.f};
        };
        
        ColorPalette(const std::vector<ColorPoint>& color_steps,                
                    unsigned int color_contours);
        /// Constructor using a predefined TColor e.g. kViridis
        ColorPalette(Color_t color_code, unsigned int color_contours = 2000);
        
        
        /// ROOT-like constructor using std::arrays  
        template <size_t N> ColorPalette(std::array<Double_t, N> red,
                                         std::array<Double_t, N> green,
                                         std::array<Double_t, N> blue,
                                         std::array<Double_t, N> step_length,
                                         unsigned int color_contours);
        /// Default constructor
        ColorPalette() = default;
        /// Copy constructor
        ColorPalette(const ColorPalette&) = default;
        /// Move constructor
        ColorPalette( ColorPalette&&) = default;
        /// Assignment operator
        ColorPalette& operator=(const ColorPalette&) = default;
        /// Move operator
        ColorPalette& operator=(ColorPalette&&) = default;

        /// Returns whether the object is properly set. I.e.
        /// the color vectors & step size have the same size and the 
        /// color gradient is greater than 1
        bool isValid() const;
        
        /// Applies the Color Palette
        void setPalette();
        /// Returns the color Contour
        unsigned int getContour() const;
        /// Returns the color code
        Color_t getColorCode() const;

    private:
        std::vector<Int_t> m_color_code{};
        unsigned int m_contour{0};        
        Color_t m_color_choice{0};
        

};


/// class to package the desired formatting of a ROOT object (typically, a histogram). 
/// will then be auto-applied in one go if desired. 
/// Designed to easily chain format instructions, for example, 
/// PlotFormat().Color(kBlue).MarkerStyle(kFullDotLarge).applyTo(myHistogram);
class PlotFormat{
public:

    /// C-tor starts with an empty format (no user-settings)
    PlotFormat() = default;

    PlotFormat(const PlotFormat & other); 
    PlotFormat& operator=(const PlotFormat & other);  

    PlotFormat Copy(){return PlotFormat(*this);}    // copy method, for easily obtaining a modified copy  

    /// format a given ROOT object according to this object
    /// Please see below for the supported settings. 
    template <class Histo> void applyTo(Histo* H) const;

    ///===================================================
    /// The following is the set of NTAU-supported settings 
    /// These are used for auto-formating the object when drawing
    /// and for creating legends etc. 
    ///
    /// Please see the UserSetting.h header for documentation
    /// on their usage.  
    ///===================================================

    /// A global color setting - overriden by the specific ones below 
    /// if those are also set 
    UserSetting<Color_t, PlotFormat> Color{gStyle->GetMarkerColor(),this}; 
    /// Setting color palette for 2D hisotgrams
    UserSetting<ColorPalette, PlotFormat> paletteFor2DPlotting{{}, this};
    /// Specific settings for fill, marker and line colors. Each can be combined with an alpha value below
    UserSetting<Color_t, PlotFormat> FillColor{gStyle->GetFillColor(),this};
    UserSetting<Color_t, PlotFormat> MarkerColor{gStyle->GetMarkerColor(), this};
    UserSetting<Color_t, PlotFormat> LineColor{gStyle->GetLineColor(), this};

    /// Alpha settings for fill,marker,line
    UserSetting<double, PlotFormat> FillAlpha{1.,this};
    UserSetting<double, PlotFormat> MarkerAlpha{1.,this};
    UserSetting<double, PlotFormat> LineAlpha{1.,this};

    /// Style for fill,marker,line
    UserSetting<Style_t, PlotFormat> FillStyle{0,this};
    UserSetting<Style_t, PlotFormat> MarkerStyle{gStyle->GetMarkerStyle(),this};
    UserSetting<Style_t, PlotFormat> LineStyle{gStyle->GetLineStyle(), this};

    /// Size for marker and width for line. 
    /// For the marker, we can also apply a relative scaling
    UserSetting<Size_t, PlotFormat> MarkerSize{gStyle->GetMarkerSize(),this};
    UserSetting<double, PlotFormat> MarkerScale{1.,this};
    UserSetting<Width_t, PlotFormat> LineWidth{gStyle->GetLineWidth(), this};

    /// Additional parameters to pass to the Draw() method
    UserSetting<std::string, PlotFormat> ExtraDrawOpts{"",this};
    /// Legend settings - draw mode and title to use
    UserSetting<std::string, PlotFormat> LegendOption{"PL",this};
    UserSetting<std::string, PlotFormat> LegendTitle{"",this};

    /// when drawing stack plots, can be used to tell something should go onto a stack.
    UserSetting<bool, PlotFormat> ShouldGoOnStack{false,this};
    
    /// ============================================
    /// These are holders for any custom information the user may 
    /// wish to attach to the format object. 
    /// This will not be used by NtupleAnalysisUtils directly,
    /// but can be useful to package information 
    /// for user-code in the same 
    /// fashion as xAOD decorations would. 
    /// ============================================
    CustomUserSettings<int, PlotFormat> CustomInt{this}; 
    CustomUserSettings<double, PlotFormat> CustomFloat{this}; 
    CustomUserSettings<std::string, PlotFormat> CustomString{this}; 

    /// does this format have any user-specified settings? 
    bool hasUserSettings() const {return m_hasUserSettings;}
    void setHasUserSettings(){m_hasUserSettings=true;}


private:

    // check if anything was set by the user
    bool m_hasUserSettings{false}; 
};


// here, we define a few premade plot formats that the user can work with 
namespace PremadePlotFormats{
    PlotFormat DataMarkers();

    PlotFormat Markers1();
    PlotFormat Markers2();
    PlotFormat Markers3();
    PlotFormat Markers4();
    PlotFormat Markers5();
    PlotFormat Markers6();

    PlotFormat Lines1();
    PlotFormat Lines2();
    PlotFormat Lines3();
    PlotFormat Lines4();
    PlotFormat Lines5();
    PlotFormat Lines6();

    // 10 formats using the ATLAS color palette. 
    // Defined up to i = 10
    PlotFormat AtlasMark(size_t i); 
    PlotFormat AtlasLine(size_t i);
    PlotFormat AtlasFill(size_t i); 

}


#include "PlotFormat.ixx"

#endif
