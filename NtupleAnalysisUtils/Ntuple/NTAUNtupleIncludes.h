#ifndef NTAU_Ntuple__INCLUDES
#define NTAU_Ntuple__INCLUDES

/// Top level header for the headers within the Ntuple part of NTAU

#include "NtupleAnalysisUtils/Ntuple/NtupleBranchAccessByName.h"
#include "NtupleAnalysisUtils/Ntuple/DerivedVirtualBranch.h"

#endif