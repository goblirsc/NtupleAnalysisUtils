#ifndef NTAU_NTUPLEBRANCHBASE__H
#define NTAU_NTUPLEBRANCHBASE__H

#include "TTree.h"
#include "TBranch.h"
#include "TRandom3.h"
#include "TLeaf.h"
#include "vector"
#include <exception>
#include <iostream>
#include <memory>
#include <iomanip>
#include <iostream>
#include <optional>

/// The NtupleBranch class provides an interface 
/// to the contents of ROOT trees which removes the need for the 
/// usual boilerplate code in connecting branches.
///
/// It also handles automatic branch status handling to significantly 
/// speed up I/O. 
///
/// Here, we declare an abstract base class of all concrete NtupleBranch implementations. 
/// The concrete implementations are templated to most branch types 
/// and found in the NtupleBranch.h header. Common functionality is implemented here. 
///
/// In addition to read operations, it can also be used to write information into a tree. 
/// Again, all the boilerplate code is handled for us 

/// forward declare the branch manager, which represents a tree as a whole. 
class NtupleBranchMgr;

/// also forward-declare the templated ntuple branch class
template <typename T> class NtupleBranch; 

/// The NtupleBranchBase itself
class NtupleBranchBase{
public:

    /// Exception thrown when we attempt to access an invalid branch
    class ExcAccessInvalidBranch : public std::runtime_error{
        public:
           ExcAccessInvalidBranch(const std::string & what): std::runtime_error(what){}
    }; 

    /// Constructor. 
    /// The first argument set the name of the branch to be read or written. 
    /// The second specifies a ROOT Tree to write to or read from. 
    /// The third makes the branch optionally connect itself to a branch manager. 
    /// This is usually convenient for analysis-level use, as the manager object
    /// can be passed around as a variable / method argument, rather than a large number
    /// of individual branches. 
    NtupleBranchBase(const std::string & branchName, TTree* tree, NtupleBranchMgr* mgr = nullptr);
    virtual ~NtupleBranchBase() =default;

    /// =======================================
    /// Methods used for reading from a tree. 
    /// 
    /// Note that access to the actual content is preferably
    /// carried out using the templated NtupleBranch classes. 
    /// =======================================

    /// Switch to a certain entry. 
    /// This method will not necessarily read data. It primarily sets the internal 
    /// entry counter to the new value. Only if the branch is already connected in read mode,
    /// it will also lead to I/O by reading from the connected tree. 
    /// Returns the number of bytes read. Zero is not indicative of an issue. 
    inline Int_t getEntry(const Long64_t & entry); 


    /// Generic access to tree content. 
    /// This is a fallback method, and it is recommended to use the specialised
    /// NtupleBranch templates or the NtupleBrancAccessByName for this. 
    /// Comes at the overhead of a dynamic_cast. 
    /// Will set the argument to the current branch content IF it is of the correct type.
    /// Returns whether or not read operation was successful. 
    template<typename T> bool operator()(T & t);

    /// another set of generic accessors. This allows to access content 
    /// as a floating value (if conversion is possible), to allow comparing 
    /// branches of types not known to the user. Use with care. 
    virtual double asDouble(size_t i =0) =0; 

    /// ask for the size of the contained element. 
    /// Will be 1 for scalars or length-1 arrays, 
    /// or equivalent to the number of entries for larger arrays / vectors.
    virtual size_t size() =0;

    /// ============================================= 
    /// Methods used for writing into a tree 
    /// 
    /// Again, we suggest to use the specialised 
    /// templates for the day-to-day work. 
    /// =============================================

    /// Set the value of the branch to the content of another branch when writing output.
    /// Very convenient for copying in postprocessing jobs 
    /// This relies on both branches having the same type. 
    virtual void set (NtupleBranchBase*  in)=0;

    /// another copy setter. This one finds the branch to copy from by name
    /// within an NtupleBranchMgr instance (see respective header). 
    /// Useful for post-processing jobs. 
    void set (NtupleBranchMgr & manager, const std::string & branchToCopy);

    /// set the branch content to a dummy value. 
    /// Used to reset this branch. 
    /// The dummy value is specified in the specific template implementations.
    virtual void setDummy()=0;

    /// ====================================
    /// Methods to retrieve information about the branch 
    /// ====================================

    /// get the branch name
    inline const std::string & getName() const; 
    // get the tree we are reading from or writing to 
    TTree* getTree() const;
    // get the TBranch we are connected to
    inline TBranch* getBranch() const;
    /// check the current connection status
    inline bool isConnectedRead() const; 
    inline bool isConnectedWrite() const; 

    /// ====================================
    /// Methods to update the branch
    /// ====================================
    /// Set the tree we point to
    virtual void setTree(TTree* t);
    /// set the tree cache size 
    static void setTreeCacheSize(float size_in_MB);
    /// set the basket size
    static void setBasketSize(float size_in_MB);
    /// Helper method to detach the branch from the tree.  
    void detach();

    /// ====================================
    /// Helper methods
    /// ====================================
    
    /// Check if the connected tree (already) has a branch of the name this object
    /// is looking for. Also works if we are not yet connected for reading/writing.  
    /// Does not check for matching type. 
    bool existsInTree();

    /// helpers for C++ source code generation: 

    /// This generates a declaration line for this branch. 
    /// Useful for example when auto-generating branch managers for
    /// arbitrary ntuples. 
    virtual std::string generateMemberString() const;

    /// This generates an initialisation line for this branch. 
    /// Used when auto-generating branch managers for
    /// arbitrary ntuples. 
    /// the "paddingWidth" is uses to nicely align the branches 
    /// in a block of init calls, purely visual purpose. 
    virtual std::string generateInitString(unsigned int paddingWidth=20) const;

protected:

    /// This enum represents the connection status
    /// of a branch. 
    /// It can either be disconnected, or attached, 
    /// either for reading or writing. 
    typedef enum {
        disconnected,
        connectedRead,
        connectedWrite
    } connectionStatus; 
    
    /// helper methods, used to obtain the correct strings to generate 
    /// C++ code for ROOT branch access
    template <typename T> static std::string branchString();
    template <typename T> static std::string memberTypeString();
    /// Dummy value for a specific type. Used to reset branch contents. 
    template <typename T> static T dummyVal();

    template <typename T> static double floatify(const T& t); 
    template <typename T> static double floatify(const std::vector<T>& t); 
    static double floatify(const std::string & t); 

    /// main methods for connecting to a tree for reading and writing, 
    /// and for disconnecting again

    /// connect for reading if not yet connected - called during read operations 
    void connectIfNeeded();

    /// connect to output tree for writing if not yet connected - called during
    /// set operations 
    void attachIfNeeded();

    /// print methods for warnings 
    void warnReadFailedConnect();
    void warnWriteFailedConnect();

    /// Implementation of the connection attempts 
    void attemptConnect();
    void attemptAttach();
    void disconnect();

    /// finally, two methods specific to the payload type.
    /// Overloaded in the concrete implementations. 

    /// for reading: 
    virtual void attachReadPayload() = 0;
    /// and for writing
    virtual TBranch* addPayloadAsBranch() = 0;
public:
    /// get the current (internal) entry number to which this brach is set. 
    /// This allows the internal counter to be overriden by an external
    /// manager if needed. 
    Long64_t getCurrEntry() const;
    
protected:
    /// =========================
    /// data members
    /// =========================

    /// the name of the branch we represent 
    const std::string m_branchName; 
    /// the tree we are connected to
    TTree* m_tree{nullptr};
    /// the branch we may be connected to
    TBranch* m_branch{nullptr};
    /// a branch manager we may be connected to
    NtupleBranchMgr* m_mgr{nullptr};
    /// Current entry to which the branch shall point to
    Long64_t m_currEntry{0};

    /// Our current connection status 
    connectionStatus m_connectionStatus{disconnected};
    /// another branch we might be set up to copy from. 
    NtupleBranchBase* m_copyFrom{nullptr};
 
    /// logging for connection attempts
    int m_n_warnings{0};
    static const int m_n_maxWarnings{1}; 
    /// flags on whether we attempted to attach 
    /// or connect already
    bool m_attempted_connect{false};
    bool m_attempted_attach{false};

    /// a flag whether we are being managed by a branch
    /// manager
    bool m_isManaged{false};

    std::optional<bool> m_exists_in_tree{std::nullopt}; 

    /// The cache and write basket sizes 
    static Long64_t s_treeCacheSize;
    static Int_t s_write_basketsize;

};


#include "NtupleBranchBase.ixx"

#endif //NTAU_NTUPLEBRANCHBASE__H
