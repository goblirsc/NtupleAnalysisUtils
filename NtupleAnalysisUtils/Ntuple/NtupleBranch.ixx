#include <iostream> 
#include <mutex> 


////////////////////////////////
//// Next: NtupleBranch<<branchType>
//////////////////////////////////
template<typename branchType> NtupleBranch<branchType>::NtupleBranch(const std::string & branchName, 
                                                                     TTree* tree, 
                                                                     NtupleBranchMgr* mgr):
    NtupleBranchBase(branchName, tree,mgr){
    if (m_tree && m_tree->GetDirectory()->GetFile() != nullptr){
        /// if we are linked to a tree, set up the tree cache as specified for this branch
        if (m_tree->GetCacheSize() != s_treeCacheSize){
            m_tree->SetCacheSize(s_treeCacheSize);           
        }
    }

}
template<typename branchType> void NtupleBranch<branchType>::set (NtupleBranchBase*  in){
    
    /// Try to attach for writing. 
    attachIfNeeded();
    /// if an invalid input was provided, bail out
    if (!in){
        std::cerr << "Failed to read the input branch when setting "<<getName()<< std::endl;
    }
    /// try to cast to the type we can consume
    NtupleBranch<branchType>* cast = dynamic_cast<NtupleBranch<branchType>*>(in);
    /// cast successful - readable! 
    if (cast){
        /// copy the content
        m_content = cast->get();
    }
    /// not compatible - bail out
    else {
        std::cerr << " Failed to correctly cast the branch "<<getName()<<"in NtupleBranch::set! "<<std::endl;
    }
}

template<typename branchType> inline const branchType & NtupleBranch<branchType>::operator() () {
    return get();
}
template<typename branchType> inline const branchType & NtupleBranch<branchType>::get(){
    /// attach in read mode if not yet attached
    connectIfNeeded(); 
    /// then return our content
    return m_content;
}

template<typename branchType> double NtupleBranch<branchType>::asDouble(size_t){
   return floatify(get());
}

template<typename branchType> inline const branchType & NtupleBranch<branchType>::readOnce (){
    /// first connect
    connectIfNeeded(); 
    /// then disconnect, leaving our content populated 
    disconnect(); 
    /// now we can return the content
    return m_content;
}
template<typename branchType> inline void NtupleBranch<branchType>::set (branchType  in){
    /// attach in write mode if not yet done
    attachIfNeeded(); 
    /// and update our content
    m_content=in;
}
template<typename branchType> inline void NtupleBranch<branchType>::set (NtupleBranch<branchType> &  in){
    /// attach in write mode if not yet done
    attachIfNeeded();
    /// then write the content of the input branch into our data member
    m_content=in();
}
template<typename branchType> inline void NtupleBranch<branchType>::setDummy (){
    /// attach in write mode if not yet done
    attachIfNeeded(); 
    /// then write a dummy value into our data member
    m_content = dummyVal<branchType>();
}
template<typename branchType> inline size_t NtupleBranch<branchType>::size() {
    return 1;
}
template<typename branchType> std::string NtupleBranch<branchType>::generateMemberString() const{
    /// build a string declaring this branch for C++ code generation
    std::stringstream ss; 
    ss << "NtupleBranch <"
    <<std::setw(30)<< std::left 
    << this->memberTypeString<branchType>()+">"
    <<m_branchName;
    return ss.str();
}
template<typename branchType> void NtupleBranch<branchType>::attachReadPayload(){
    /// simple for scalar types
    if (m_branch) m_branch->SetAddress(&m_content);
}

template<typename branchType> TBranch* NtupleBranch<branchType>::addPayloadAsBranch(){
    return (m_tree ? m_tree->Branch(m_branchName.c_str(), &m_content) : nullptr);
}


////////////////////////////////
//// Next: NtupleBranch<std::vector<branchType> > 
//////////////////////////////////

template<typename branchType> NtupleBranch<std::vector<branchType> >::NtupleBranch(const std::string & branchName, TTree* tree, NtupleBranchMgr* mgr):
    NtupleBranchBase(branchName, tree,mgr),
    m_content(nullptr),
    m_cleanupHelper(std::make_shared<std::vector<branchType>>()){   
    if (m_tree && m_tree->GetDirectory()->GetFile() != nullptr){
        /// if we are linked to a tree, set up the tree cache as specified for this branch
        if (m_tree->GetCacheSize() != s_treeCacheSize){
            m_tree->SetCacheSize(s_treeCacheSize);    
        }
    }
}

template<typename branchType> NtupleBranch<std::vector<branchType> >::~NtupleBranch(){
    m_content = nullptr;
}
template<typename branchType> inline const std::vector<branchType> & NtupleBranch<std::vector<branchType> >::operator() () {
    return get();
}
template<typename branchType> inline const std::vector<branchType> & NtupleBranch<std::vector<branchType> >::get() {
    /// connect for reading if not yet connected
    connectIfNeeded(); 
    /// return the vector 
    return *m_content;
}
template<typename branchType> inline branchType NtupleBranch<std::vector<branchType> >::operator() (size_t i) {
    return get(i);
}
template<typename branchType> double NtupleBranch<std::vector<branchType> >::asDouble(size_t i){     
    return floatify(get(i));
}


template<typename branchType> inline branchType NtupleBranch<std::vector<branchType> >::get(size_t i) {
    /// connect for reading if not yet connected
    connectIfNeeded(); 
    /// use vector::at for integrated bounds-checking
    return m_content->at(i);
}
template<typename branchType> 
    inline const typename std::vector<branchType>::const_iterator NtupleBranch<std::vector<branchType> >::begin(){
    return get().begin();
}
template<typename branchType> 
    inline const typename std::vector<branchType>::const_iterator NtupleBranch<std::vector<branchType> >::end(){
    return get().end();
}
template<typename branchType> 
    const std::vector<branchType> & NtupleBranch<std::vector<branchType> >::readOnce(){
        /// first connect for reading if needed
        connectIfNeeded(); 
        /// then drop the connection (the current branch entry is stored in our member) 
        disconnect(); 
        /// return what we read. 
        return *m_content;
}
template<typename branchType> 
    inline void NtupleBranch<std::vector<branchType> >::set(const std::vector<branchType> & in){
        /// attach for writing if needed
        attachIfNeeded(); 
        /// copy the vector into our output
        *m_content = in;
}
template<typename branchType> 
    inline void NtupleBranch<std::vector<branchType> >::set(NtupleBranch<std::vector<branchType> > &  in){
        /// attach for writing if needed
        attachIfNeeded(); 
        /// perform a vector copy of the input's content into our output vector
        *m_content = in();
}
template<typename branchType> 
    inline std::vector<branchType> & NtupleBranch<std::vector<branchType> >::outVec(){
        /// attach for writing if needed
        attachIfNeeded(); 
        /// return the branch as a ref
        return *m_content;
}
template<typename branchType> 
    inline void NtupleBranch<std::vector<branchType> >::push_back(const branchType& t){
        /// attach for writing if needed
        attachIfNeeded();
        /// perform a push_back on our output vector
        m_content->push_back(t); 
}
template<typename branchType> 
    inline void NtupleBranch<std::vector<branchType> >::clear(){
        /// attach for writing if needed
        attachIfNeeded();
        /// perform a clear() on our output vector
        m_content->clear();
}

template<typename branchType> 
    size_t NtupleBranch<std::vector<branchType> >::size(){
        /// connect for reading if not yet connected
        connectIfNeeded(); 
        /// then return the size of our vector
        return m_content->size();
}
template<typename branchType> inline void NtupleBranch<std::vector<branchType> >::setDummy (){ 
    /// connect for writing if not yet done
    attachIfNeeded(); 
    /// and clear our vector
    m_content->clear();
}
template<typename branchType> 
    std::string NtupleBranch<std::vector<branchType> >::generateMemberString() const {
        std::stringstream ss; 
        ss << "NtupleBranch <"
        <<std::setw(30)<< std::left
        << "std::vector < "+this->memberTypeString<branchType>()+">>"
        <<m_branchName;
        return ss.str();
}

template<typename branchType> void NtupleBranch<std::vector<branchType> >::set(size_t index, const branchType & in){
    /// connect for writing if not yet done
    attachIfNeeded();
    /// use the std::vector's bound-check 
    m_content->at(index) = in;
}
template<typename branchType> void NtupleBranch<std::vector<branchType> >::set(NtupleBranchBase*  in){
    /// connect for writing if not yet done
    attachIfNeeded();
    /// if an invalid input was provided, bail out
    if (!in){
        std::cerr << "Failed to read the input branch when setting "<<getName()<< std::endl;
    }
    /// try to cast the input branch to our own type
    NtupleBranch< std::vector<branchType>>* cast = dynamic_cast<NtupleBranch<std::vector<branchType>>*>(in);
    /// compatible --> perform a vector copy into our output vec
    if (cast){
        *m_content = cast->get();
    }
    /// incompatible --> bail out!
    else {
        std::cerr << " Failed to correctly cast the branch in NtupleBranch::set! "<<std::endl;
    }
}
template<typename branchType> 
    void NtupleBranch<std::vector<branchType> >::attachReadPayload(){
        m_content = nullptr;
        /// need to show ROOT the address of our pointer
        if (m_branch) m_branch->SetAddress(&m_content);
}
    /// attach our vector payload for writing
template<typename branchType> 
    TBranch* NtupleBranch<std::vector<branchType> >::addPayloadAsBranch(){
        if (!m_content) m_content = m_cleanupHelper.get();
        return (m_tree ? m_tree->Branch(m_branchName.c_str(), m_content) : nullptr);
}

////////////////////////////////
//// Next: NtupleBranch<arrBranchType* > 
//////////////////////////////////

template<typename arrBranchType> NtupleBranch<arrBranchType*>::NtupleBranch(const std::string & branchName, size_t length, TTree* tree, NtupleBranchMgr* mgr):
    NtupleBranchBase(branchName, tree, mgr),
    m_content(0),
    m_length(length){
    if (m_tree && m_tree->GetDirectory()->GetFile() != nullptr){
        /// if a tree was provided, set the cache properties of the tree accordingly
        if (m_tree->GetCacheSize() != s_treeCacheSize){
            m_tree->SetCacheSize(s_treeCacheSize);    
        }
    }
    m_content = new arrBranchType[m_length];
}
template<typename arrBranchType> NtupleBranch<arrBranchType*>::~NtupleBranch(){
    if ( m_content) delete m_content;
}
template<typename arrBranchType> arrBranchType* NtupleBranch<arrBranchType*>::operator()() {
    return get();
}
template<typename arrBranchType> arrBranchType* NtupleBranch<arrBranchType*>::get()  {
    /// connect in read mode if not connected
    connectIfNeeded(); 
    /// and return the content
    return m_content;
}
template<typename arrBranchType> const arrBranchType & NtupleBranch<arrBranchType*>::operator()(size_t n){
    return get(n);
}
template<typename arrBranchType> double NtupleBranch<arrBranchType*>::asDouble(size_t i){
   return floatify(get(i));
}
template<typename arrBranchType> const arrBranchType & NtupleBranch<arrBranchType*>::get(size_t n){
    /// connect in read mode if not connected
    connectIfNeeded();
    /// check for a valid content
    if (!m_content){
        throw ExcAccessInvalidBranch("Trying to access array branch "+m_branchName+", which is not connected.");
    }
    /// perform bounds checking
    if (n >= m_length){
        throw std::out_of_range(Form("Requested entry %zu of length %zu array branch %s",n,m_length,m_branchName.c_str()));
    }
    /// then return the appropriate element
    return m_content[n];
}
template<typename arrBranchType> const arrBranchType* NtupleBranch<arrBranchType*>::readOnce (){
    /// connect in read mode if not connected
    connectIfNeeded(); 
    /// then disconnect, leaving our data member populated with the current entry
    disconnect(); 
    /// and return the current content
    return m_content;
}
template<typename arrBranchType> size_t NtupleBranch<arrBranchType*>::size() {
    return m_length;
}

template<typename arrBranchType> std::string NtupleBranch<arrBranchType*>::attachString(){
    return Form("%s[%zu]%s",m_branchName.c_str(),m_length,branchString<arrBranchType>().c_str());
}

template<typename arrBranchType> void NtupleBranch<arrBranchType*>::set(size_t n, arrBranchType in){
    /// connect in write mode if not yet done
    attachIfNeeded();
    /// bounds checking
    if (n >= m_length){
        throw std::out_of_range(Form("Attempting to fill index %zu in a size %zu array (branch %s)", n, m_length, m_branchName));
        return;
    }
    /// check that our array is properly initialized, then set the requested element
    if (m_content){
        m_content[n] = in;
    }
}


template<typename arrBranchType> template <size_t N>  void NtupleBranch<arrBranchType*>::set(arrBranchType (&in)[N]){
    /// connect in write mode if not yet done
    attachIfNeeded();
    /// bounds checking
    if (m_length != N){
        std::cerr << "Attempt to assign an array of length "<<N<<" to the size "<<m_length<<" branch "<<m_branchName<<", not doing anything"<<std::endl;
        return;
    }
    /// populate our array element by element
    for (size_t i = 0; i < m_length; ++i){
        m_content[i] = in[i];
    }
}

template<typename arrBranchType> void NtupleBranch<arrBranchType*>::set (arrBranchType* in){
    /// connect in write mode if not yet done
    attachIfNeeded();
    /// populate our array element by element
    /// Here we have no way of knowing the input array size! 
    /// Fragile... 
    /// TODO: Should this method be removed? 
    for (size_t i = 0; i < m_length; ++i){
        m_content[i] = in[i];
    }
}


template<typename arrBranchType> void NtupleBranch<arrBranchType*>::set (NtupleBranchBase*  in){
    /// connect in write mode if not yet done
    attachIfNeeded();
    /// if an invalid input was provided, bail out
    if (!in){
        std::cerr << "Failed to read the input branch when setting "<<getName()<< std::endl;
    }
    /// bounds checking
    if (in->size() != m_length){
        std::cerr << "attempt to set our length "<<m_length<<" branch "<<m_branchName<<" to a length "<<in->size()<<" input. This won't fly."<<std::endl;
        return;
    }
    /// next, see if we can cast to a compatible type
    NtupleBranch<arrBranchType*>* cast = dynamic_cast<NtupleBranch<arrBranchType*>*>(in);
    /// if compatible: copy element-wise 
    if (cast){
        for (size_t i = 0; i < m_length; ++i){
            m_content[i] = cast->get(i);
        }
    }
    /// incompatible: bail out!
    else {
        std::cerr << " Failed to correctly cast the branch "<<m_branchName<<" in NtupleBranch::set - the input branch seems to be of a wrong type! "<<std::endl;
    }
}

template<typename arrBranchType> void NtupleBranch<arrBranchType*>::set(NtupleBranch <arrBranchType*> & in){
    /// connect in write mode if not yet done
    attachIfNeeded();
    /// bounds checking
    if (m_length != in.size()){
        std::cerr << "attempt to set our length "<<m_length<<" branch "<<m_branchName<<" to a length "<<in.size()<<" input. This won't fly."<<std::endl;
        return;
    }
    /// copy element-wise
    for (size_t i = 0; i < m_length; ++i){
        m_content[i] = in(i);
    }
}
template<typename arrBranchType> void NtupleBranch<arrBranchType*>::setDummy(){
    /// connect in write mode if not yet done
    attachIfNeeded();
    for (size_t i = 0; i < m_length; ++i){
        /// set each element to the respective dummy value
        m_content[i] = dummyVal<arrBranchType>();
    }
}
template<typename arrBranchType> std::string NtupleBranch<arrBranchType*>::generateMemberString() const {
    std::stringstream ss; 
    ss << "NtupleBranch <"
    <<std::setw(30)<< std::left 
    << this->memberTypeString<arrBranchType>()+"*>"
    <<m_branchName;
    return ss.str();
}
template<typename arrBranchType> std::string NtupleBranch<arrBranchType*>::generateInitString(unsigned int paddingWidth) const {
    std::stringstream ss; 
    ss << std::setw(paddingWidth)<< std::left 
    <<m_branchName 
    << "(\""
    <<std::setw(paddingWidth)<< std::left
    <<m_branchName+"\","
    <<std::setw(2)<< std::left
    <<m_length<<",t, this)";
    return ss.str();
}

template<typename arrBranchType> void NtupleBranch<arrBranchType*>::attachReadPayload(){
    m_branch->SetAddress(m_content);
}

template<typename arrBranchType> TBranch* NtupleBranch<arrBranchType*>::addPayloadAsBranch(){
    if (m_tree){
        /// for C-arrays, we actually need the 'long version' of the branch method to 
        /// correctly convey the size... 
        return m_tree->Branch(m_branchName.c_str(), m_content, attachString().c_str(),s_write_basketsize); 
    }
    else return nullptr;
}


template <typename T> std::string NtupleBranchBase::branchString(){
    return "";
} 
