
inline Int_t NtupleBranchBase::getEntry(const Long64_t & entry) {      
    m_currEntry = entry;
    /// only if we are actively reading the branch, we actually read data. 
    if (isConnectedRead()) return m_branch->GetEntry(entry); 
    /// otherwise, we report zero bytes read. 
    return 0;
}

/// this is the fallback access operator
template<typename T> bool NtupleBranchBase::operator()(T & t){
    /// perform a dynamic cast to the expected type. 
    NtupleBranch<T>* b = dynamic_cast<NtupleBranch<T>*>(this); 
    /// if successful, we read the branch and return true
    if (b) {
        t=b->get();
        return true;
    }
    /// otherwise, return false to admit our failure :-( 
    return false; 
}

inline const std::string &  NtupleBranchBase::getName() const {
    return m_branchName;
}
inline TBranch* NtupleBranchBase::getBranch() const {
    return m_branch;
}
inline bool NtupleBranchBase::isConnectedRead() const{
    return (m_connectionStatus == connectedRead); 
}
inline bool NtupleBranchBase::isConnectedWrite() const{
    return (m_connectionStatus == connectedWrite); 
}

template <typename T> double NtupleBranchBase::floatify(const T& t){
    return (double)(t); 
}
template <typename T> double NtupleBranchBase::floatify(const std::vector<T>& t){
    std::cerr << "Careful! float conversion is not supported for nested vectors. "<<std::endl;
    std::cerr << "Will return the size of the internal vector, no particular element"<<std::endl;
    return floatify(t.size()); 
}

/// The 'branchString' method requires template specialisation 
/// because ROOT needs the variable type as a string in the "Branch" method call
/// These are defined in the .cxx
template <> std::string NtupleBranchBase::branchString<char*>();
template <> std::string NtupleBranchBase::branchString<std::string>();
template <> std::string NtupleBranchBase::branchString<Char_t>();
template <> std::string NtupleBranchBase::branchString<UChar_t>();
template <> std::string NtupleBranchBase::branchString<Short_t>();
template <> std::string NtupleBranchBase::branchString<Int_t>();
template <> std::string NtupleBranchBase::branchString<Long64_t>();
template <> std::string NtupleBranchBase::branchString<UShort_t>();
template <> std::string NtupleBranchBase::branchString<UInt_t>();
template <> std::string NtupleBranchBase::branchString<ULong64_t>();
template <> std::string NtupleBranchBase::branchString<Float_t>();
template <> std::string NtupleBranchBase::branchString<Double_t>();
template <> std::string NtupleBranchBase::branchString<Bool_t>();


template <> std::string NtupleBranchBase::memberTypeString<char*>();
template <> std::string NtupleBranchBase::memberTypeString<std::string>();
template <> std::string NtupleBranchBase::memberTypeString<Char_t>();
template <> std::string NtupleBranchBase::memberTypeString<UChar_t>();
template <> std::string NtupleBranchBase::memberTypeString<Short_t>();
template <> std::string NtupleBranchBase::memberTypeString<Int_t>();
template <> std::string NtupleBranchBase::memberTypeString<Long64_t>();
template <> std::string NtupleBranchBase::memberTypeString<UShort_t>();
template <> std::string NtupleBranchBase::memberTypeString<UInt_t>();
template <> std::string NtupleBranchBase::memberTypeString<ULong64_t>();
template <> std::string NtupleBranchBase::memberTypeString<Float_t>();
template <> std::string NtupleBranchBase::memberTypeString<Double_t>();
template <> std::string NtupleBranchBase::memberTypeString<Bool_t>();


template <> std::string NtupleBranchBase::memberTypeString<std::vector<char*>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<std::string>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<Char_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<UChar_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<Short_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<Int_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<Long64_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<UShort_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<UInt_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<ULong64_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<Float_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<Double_t>>();
template <> std::string NtupleBranchBase::memberTypeString<std::vector<Bool_t>>();


template <> char* NtupleBranchBase::dummyVal<char*>();
template <> std::string NtupleBranchBase::dummyVal<std::string>();
template <> Char_t NtupleBranchBase::dummyVal<Char_t>();
template <> UChar_t NtupleBranchBase::dummyVal<UChar_t>();
template <> Short_t NtupleBranchBase::dummyVal<Short_t>();
template <> Int_t NtupleBranchBase::dummyVal<Int_t>();
template <> Long64_t NtupleBranchBase::dummyVal<Long64_t>();
template <> UShort_t NtupleBranchBase::dummyVal<UShort_t>();
template <> UInt_t NtupleBranchBase::dummyVal<UInt_t>();
template <> ULong64_t NtupleBranchBase::dummyVal<ULong64_t>();
template <> Float_t NtupleBranchBase::dummyVal<Float_t>();
template <> Double_t NtupleBranchBase::dummyVal<Double_t>();
template <> Bool_t NtupleBranchBase::dummyVal<Bool_t>();

template <> std::vector<char*> NtupleBranchBase::dummyVal<std::vector<char*>>();
template <> std::vector<std::string> NtupleBranchBase::dummyVal<std::vector<std::string>>();
template <> std::vector<Char_t> NtupleBranchBase::dummyVal<std::vector<Char_t>>();
template <> std::vector<UChar_t> NtupleBranchBase::dummyVal<std::vector<UChar_t>>();
template <> std::vector<Short_t> NtupleBranchBase::dummyVal<std::vector<Short_t>>();
template <> std::vector<Int_t> NtupleBranchBase::dummyVal<std::vector<Int_t>>();
template <> std::vector<Long64_t> NtupleBranchBase::dummyVal<std::vector<Long64_t>>();
template <> std::vector<UShort_t> NtupleBranchBase::dummyVal<std::vector<UShort_t>>();
template <> std::vector<UInt_t> NtupleBranchBase::dummyVal<std::vector<UInt_t>>();
template <> std::vector<ULong64_t> NtupleBranchBase::dummyVal<std::vector<ULong64_t>>();
template <> std::vector<Float_t> NtupleBranchBase::dummyVal<std::vector<Float_t>>();
template <> std::vector<Double_t> NtupleBranchBase::dummyVal<std::vector<Double_t>>();
template <> std::vector<Bool_t> NtupleBranchBase::dummyVal<std::vector<Bool_t>>();