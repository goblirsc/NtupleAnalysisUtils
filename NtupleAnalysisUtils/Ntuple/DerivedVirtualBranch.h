# ifndef NTAU__DERIVEDVIRTUALBRANCH__H
#define NTAU__DERIVEDVIRTUALBRANCH__H

#include <limits> 

/// This class allows to define a pseudo-branch
/// that can be 'read' just like an ordinary branch,
/// but instead of reading something from a tree
/// performs a calculation based on its content. 
/// 
/// For example: Calculation of 
/// invariant mass if a tree has 
/// per-object 4-momentum branches. 
/// 
/// The result will be cached to avoid 
/// unnecessary re-calculations
///
/// Word of warning: Making a branch use itself 
/// within the calculation will make the calculation 
/// "see" the value from the 
/// previous entry (or for the first entry, the default value)! 
///
/// Idea obo J.Junggeburth
///
template <class outputType, class inputType> class DerivedVirtualBranch{
public: 
    /// Construct by passing the calculation to be performed and the manager to take
    /// the input data from
    DerivedVirtualBranch(std::function<outputType(inputType &)> calculationImplementation, 
                         NtupleBranchMgr*                       inputSource); 

    /// get the result of our calculation. Performs the calculation
    /// the first time we call this, and  
    /// retrieves the cache content afterwards while we are in
    /// the same entry of the input 
    inline const outputType& operator()(); 

private: 
    /// cache 
    outputType m_cachedResult{}; 
    Long64_t m_cachedEntry{std::numeric_limits<Long64_t>::max()};
    /// getter function 
    std::function<outputType(inputType &)> m_calculation;
    /// Input tree. We have to keep the base class here
    /// to avoid introducing circular dependencies in case 
    /// of branches operating on the input tree
    inputType* m_theInput{nullptr}; 
}; 


/// As above, but with support for element-wise access! 
template <class outputType, class inputType> class DerivedVirtualVecLikeBranch{
public: 
    /// Construct by passing the calculation to be performed and the manager to take
    /// the input data from
    DerivedVirtualVecLikeBranch(std::function<outputType(inputType &, size_t)> calculationImplementation, 
                         NtupleBranchMgr*                       inputSource); 

    /// get the result of our calculation. Performs the calculation
    /// the first time we call this, and  
    /// retrieves the cache content afterwards while we are in
    /// the same entry of the input 
    inline const outputType& operator()(size_t index); 

private: 
    /// cache 
    inline void resizeIfNeeded(size_t index); 
    std::vector<outputType> m_cachedResult{}; 
    std::vector<Long64_t> m_cachedEntry{};
    /// getter function 
    std::function<outputType(inputType &, size_t)> m_calculation;
    size_t m_capacity{0}; 
    /// Input tree. We have to keep the base class here
    /// to avoid introducing circular dependencies in case 
    /// of branches operating on the input tree
    inputType* m_theInput{nullptr}; 
}; 


#include "DerivedVirtualBranch.ixx"

#endif // NTAU__DERIVEDVIRTUALBRANCH__H
