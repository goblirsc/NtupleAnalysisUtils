template <class outputType, class inputType> 
    DerivedVirtualBranch<outputType,inputType>::DerivedVirtualBranch(
            std::function<outputType(inputType &)> calculationImplementation, 
            NtupleBranchMgr*                       inputSource):
        m_calculation(calculationImplementation){
    /// check the input type at construction level to provide early warning
    if (!inputSource || !dynamic_cast<inputType*>(inputSource)){
        std::cerr << "Operating a dynamic branch with the wrong input tree type, will produce invalid results!"<<std::endl;
        m_calculation = [](inputType &){
            return outputType{}; 
        };
    }
    m_theInput = dynamic_cast<inputType*>(inputSource);

}

template <class outputType, class inputType> 
    inline const outputType& DerivedVirtualBranch<outputType,inputType>::operator()(){

    if (m_theInput && m_cachedEntry != m_theInput->getCurrEntry()){
        /// first update the cached entry - this prevents endless circular call chains
        m_cachedEntry = m_theInput->getCurrEntry();  
        /// Here, we don't need to check for nullptr 
        /// because the constructor precaution ensures that
        /// a nullptr dereference will never be actually performed. 
        m_cachedResult = m_calculation(*m_theInput);
    }
    return m_cachedResult; 
}


template <class outputType, class inputType> 
    DerivedVirtualVecLikeBranch<outputType,inputType>::DerivedVirtualVecLikeBranch(
            std::function<outputType(inputType &, size_t)> calculationImplementation, 
            NtupleBranchMgr*                       inputSource):
        m_calculation(calculationImplementation),
        m_theInput(dynamic_cast<inputType*>(inputSource)){
    /// check the input type at construction level to provide early warning
    if (!inputSource || !m_theInput){
        std::cerr << "Operating a dynamic branch with the wrong input tree type, will produce invalid results!"<<std::endl;
        m_calculation = [](inputType &,size_t){
            return outputType{}; 
        };
    }

}

template <class outputType, class inputType> 
    inline const outputType& DerivedVirtualVecLikeBranch<outputType,inputType>::operator()(size_t index){
    resizeIfNeeded(index);
    if (m_theInput && m_cachedEntry.at(index) != m_theInput->getCurrEntry()){
        /// first update the cached entry - this prevents endless circular call chains
        m_cachedEntry.at(index) = m_theInput->getCurrEntry();  
        /// Here, we don't need to check for nullptr 
        /// because the constructor precaution ensures that
        /// a nullptr dereference will never be actually performed. 
        if (index < m_cachedResult.size()){
            m_cachedResult.at(index) = m_calculation(*m_theInput,index);
        } else {
            /// We need to inflate the vector
            const outputType the_output = m_calculation(*m_theInput,index);
            while (index >= m_cachedResult.size()) m_cachedResult.emplace_back(the_output);
        
        }
    }
    return m_cachedResult.at(index); 
}

template <class outputType, class inputType> 
    inline void DerivedVirtualVecLikeBranch<outputType,inputType>::resizeIfNeeded(size_t index){
    if (index >= m_capacity){
        m_capacity = index+5; 
        m_cachedResult.reserve(m_capacity); 
        m_cachedEntry.resize(m_capacity, std::numeric_limits<Long64_t>::max()); 
    }
}
