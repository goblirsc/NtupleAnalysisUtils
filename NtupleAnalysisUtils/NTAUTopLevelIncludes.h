#ifndef NTAU_TOPLEVEL__INCLUDE
#define NTAU_TOPLEVEL__INCLUDE

/// This is a top-level header designed to conveniently make all features of NtupleAnalysisUtils 
/// available to client packages. 

#include "NtupleAnalysisUtils/Plot/NTAUPlotIncludes.h"
#include "NtupleAnalysisUtils/Helpers/NTAUHelpersIncludes.h"
#include "NtupleAnalysisUtils/Configuration/NTAUConfigurationIncludes.h"
#include "NtupleAnalysisUtils/Visualisation/NTAUVisualisationIncludes.h"
#include "NtupleAnalysisUtils/HistoFiller/NTAUHistoFillerIncludes.h"
#include "NtupleAnalysisUtils/Ntuple/NTAUNtupleIncludes.h"

#endif // NTAU_TOPLEVEL__INCLUDE