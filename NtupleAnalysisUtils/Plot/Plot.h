#ifndef __NTAU__PLOT_H
#define __NTAU__PLOT_H

#include "NtupleAnalysisUtils/Plot/IPlotPopulator.h"
// #include "NtupleAnalysisUtils/Plot/Ba    sicPopulators.h"
#include "NtupleAnalysisUtils/Configuration/PlotFormat.h"

/// Class for working with Plots in NtupleAnalysisUtils. 
/// 
/// The Plot class internally wraps a ROOT object using a shared pointer.
/// It is defined by a populator, responsible for obtaining said ROOT object, 
/// and a format defining how it will be visualised, named, etc. 
///
/// The populator can take a variety of forms, from simply copying 
/// an existing histogram to running an event loop or reading 
/// from a file. 
///
/// The populator will do its work either when the stored histogram 
/// of the Plot is requested the first time in a non-const context,
/// or when the populate() method of the Plot is called. 
///  
/// (c) 2017-2021, goblirsc<at>cern<dot>ch


// This is a base class with the main histogram storage functionality behind
// every plot we have 
template <class TypeOfHistogram> class Plot final: public IPopulatorSource<TypeOfHistogram>{
public:

    /// ===================================
    /// Constructors
    /// ===================================

    /// A plot is specified by two aspects, which the user can specify: 
    /// 
    /// - a means of obtaining content, called the populator. 
    ///   This can be done for example by loading from a file, 
    ///   running an event loop, copying an existing histo, plus arbitrary post-processing as needed 
    /// 
    /// - a format for displaying the plot on a canvas. This includes all line/marker aspects as well 
    ///   as the legend title and style. 
    Plot( const IPlotPopulator<TypeOfHistogram> & populator, 
                PlotFormat theFormat= PlotFormat()
    ); 

    /// copy c-tor. 
    /// If the plot being copied has already been populated, it will directly clone the populated content. 
    /// Otherwise, it will duplicate the populator of the argument, resulting in similar content after 
    /// the population has been called. 
    Plot(const Plot<TypeOfHistogram> & other); 

    /// assignment operator. Will also result in a clone. 
    Plot & operator=(const Plot<TypeOfHistogram> & other); 

    /// This c-tor is designed to consume the r-value ref, resulting in a 1:1 copy (same contained histo and state). 
    Plot(Plot<TypeOfHistogram> && other); 

    /// default c-tor, will wrap a nullptr and do nothing
    Plot(); 

    /// ===================================
    /// Access to the histogram 
    /// ===================================
    
    // this is how we interact with or expose the underlying ROOT object. 
    // Used when we want to call ROOT methods / pass our object to other ROOT methods
    // This will also trigger the population step if it has not been run yet. 
    inline TypeOfHistogram* operator()() ;
    inline TypeOfHistogram* operator->();
    /// identical to operator(), just results in nicer syntax when working with pointers to Plots
    inline TypeOfHistogram* getHisto();

    /// In a const context, we can not trigger the population step. 
    /// In this context, we just provide access to the content. 
    /// If your first use of a Plot is as a const Plot&, call
    /// populate() before passing the const ref.  
    inline TypeOfHistogram* operator()(void) const;
    inline TypeOfHistogram* operator->() const;
    inline TypeOfHistogram* getHisto() const;

    // also allow to access the shared pointer directly
    inline std::shared_ptr<TypeOfHistogram> getSharedHisto(void) const;


    /// =========================================
    /// Useful methods for working with the class
    /// =========================================

    /// The populate method will cause the connected populator to do its work 
    /// and update the internal histogram to the 'final' content. 
    /// This can for example trigger an event loop or read an input file. 
    /// 
    /// The doApplyFormat argument allows to also directly apply the 
    /// desired formatting to the now-populated object.  
    /// 
    /// populate() is also triggered automatically when attempting to access the wrapped 
    /// histogram the first time in a non-const context, the manual invocation
    /// is intended to give the user control over when the filling happens if so desired. 
    bool populate(bool doApplyFormat=true); 

    /// applies the previously stored formatting to contained histogram.
    /// This can be used to update the format if changes were made w.r.t the format
    /// present when populating (which is automatically applied). 
    void applyFormat();

    /// here we can retrieve or modify the format of the plot:  
    inline PlotFormat & plotFormat(); /// allows access and in-place modification
    inline const PlotFormat & plotFormat() const; /// allows access but no modification
    inline void setPlotFormat(PlotFormat in);  /// should rarely be needed

    /// setter and getter for the populator 
    void setPopulator(std::shared_ptr<IPlotPopulator<TypeOfHistogram>> thePopulator);
    std::shared_ptr<IPlotPopulator<TypeOfHistogram>>  getPopulator() const;

    /// this allows to obtain a clone of the populator. Makes the plot a viable 
    /// argument to a postprocessing chain
    std::shared_ptr<IPlotPopulator<TypeOfHistogram>> clonePopulator() const override; 

    /// Do a quick plot dump of the object drawn within a ROOT canvas. 
    /// Useful for fast debugging. 
    void QuickSaveAs(const std::string & filename) const;

    inline bool isPopulated() const{ return m_isPopulated;} 

protected:
    std::shared_ptr<IPlotPopulator<TypeOfHistogram>> m_populator{nullptr};    /// This is responsible for populating our plot 
    PlotFormat m_format{};                                                    /// the format used for drawing
    std::shared_ptr<TypeOfHistogram> m_histo{nullptr};                        /// the underlying ROOT object. Can be null. 
    bool m_isPopulated{false};                                                /// Remember whether we already populated this plot
};


// A fairly trivial populator which will package 
// already existing histograms 
template <class HistoType> class CopyExisting final : public IPlotPopulator<HistoType>{
    public:
        /// note that the constructor here already clones the histo at the time of creation. 
        CopyExisting(const HistoType & toCopy);
        CopyExisting(const HistoType* toCopy);
        CopyExisting(const Plot<HistoType> & toCopy);
        CopyExisting(std::shared_ptr<HistoType> toCopy);
        /// No clone happens in populate(), since we clone at construction time 
        std::shared_ptr<HistoType> populate() override;
        std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override; 
    private: 
        std::shared_ptr<HistoType> m_localCopy{nullptr}; 
};



#include "Plot.ixx"


#endif // __NTAU__PLOT_H
