#ifndef __NTAU__I__PLOT__POPULATOR__H
#define __NTAU__I__PLOT__POPULATOR__H

#include <memory> 

/// This defines the common interfaces used by all plot populator tools in NTAU. 
/// There are two of them: 

/// The IPlotPopultor knows how to populate a plot (produce a shared_ptr containing 
/// a filled plot obtained in some way, 
/// and how to provide the user with a clone of itself.

/// The IPopulatorSource just knows how to supply a cloned IPlotPopulator. 
/// This is a weaker requirement also fulfilled by the Plot, and is used
/// to allow already defined Plots as inputs to post-processing steps. 

/// forward declare the IPlotPopulator 
template <class HistoType> class IPlotPopulator;

/// This is the IPopulatorSource. Only feature is to supply an IPlotPopulator clone 
template <class HistoType> class IPopulatorSource{
public:
    virtual std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const  = 0;     /// returns a clone of this populator
};

/// This is the IPlotPopulator. Extends the IPopulatorSource by actually being able to populate 
/// objects. 
template <class HistoType> class IPlotPopulator: public IPopulatorSource<HistoType>{
public:
    virtual std::shared_ptr<HistoType> populate()=0;   /// will return a populated histogram
};




#endif // __NTAU__I__PLOT__POPULATOR__H
