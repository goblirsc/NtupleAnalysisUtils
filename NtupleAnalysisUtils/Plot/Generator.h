#pragma once 

#include "NtupleAnalysisUtils/Helpers/Concepts.h"
#include <tuple> 
#include <memory> 

namespace PlotUtils{
    /// Defines a "generator" object holding a set of constructor args for delayed instantiation of objects.
    /// Useful when booking many large histograms to avoid allocating memory before it is needed. 

    /// abstract base class for a generator, holding arbitrary args 
    /// and specifying a generated object type 
    template <class target> 
    class objGenerator{
        public: 
        /// Generates a new shared pointer with the stored ctor args 
        virtual std::shared_ptr<target> generateShared() const = 0 ; 
        /// Generates a new local object with the stored ctor args 
        virtual target generate() const = 0 ; 
    };

    /// concrete implementation of the generator for a given arg set. 
    /// Uses the canConstruct concept to ensure the argument set is valid 
    /// for the desired object. 
    template <class target, typename... args> requires canConstruct<target,args...> 
    class objGeneratorImp: public objGenerator<target>{
        public: 
        /// constructor - captures argument set 
        objGeneratorImp(args... a): m_myargs(a...){}
        /// generator for a local object
        virtual target generate() const override final{
            return std::make_from_tuple<target>(m_myargs); 
        }
        /// generator for a shared pointer 
        virtual std::shared_ptr<target> generateShared() const override final{
            return std::apply([](args... a)->std::shared_ptr<target>{
                return std::make_shared<target>(a...);
            }, m_myargs); 
        }
        private: 
        /// storage for the argument set 
        std::tuple<args...> m_myargs; 
    };

    // /// additional implementation implementing a delayed clone of a passed reference histogram.
    // /// This one is used to wrap cases where the user pre-specifies an existing ref histo
   
    template <class target, typename refType> requires std::is_base_of<target,refType>::value
    class objGeneratorFromShared: public objGenerator<target>{
        public: 
        objGeneratorFromShared(std::shared_ptr<refType> p): m_ref(p){}
        virtual target generate() const override final{
            return *m_ref; 
        }
        virtual std::shared_ptr<target> generateShared() const override final{
            return m_ref; 
        }
        private: 
        std::shared_ptr<refType> m_ref; 
    };
    template <class target, typename refType> requires std::is_base_of<target,refType>::value
    class objGeneratorFromPtr: public objGenerator<target>{
        public: 
        objGeneratorFromPtr(refType* p): m_ref(p){}
        virtual target generate() const override final{
            return *m_ref; 
        }
        virtual std::shared_ptr<target> generateShared() const override final{
            return std::shared_ptr<target>(dynamic_cast<target*>(m_ref->Clone())); 
        }
        private: 
        refType* m_ref; 
    };


    /// helper to build a shared-ptr to a generator base class from a set of args 
    template <typename T, typename... args> requires canConstruct<T,args...> 
    std::shared_ptr<objGenerator<T>> bookGenerator(args... a){
        return std::make_shared<objGeneratorImp<T,args...>>( a...); 
    };
    /// helper to build a shared-ptr to a generator base class from a shared pointer to an existing histo 
    template <typename T, typename refType> requires std::is_base_of<T,refType>::value 
    std::shared_ptr<objGenerator<T>> bookGenerator(std::shared_ptr<refType> ptr){
        return std::make_shared<objGeneratorFromShared<T,refType>>( ptr ); 
    };
    /// helper to build a shared-ptr to a generator base class from a raw pointer to an existing histo 
    template <typename T, typename refType> requires std::is_base_of<T,refType>::value 
    std::shared_ptr<objGenerator<T>> bookGenerator(refType* ptr){
        return std::make_shared<objGeneratorFromPtr<T,refType>>( ptr ); 
    };


}
