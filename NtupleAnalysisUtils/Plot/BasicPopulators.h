#ifndef __NTAU__PLOT__BASIC_POPULATOR__H
#define __NTAU__PLOT__BASIC_POPULATOR__H

#include "NtupleAnalysisUtils/Plot/Plot.h"
#include "NtupleAnalysisUtils/Helpers/Concepts.h"
#include <functional>
#include <regex>
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"

/// The classes declared here are used for actually populating our plots. 
/// Several flavours exist as described below. 
/// They all implement the IPlotPopulator interface, which is used to talk to the 
/// Plot class. 


/// You can simply create histograms in-place.
/// All arguments will be passed directly into the ROOT constructor
template <class HistoType> class ConstructInPlace final: public IPlotPopulator<HistoType>{
    public:
        /// forwards the arguments used for histogram definition to the ROOT constructor
        template <class... Args> ConstructInPlace(Args ...args);
        std::shared_ptr<HistoType> populate() override final;
        std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override final;
    private: 
        std::shared_ptr<HistoType> m_localHisto{nullptr}; 
};
/// This version will load a histogram from an input file. 
/// Handles opening/closing of the file internally. 
template <class HistoType> class LoadFromFile final: public IPlotPopulator<HistoType>{
    public:
        LoadFromFile(const std::string & fileName,  /// name of the file to read from
                     const std::string & objKey );  /// name (incl. directories) of the object to load. 
                             
        virtual std::shared_ptr<HistoType> populate() override final;
        virtual std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override final;
    protected:
        std::string m_fname;
        std::string m_objKey;
};

/// This version will load a histogram from an input file. 
/// Handles opening/closing of the file internally. 
/// Uses a regular expression instead of an explicit file name 
template <class HistoType> class LoadFromFileViaPattern final: public IPlotPopulator<HistoType>{
    public:
        LoadFromFileViaPattern(const std::string & fileName,  /// name of the file to read from
                     const std::string & pattern );  /// name (incl. directories) of the object to load. 
                             
        virtual std::shared_ptr<HistoType> populate() override final;
        virtual std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override final;
    protected:
        std::string m_fname;
        std::string m_pattern;
};

/// This version will create a histogram in place an populate it using a manually defined function. 
/// Can be used with fillRandom for example. But mainly intended as a fallback for cases not covered above
template <class HistoType> class ConstructAndFillInPlace final: public IPlotPopulator<HistoType>{
    public:
        template <class... Args> ConstructAndFillInPlace(std::function<void(HistoType*)> filler, 
                                                         Args... args);  /// Arguments to the ROOT object constructor
        ConstructAndFillInPlace(const HistoType* hist,   /// here we pass the histogram directly 
                                std::function<void(HistoType*)> filler
                                );  
        virtual std::shared_ptr<HistoType> populate() override final;
        virtual std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override final;
    protected:
        std::shared_ptr<HistoType> m_localCopy{nullptr}; 
        std::function<void(HistoType*)> m_fillFunc;
        bool m_alreadyFilled{false}; 
};


/// helper method needed for the LoadFromFile populator 

template <withSetDirectory Thing> inline void SetDirIfNeeded(std::shared_ptr<Thing> item){
    item->SetDirectory(0);
}
template <typename Thing> requires (!withSetDirectory<Thing>) inline void SetDirIfNeeded(std::shared_ptr<Thing> ){
}


#include "BasicPopulators.ixx"

#endif
