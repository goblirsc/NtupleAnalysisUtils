#ifndef NTUPLEANALYSISUTILS_PLOT_IXX
#define NTUPLEANALYSISUTILS_PLOT_IXX
#include <TCanvas.h> 
#include <iostream> 
#include "TH1.h"

#include <memory>

// implement constructors


template <class TypeOfHistogram>  Plot<TypeOfHistogram>::Plot( 
        const IPlotPopulator<TypeOfHistogram> & populator, 
              PlotFormat                        theFormat): 
    m_populator(populator.clonePopulator()),
    m_format(theFormat){
        TH1::AddDirectory(kFALSE);  // ensure that ROOT keeps its dirty fingers off our histo! 
}

/// default c-tor
template <class TypeOfHistogram>  Plot<TypeOfHistogram>::Plot(): 
    m_isPopulated(true){        /// set m_isPopulated to true to avoid triggering an empty populator
        TH1::AddDirectory(kFALSE);  // ensure that ROOT keeps its dirty fingers off our histo! 
}


// copy constructor - trivial case
template <class H>  Plot<H>::Plot(const Plot<H> & other):
    m_populator(other.m_populator),
    m_format(other.m_format),
    m_histo(nullptr),
    m_isPopulated(other.m_isPopulated){
        TH1::AddDirectory(kFALSE);  // ensure that ROOT keeps its dirty fingers off our histo! 
        if (other.m_isPopulated && other.m_histo){
            m_histo = std::shared_ptr<H>(dynamic_cast<H*>(other.m_histo->Clone()));
        }
}

template <class H> Plot<H> & Plot<H>::operator=(const Plot<H> & other){
    m_populator = other.m_populator; 
    m_format = other.m_format; 
    m_histo = nullptr; 
    m_isPopulated = other.m_isPopulated; 
    if (other.m_isPopulated && other.m_histo){
        m_histo = std::shared_ptr<H>(dynamic_cast<H*>(other.m_histo->Clone()));
    }
    return *this;
}


template <class H> bool Plot<H>::populate(bool doApplyFormat){
    if (!m_isPopulated){
        if (!m_populator){
            std::cerr << "Careful, you are attempting to populate a plot without a populator! "<<std::endl; 
            return false; 
        }
        m_histo = m_populator->populate();
        if (!m_histo){
            std::cerr << "Careful! We failed to properly populate a plot! "<<std::endl;
            return false;
        }
        m_isPopulated = true; 
    }
    if (m_histo != nullptr && doApplyFormat) applyFormat();
    return true;    /// TODO: Do we want a status return? 
}



// copy constructor - trivial case
template <class H>  Plot<H>::Plot(Plot<H> && other):
    m_populator(std::move(other.m_populator)),
    m_format(std::move(other.m_format)),
    m_histo(std::move(other.m_histo)),
    m_isPopulated(std::move(other.m_isPopulated)){
        other.m_populator = nullptr; 
        other.m_format = PlotFormat(); 
        other.m_histo = nullptr; 
        other.m_isPopulated = false; 
}

template <class H>  void Plot<H>::applyFormat(){
    // check if histo is valid before attempting to fill it
    if (m_histo.get() != nullptr) m_format.applyTo(m_histo.get());
}

template <class H> inline H* Plot<H>::operator()() {
    if (!m_isPopulated) populate(); 
    return m_histo.get();
}
template <class H> inline H* Plot<H>::operator->() {
    if (!m_isPopulated) populate();  
    return m_histo.get();
}

template <class H> inline H* Plot<H>::getHisto() {
    if (!m_isPopulated) populate(); 
    return m_histo.get();
}

template <class H> inline H* Plot<H>::operator()() const{
    return m_histo.get();
}
template <class H> inline H* Plot<H>::operator->() const{
    return m_histo.get();
}

template <class H> inline H* Plot<H>::getHisto() const{
    return m_histo.get();
}

template <class H> inline std::shared_ptr<H> Plot<H>::getSharedHisto() const{
    return m_histo; 
}
template <class H> inline PlotFormat & Plot<H>::plotFormat() {
    return m_format;
}
template <class H> inline const PlotFormat & Plot<H>::plotFormat() const {
    return m_format;
}
template <class H> inline void Plot<H>::setPlotFormat(PlotFormat in){
    m_format = in;
}

template <class H> void Plot<H>::setPopulator(std::shared_ptr<IPlotPopulator<H>> thePopulator){
    m_isPopulated = false;  // changing the populator implies this plot should be repopulated
    m_populator = thePopulator;
}
template <class H> std::shared_ptr<IPlotPopulator<H>>  Plot<H>::getPopulator() const {
    return m_populator;
}
template <class H> void Plot<H>::QuickSaveAs(const std::string & filename) const{
    // this is a very rudimentary draw macro. Used for debugging.
    TCanvas can ("cantmp","can",800,600);
    can.cd();
    applyFormat();
    m_histo->Draw();
    can.SaveAs(filename.c_str());
}
template <class H> std::shared_ptr<IPlotPopulator<H>> Plot<H>::clonePopulator() const{
    if (m_isPopulated){
        return std::make_shared<CopyExisting<H>>(m_histo); 
    }
    return m_populator->clonePopulator(); 
}
template <class HistoType> CopyExisting<HistoType>::CopyExisting(const HistoType & toCopy){
    m_localCopy = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(toCopy.Clone())); 
    SetDirIfNeeded(m_localCopy); 
}
template <class HistoType> CopyExisting<HistoType>::CopyExisting(const HistoType* toCopy){
    if (toCopy) m_localCopy = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(toCopy->Clone())); 
    SetDirIfNeeded(m_localCopy); 
}
template <class HistoType> CopyExisting<HistoType>::CopyExisting(std::shared_ptr<HistoType> toCopy){
    if (toCopy) m_localCopy = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(toCopy->Clone())); 
    SetDirIfNeeded(m_localCopy); 
}
template <class HistoType> CopyExisting<HistoType>::CopyExisting(const Plot<HistoType> & toCopy){
    if (toCopy()) m_localCopy = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(toCopy->Clone())); 
    SetDirIfNeeded(m_localCopy); 
}
template <class HistoType> std::shared_ptr<HistoType> CopyExisting<HistoType>::populate() {
    /// we return a clone, in order to prevent the customer Plot from changing our reference histogram
    return std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(m_localCopy->Clone()));
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> CopyExisting<HistoType>::clonePopulator() const {
    return std::make_shared<CopyExisting<HistoType>>(m_localCopy);
}


#endif
