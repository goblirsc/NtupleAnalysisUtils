#include <TCanvas.h> 
#include <iostream> 
#include "TH1.h"
#include "TFile.h"
#include "NtupleAnalysisUtils/Helpers/IOFunctions.h"
#include <memory>




template <class HistoType> template <class... Args> ConstructInPlace<HistoType>::ConstructInPlace(Args ...args){
    m_localHisto = std::make_shared<HistoType>(args...); 
    SetDirIfNeeded(m_localHisto); 
}
template <class HistoType> std::shared_ptr<HistoType> ConstructInPlace<HistoType>::populate() {
    /// we return a clone, in order to prevent the customer Plot from changing our reference histogram
    return std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(m_localHisto->Clone()));
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> ConstructInPlace<HistoType>::clonePopulator() const {
    return std::make_shared<CopyExisting<HistoType>>(m_localHisto);
}




template <class HistoType> LoadFromFile<HistoType>::LoadFromFile(const std::string & fileName,
                                                                 const std::string & objKey):     
            m_fname(fileName),
            m_objKey(objKey){
}
template <class HistoType> std::shared_ptr<HistoType> LoadFromFile<HistoType>::populate(){

    HistoType* h_tmp = PlotUtils::readFromFile<HistoType>(m_fname, m_objKey);
    if (!h_tmp){
        std::cerr <<" Failed to load object "<<m_objKey<<" from file "<<m_fname<<std::endl;
        return nullptr;
    }
    return std::shared_ptr<HistoType>(h_tmp);
    
}

template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> LoadFromFile<HistoType>::clonePopulator() const {
    return std::make_shared<LoadFromFile<HistoType>>(m_fname, m_objKey);
}



template <class HistoType> LoadFromFileViaPattern<HistoType>::LoadFromFileViaPattern(const std::string & fileName,
                                                                 const std::string & re):     
            m_fname(fileName),
            m_pattern(re){
}
template <class HistoType> std::shared_ptr<HistoType> LoadFromFileViaPattern<HistoType>::populate(){

    auto histos = PlotUtils::getAllObjectsFromFile<HistoType>(m_fname);
    HistoType* h_tmp{nullptr}; 
    std::regex target(m_pattern); 
    for (auto & h : histos){
        if (std::regex_match(h,target)){
            if (h_tmp != nullptr){
                std::cout <<" CAREFUL: Multiple matches found for regex pattern "<<m_pattern<<" - will return the first match and discard "<<h<<std::endl; 
            }
            else{
                h_tmp = PlotUtils::readFromFile<HistoType>(m_fname, h);
            }
        }
    }
    if (!h_tmp){
        std::cerr <<" Failed to load object "<<m_pattern<<" from file "<<m_fname<<std::endl;
        return nullptr;
    }
    return std::shared_ptr<HistoType>(h_tmp);
    
}

template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> LoadFromFileViaPattern<HistoType>::clonePopulator() const {
    return std::make_shared<LoadFromFileViaPattern<HistoType>>(m_fname, m_pattern);
}


template <class HistoType> template <class... Args> ConstructAndFillInPlace<HistoType>::ConstructAndFillInPlace(std::function<void(HistoType*)> filler,
 Args... args):
    m_localCopy (std::make_shared<HistoType>(args...)), 
    m_fillFunc(filler){
    SetDirIfNeeded(m_localCopy); 
}
template <class HistoType> ConstructAndFillInPlace<HistoType>::ConstructAndFillInPlace(const HistoType* hist, 
                                                                                       std::function<void(HistoType*)> filler){
    m_localCopy = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(hist->Clone()));
    SetDirIfNeeded(m_localCopy); 
    m_fillFunc = filler; 
}
template <class HistoType> std::shared_ptr<HistoType> ConstructAndFillInPlace<HistoType>::populate(){
    /// apply the filler to a clone of our object, so that we can safely be called multiple times
    /// without filling more than once
    std::shared_ptr<HistoType> theClone{dynamic_cast<HistoType*>(m_localCopy->Clone())};
    m_fillFunc(theClone.get()); 
    return theClone; 
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> ConstructAndFillInPlace<HistoType>::clonePopulator() const {
    return std::make_shared<ConstructAndFillInPlace<HistoType>>(m_localCopy.get(),m_fillFunc);
}
