
template <class InputHistoType> CalculateRatio<InputHistoType>::CalculateRatio( 
                    const IPopulatorSource<InputHistoType> & numeratorSource,  
                    const IPopulatorSource<InputHistoType> & denominatorSource,
                    PlotUtils::EfficiencyMode errorDefinition):
        m_numeratorSource(numeratorSource.clonePopulator()),
        m_denominatorSource(denominatorSource.clonePopulator()),
        m_errorDefinition(errorDefinition){
}
template <class InputHistoType>  std::shared_ptr<ratioType<InputHistoType>> CalculateRatio<InputHistoType>::populate() {
    auto numerator = m_numeratorSource->populate();
    auto denominator = m_denominatorSource->populate();
    if (!numerator){
        std::cerr << "Failed to compute a ratio - numerator is null histogram"<<std::endl;
        return nullptr;
    }
    if (!denominator){
        std::cerr << "Failed to compute a ratio - denominator is null histogram"<<std::endl;
        return nullptr;
    }
    ratioType<InputHistoType>* r = PlotUtils::getRatio(numerator.get(),denominator.get(),m_errorDefinition);
    return std::shared_ptr<ratioType<InputHistoType>>{r}; 
}   
template <class InputHistoType>  std::shared_ptr<IPlotPopulator<ratioType<InputHistoType>>> CalculateRatio<InputHistoType>::clonePopulator() const  {
    return std::make_shared<CalculateRatio<InputHistoType>>(*m_numeratorSource, *m_denominatorSource, m_errorDefinition);
}


template <class InputHistoType> CalculateProduct<InputHistoType>::CalculateProduct ( 
                        const IPopulatorSource<InputHistoType> & firstSource,  
                        const IPopulatorSource<InputHistoType> & secondSource):
        m_firstSource(firstSource.clonePopulator()),
        m_secondSource(secondSource.clonePopulator()){
}

    /// this will populate the upstream sources and then perform a product calculation
template <class InputHistoType>  std::shared_ptr<InputHistoType> CalculateProduct<InputHistoType>::populate(){
    auto firstHisto = PlotUtils::CloneHist<InputHistoType>(m_firstSource->populate());
    auto secondHisto = m_secondSource->populate();
    if (!firstHisto){
        std::cerr << "Failed to compute a product - firstHisto is null histogram"<<std::endl;
        return nullptr;
    }
    if (!secondHisto){
        std::cerr << "Failed to compute a product - secondHisto is null histogram"<<std::endl;
        return nullptr;
    }
    firstHisto->Multiply(secondHisto.get()); 
    return firstHisto; 
}

template <class InputHistoType>  std::shared_ptr<IPlotPopulator<InputHistoType>> CalculateProduct<InputHistoType>::clonePopulator() const{
    return std::make_shared<CalculateProduct<InputHistoType>>(*m_firstSource, *m_secondSource);
}



template <class HistoType> NormaliseBinsToWidth<HistoType>::NormaliseBinsToWidth( const IPopulatorSource<HistoType> & inputSource, double referenceWidth):
    m_input(inputSource.clonePopulator()),
    m_refWidth(referenceWidth){     
}  

template <class HistoType> std::shared_ptr<HistoType> NormaliseBinsToWidth<HistoType>::populate() {
    auto inHist = PlotUtils::CloneHist<HistoType>(m_input->populate());
    if (!inHist){
        std::cerr << "Failed to normalise to bin width - input is null histogram"<<std::endl;
        return nullptr;
    }
    PlotUtils::normaliseToBinWidth(inHist.get(),m_refWidth); 
    return inHist; 
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> NormaliseBinsToWidth<HistoType>::clonePopulator() const {
    return std::make_shared<NormaliseBinsToWidth<HistoType>>(*m_input, m_refWidth);
}


template <class HistoType> ShiftOverflows<HistoType>::ShiftOverflows( const IPopulatorSource<HistoType> & inputSource):
    m_input(inputSource.clonePopulator()){     
}  

template <class HistoType> std::shared_ptr<HistoType> ShiftOverflows<HistoType>::populate() {
    auto inHist = PlotUtils::CloneHist<HistoType>(m_input->populate());
    if (!inHist){
        std::cerr << "Failed to shift overflows - input is null histogram"<<std::endl;
        return nullptr;
    }
    PlotUtils::shiftOverflows(inHist.get()); 
    return inHist; 
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> ShiftOverflows<HistoType>::clonePopulator() const {
    return std::make_shared<ShiftOverflows<HistoType>>(*m_input);
}



template <class HistoType> ScaleByConstant<HistoType>::ScaleByConstant( const IPopulatorSource<HistoType> & inputSource, double scaleFactor):
    m_input(inputSource.clonePopulator()),
    m_scaleFactor(scaleFactor){    
}  
template <class HistoType> std::shared_ptr<HistoType> ScaleByConstant<HistoType>::populate() {
    auto inHist = PlotUtils::CloneHist<HistoType>(m_input->populate());
    if (!inHist){
        std::cerr << "Failed to scale an object - input is null histogram"<<std::endl;
        return nullptr;
    }
    inHist->Scale(m_scaleFactor); 
    return inHist; 
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> ScaleByConstant<HistoType>::clonePopulator() const {
    return std::make_shared<ScaleByConstant<HistoType>>(*m_input, m_scaleFactor);
}



template <class HistoType> LinearCombination<HistoType>::LinearCombination( const IPopulatorSource<HistoType> & inputSource, const IPopulatorSource<HistoType> & toAdd,  double scaleFactor):
    m_input(inputSource.clonePopulator()),
    m_toAdd(toAdd.clonePopulator()),
    m_scaleFactor(scaleFactor){    
}  
template <class HistoType> std::shared_ptr<HistoType> LinearCombination<HistoType>::populate() {
    auto inHist = PlotUtils::CloneHist<HistoType>(m_input->populate());
    auto addMe = m_toAdd->populate();
    if (!inHist){
        std::cerr << "Failed to perform addition - first argument of sum is null histogram"<<std::endl;
        return nullptr;
    }
    if (!addMe){
        std::cerr << "Failed to perform addition - second argument of sum is null histogram"<<std::endl;
        return nullptr;
    }
    inHist->Add(addMe.get(), m_scaleFactor); 
    return inHist; 
}

template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> LinearCombination<HistoType>::clonePopulator() const {
    return std::make_shared<LinearCombination<HistoType>>(*m_input, *m_toAdd, m_scaleFactor);
}



template <class HistoType> NormaliseToIntegral<HistoType>::NormaliseToIntegral( const IPopulatorSource<HistoType> & inputSource, 
                                                                                double targetIntegral, 
                                                                                bool multiplyByWidth,
                                                                                bool includeOflow):
    m_input(inputSource.clonePopulator()),
    m_targetIntegral(targetIntegral),
    m_includeOflow(includeOflow),
    m_multByWidth(multiplyByWidth){    
}  
template <class HistoType> std::shared_ptr<HistoType> NormaliseToIntegral<HistoType>::populate() {
    auto inHist = PlotUtils::CloneHist<HistoType>(m_input->populate());
    if (!inHist){
        std::cerr << "Failed to normalise an object to integral - input is null histogram"<<std::endl;
        return nullptr;
    }    
    PlotUtils::normaliseToIntegral(inHist.get(), m_targetIntegral, m_multByWidth, m_includeOflow); 
    return inHist; 
}
template <class HistoType> std::shared_ptr<IPlotPopulator<HistoType>> NormaliseToIntegral<HistoType>::clonePopulator() const {
    return std::make_shared<NormaliseToIntegral<HistoType>>(*(m_input.get()), m_targetIntegral, m_multByWidth, m_includeOflow);
}



/// This constructor uses the auxiliary method "collateInputs" to pacakge the received contributions 
/// to the input vector (see below)
template <class HistoType> template<typename... Args>  SumContributions<HistoType>::SumContributions (Args... args){
    collateInputs(args...); 
}
template <class HistoType> 
SumContributions<HistoType>::SumContributions (const std::vector<Plot<HistoType>> & inputs){
    for(auto & i : inputs){
        m_inputs.push_back(i.clonePopulator());
    }
}
/// here we just copy the vector
template <class HistoType> SumContributions<HistoType>::SumContributions (const std::vector<std::shared_ptr<IPlotPopulator<HistoType>>> & inputs):
    m_inputs(inputs){
}
/// this will populate the upstream sources and then perform a ratio calculation
template <class HistoType> std::shared_ptr<HistoType> SumContributions<HistoType>::populate() {
    std::shared_ptr<HistoType> out = nullptr;
    for (auto & in : m_inputs){
        auto theInput = in->populate();
        if (!theInput){
            std::cerr << "SumContributions encountered an empty histo, not adding to sum. Please check your setup!"<<std::endl; 
            continue; 
        }
        if (!out) out = std::shared_ptr<HistoType>(dynamic_cast<HistoType*>(theInput->Clone())); 
        else out->Add(theInput.get()); 
    }
    return out; 
}
template <class HistoType>  std::shared_ptr<IPlotPopulator<HistoType>> SumContributions<HistoType>::clonePopulator() const{
    return std::make_shared<SumContributions<HistoType>>(m_inputs);
}
/// Recursive argument collection, default step: Package one arg, then repeat for the remaining
template <typename HistoType> template<typename... Args> void SumContributions<HistoType>::collateInputs(const IPopulatorSource<HistoType> & H, Args... args){
    m_inputs.push_back(H.clonePopulator()); 
    collateInputs(args...); 
}
/// Recursive argument collection, final step: package the one remaining arg
template <typename HistoType> void SumContributions<HistoType>::collateInputs(const IPopulatorSource<HistoType>& H){
    m_inputs.push_back(H.clonePopulator()); 
}


template <class HistoType>
toTGAE_PoissonErr<HistoType>::toTGAE_PoissonErr( const IPopulatorSource<HistoType> & inputSource){
    m_input = inputSource.clonePopulator();
}

template <class HistoType>
std::shared_ptr<TGraphAsymmErrors> toTGAE_PoissonErr<HistoType>::populate(){
    auto in = m_input->populate(); 
    return std::shared_ptr<TGraphAsymmErrors>(PlotUtils::toTGAE(in.get())); 
}

template <class HistoType>
std::shared_ptr<IPlotPopulator<TGraphAsymmErrors>> toTGAE_PoissonErr<HistoType>::clonePopulator() const {
    return std::make_shared<toTGAE_PoissonErr<HistoType>>(*m_input);
}
template <class HistoType> 
RunSuperSmoother<HistoType>::RunSuperSmoother( const IPopulatorSource<HistoType> & inputSource, 
                        double smoothness,
                        double span,
                        bool isPeriodic):
        m_inSource(inputSource.clonePopulator()),
        m_smoothness(smoothness),
        m_span(span),
        m_isPeriodic(isPeriodic){
}

template <class HistoType>  
std::shared_ptr<HistoType> RunSuperSmoother<HistoType>::populate(){
    std::shared_ptr<HistoType> theHist = m_inSource->populate();
    return std::shared_ptr<HistoType>(PlotUtils::runSuperSmoother(theHist.get(),
                                                                    m_smoothness,
                                                                    m_span,
                                                                    m_isPeriodic));
}

template <class HistoType>  
std::shared_ptr<IPlotPopulator<HistoType>> RunSuperSmoother<HistoType>::clonePopulator() const {
    return std::make_shared<RunSuperSmoother<HistoType>>(*m_inSource, m_smoothness, m_span, m_isPeriodic);
}

template<class outType, class inType>
CastType<outType, inType>::CastType(const IPopulatorSource<inType> & inputSource):
    m_inSource(inputSource.clonePopulator()){
}

template<class outType, class inType>
std::shared_ptr<outType> CastType<outType, inType>::populate(){
    auto in = m_inSource->populate(); 
    auto out = std::dynamic_pointer_cast<outType>(in);
    if (!out){
        std::cerr << "Error: Type conversion failed for "<<in->GetName()<<std::endl;
    }
    return out; 
}

template<class outType, class inType>
std::shared_ptr<IPlotPopulator<outType>> CastType<outType, inType>::clonePopulator() const{
    return std::make_shared<CastType<outType,inType>>(*m_inSource);
}

template <class OutputHistoType, class InputHistoType>  GenericPostProcessing<OutputHistoType, InputHistoType>::GenericPostProcessing(
        const IPopulatorSource<InputHistoType> & inputSource, 
        std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<InputHistoType>)> processingStep){
    m_input = inputSource.clonePopulator(); 
    m_processingStep = processingStep;
}
template <class OutputHistoType, class InputHistoType>  std::shared_ptr<OutputHistoType> GenericPostProcessing<OutputHistoType, InputHistoType>::populate() {
    std::shared_ptr<InputHistoType> inputHist = PlotUtils::CloneHist<InputHistoType>(m_input->populate()); 
    return m_processingStep(inputHist); 
}
template <class OutputHistoType, class InputHistoType>  std::shared_ptr<IPlotPopulator<OutputHistoType>> GenericPostProcessing<OutputHistoType, InputHistoType>::clonePopulator() const  {
    return std::make_shared<GenericPostProcessing<OutputHistoType,InputHistoType>>(*m_input, m_processingStep);
}

template <class OutputHistoType, typename... inputs> 
    ExtendedPostProcessing<OutputHistoType,inputs...>::ExtendedPostProcessing(
        std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<inputs>...)> processingStep,
        const IPopulatorSource<inputs>&...  inputSources ){
                m_processingStep = processingStep;
                m_inputs = std::make_tuple(inputSources.clonePopulator()...);
        }

template <class OutputHistoType, typename... inputs> 
    std::shared_ptr<OutputHistoType> ExtendedPostProcessing<OutputHistoType,inputs...>::populate(){
        auto inputObjects = std::apply(
                                [](std::shared_ptr<IPlotPopulator<inputs>>... ins)->std::tuple<std::shared_ptr<inputs>...>{
                                    return std::make_tuple<std::shared_ptr<inputs>...>(PlotUtils::CloneHist(ins->populate())...);       
                                },
                                m_inputs);
        return std::apply(m_processingStep, inputObjects);
    }

template <class OutputHistoType, typename... inputs> 
ExtendedPostProcessing<OutputHistoType,inputs...>::ExtendedPostProcessing(
        std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<inputs>...)> processingStep,
        std::tuple<std::shared_ptr<IPlotPopulator<inputs>>...>  inputSources){
            m_processingStep = processingStep;
            m_inputs = inputSources;
    }
template <class OutputHistoType, typename... inputs> 
    std::shared_ptr<IPlotPopulator<OutputHistoType>>  ExtendedPostProcessing<OutputHistoType,inputs...>::clonePopulator() const{
        auto inputClones = std::apply(
                                [](std::shared_ptr<IPlotPopulator<inputs>>... ins){
                                    return std::make_tuple<std::shared_ptr<IPlotPopulator<inputs>>...>(ins->clonePopulator()...);
                                }, m_inputs);
        return std::make_shared<ExtendedPostProcessing<OutputHistoType,inputs...>>(m_processingStep, inputClones); 
    }


template <class OutputHistoType, typename... types> 
WrapToPostProcessor<OutputHistoType, types...>::WrapToPostProcessor(
     std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<types>...)> processingStep): 
            m_processingStep(processingStep){

    }
template <class OutputHistoType, typename... types>
    ExtendedPostProcessing<OutputHistoType,types...> WrapToPostProcessor<OutputHistoType, types...>::operator()(const IPopulatorSource<types>&... inputs){
            return ExtendedPostProcessing<OutputHistoType,types...>(m_processingStep, inputs...);
        }
