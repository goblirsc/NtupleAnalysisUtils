#ifndef NTAU_POSTPROCESSING_POPULATORS__H
#define NTAU_POSTPROCESSING_POPULATORS__H

/// Here, we define a set of populators which can be used for post-processing previously populated histograms.
/// They take one or several upstream populators as constructor arguments, and then act on the 
/// output of these populators.
///
/// Fun fact: The post processing populators are themselves viable inputs to further 
/// post-processing populators! So you can set up an entire work flow using 
/// this formalism, with the Plot objects eventually populated containing the final
/// results! 
///
/// You can pass the upstream populators either as IPlotPopulators (meaning other populators), 
/// or you can also pass Plot objects directly - their populators will be cloned. 
/// Note that the populator of the Plot object, not its actual content, will be used.  
///
/// Quite a few of these postprocessors are restricted to certain input / output types. 

#include "NtupleAnalysisUtils/Plot/IPlotPopulator.h"
#include "NtupleAnalysisUtils/Helpers/NTAUHelpersIncludes.h" 
#include "TH2D.h"

/// ==============================================================================
/// Ratio calculation.
/// ==============================================================================
template <class InputHistoType> class CalculateRatio final: public IPlotPopulator<ratioType<InputHistoType>>{
public:
    /// construct by providing numerator and denominator sources and a bin error strategy. 
    /// Both numeratorSource and denominatorSource can be either a Plot or another populator.
    CalculateRatio ( const IPopulatorSource<InputHistoType> & numeratorSource,  
                     const IPopulatorSource<InputHistoType> & denominatorSource,
                     PlotUtils::EfficiencyMode errorDefinition = PlotUtils::defaultErrors); 

    /// this will populate the upstream sources and then perform a ratio calculation
    std::shared_ptr<ratioType<InputHistoType>> populate() override  final;
    std::shared_ptr<IPlotPopulator<ratioType<InputHistoType>>> clonePopulator() const override  final;

private: 
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_numeratorSource;
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_denominatorSource;
    PlotUtils::EfficiencyMode m_errorDefinition{PlotUtils::defaultErrors};
};

/// ==============================================================================
/// Product calculation.
/// ==============================================================================
template <class InputHistoType> class CalculateProduct final: public IPlotPopulator<InputHistoType>{
public:

    /// construct by providing two sources. 
    /// Both sources can be either a Plot or another populator.
    CalculateProduct ( const IPopulatorSource<InputHistoType> & firstSource,  
                     const IPopulatorSource<InputHistoType> & secondSource); 

    /// this will populate the upstream sources and then perform a ratio calculation
    std::shared_ptr<InputHistoType> populate() override  final;
    std::shared_ptr<IPlotPopulator<InputHistoType>> clonePopulator() const override  final;

private: 
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_firstSource;
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_secondSource;
};


/// ==============================================================================
/// Efficiency extraction from a TEfficiency object. This can be used to collapse a TEfficiency 
/// result into a primitive histo. 
/// Will return a TH1D (the type of TEfficiency's internal histos)
/// ==============================================================================
class ExtractEfficiency final: public IPlotPopulator<TH1D>{
public:
    /// inputEff can be either a Plot or another populator.
    ExtractEfficiency( const IPopulatorSource<TEfficiency> & inputEff); 

    std::shared_ptr<TH1D> populate() override  final; 
    std::shared_ptr<IPlotPopulator<TH1D>> clonePopulator() const override  final; 

private: 
    std::shared_ptr<IPlotPopulator<TEfficiency>> m_inputEff;
};


/// ==============================================================================
/// Efficiency extraction from a two-dim. TEfficiency object. 
/// This can be used to collapse a TEfficiency result into a primitive histo. 
/// Will return a TH2D (the type of TEfficiency's internal histos)
/// ==============================================================================
class ExtractEfficiency2D final: public IPlotPopulator<TH2D>{
public:
    /// inputEff can be either a Plot or another populator.
    ExtractEfficiency2D( const IPopulatorSource<TEfficiency> & inputEff);   

    std::shared_ptr<TH2D> populate() override  final; 
    std::shared_ptr<IPlotPopulator<TH2D>> clonePopulator() const override  final; 

private: 
    std::shared_ptr<IPlotPopulator<TEfficiency>> m_inputEff;
};

/// ==============================================================================
/// Normalise the bin contents relative to a reference width. 
/// The reference width can be set to autodetect the first bin width if desired, 
/// by passing a negative value for the respective argument
/// ==============================================================================
template <class HistoType> class NormaliseBinsToWidth final: public IPlotPopulator<HistoType>{
public:
    /// inputSource can be either a Plot or another populator.
    NormaliseBinsToWidth( const IPopulatorSource<HistoType> & inputSource, double referenceWidth = 1.0);    

    std::shared_ptr<HistoType> populate() override  final; 
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override  final; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
    double m_refWidth; 
};

/// ==============================================================================
/// Move the under/overflow into the first/last bin.
/// ==============================================================================
template <class HistoType> class ShiftOverflows final: public IPlotPopulator<HistoType>{
public:
    /// inputSource can be either a Plot or another populator.
    ShiftOverflows( const IPopulatorSource<HistoType> & inputSource);    

    std::shared_ptr<HistoType> populate() override  final; 
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override  final; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
};

/// ==============================================================================
/// Scale everything by a constant factor
/// ==============================================================================
template <class HistoType> class ScaleByConstant final: public IPlotPopulator<HistoType>{
public:

    /// inputSource can be either a Plot or another populator.
    ScaleByConstant( const IPopulatorSource<HistoType> & inputSource, double scaleFactor = 1.0); 

    std::shared_ptr<HistoType> populate() override  final; 
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override  final; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
    double m_scaleFactor; 
};


/// ==============================================================================
/// Add a second plot, with an optional pre-factor
/// ==============================================================================
template <class HistoType> class LinearCombination final: public IPlotPopulator<HistoType>{
public:

    /// inputSource and toAdd can be either a Plot or another populator.
    /// The scale factor is applied to the second argument (toAdd) - so a -1 would give you subtraction
    LinearCombination( const IPopulatorSource<HistoType> & inputSource, const IPopulatorSource<HistoType> & toAdd, double scaleFactor = 1.0); 

    std::shared_ptr<HistoType> populate() override  final; 
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override  final; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
    std::shared_ptr<IPlotPopulator<HistoType>> m_toAdd;
    double m_scaleFactor; 
};

/// ==============================================================================
/// Normalise to a certain integral value
/// ==============================================================================
template <class HistoType> class NormaliseToIntegral final: public IPlotPopulator<HistoType>{
public:

    /// inputSource can be either a Plot or another populator.
    NormaliseToIntegral( const IPopulatorSource<HistoType> & inputSource, 
                        double target_integral = 1.0, 
                        bool multByBinWidth = false, 
                        bool includeOverflow = true);   

    std::shared_ptr<HistoType> populate() override  final;
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override  final; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
    double m_targetIntegral; 
    bool m_includeOflow; 
    bool m_multByWidth;
};

/// ==============================================================================
/// Convert a 1D data histogram to a TGraphAsymmErrors with approximate poisson errors
/// ==============================================================================
template <class HistoType> class toTGAE_PoissonErr final: public IPlotPopulator<TGraphAsymmErrors>{
public:

    /// inputSource can be either a Plot or another populator.
    toTGAE_PoissonErr( const IPopulatorSource<HistoType> & inputSource);   

    std::shared_ptr<TGraphAsymmErrors> populate() override  final;
    std::shared_ptr<IPlotPopulator<TGraphAsymmErrors>> clonePopulator() const override  final; 

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
};

/// ==============================================================================
/// Obtain the sum of a set of populators representing individual contributions.
/// Useful for example for obtaining a "sum(SM)" histogram from a number of samples. 
/// ==============================================================================
template <class HistoType> class SumContributions final: public IPlotPopulator<HistoType>{
public:

    /// Construct by providing an arbitrary number of populators or plots to sum together.
    /// 
    /// Note that this signature looks a bit strange since C++ won't accept 
    /// a vector of const-refs as a function argument and I don't want to force 
    /// the user to create shared pointers. We use a recursive variadic template 
    /// to be able to process an arbitrary number of const-refs to populator sources. 
    ///
    /// tl;dr: Don't worry about the look of the signature, just pass all your sum components 
    /// as the list of arguments and it should work! :-)
    template < typename... Args> SumContributions (Args... args); 

    /// Auxiliary constructor used for the clonePopulator method. 
    /// You can also call this in user code, but the signature above 
    /// will typically be much easier to work with! 
    SumContributions (const std::vector<std::shared_ptr<IPlotPopulator<HistoType>>> & inputs); 
    SumContributions (const std::vector<Plot<HistoType>> & inputs); 

    /// this will populate the upstream sources and fill a histogram with the sum of them 
    std::shared_ptr<HistoType> populate() override final;
    
    /// clone this populator 
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override final;

private: 
    /// helper methods to achieve desired constructor behaviour. 
    /// general recursion step
    template <typename... Args> void collateInputs(const IPopulatorSource<HistoType> & s, Args... args); 
    /// recursion end
    void collateInputs(const IPopulatorSource<HistoType> & s); 
    /// vector of populators to sum over
    std::vector<std::shared_ptr<IPlotPopulator<HistoType>>> m_inputs;
};


/// ==============================================================================
/// Run Friedman's super smoother on a histogram. 
/// User-selectable smoothing options
/// ==============================================================================
template <class HistoType> class RunSuperSmoother final: public IPlotPopulator<HistoType>{
public:

    /// inputSource can be either a Plot or another populator.
    /// Please see https://root.cern.ch/doc/master/classTGraphSmooth.html#a54f4a72b6e2e4d168cdcbebea070291a
    /// for more documentation of the algorithm and options
    /// @tparam HistoType: Histogram type to consume and return. Currently, assumed to be a 1D histogram class
    /// @param inputSource: populator providing the input 
    /// @param smoothness: Smoothness flag of the smoother. 0...10, 10 is maximum smoothness
    /// @param span: Span flag of the smoother. ROOT recommends for small (n < 40) samples to use a span > 0, between 0.2 and 0.4  
    /// @param isPeriodic: periodic flag for the smoother. If set, the x values are assumed to be in [0, 1] and of period 1.
    RunSuperSmoother( const IPopulatorSource<HistoType> & inputSource, 
                        double smoothness= 1,
                        double span = 0,
                        bool isPeriodic = false);

    std::shared_ptr<HistoType> populate() override  final;
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const override  final;

private: 
    std::shared_ptr<IPlotPopulator<HistoType>> m_inSource;
    double  m_smoothness{1.}; 
    double m_span{0.};
    bool m_isPeriodic{false}; 
};


///==============================================
/// @brief Performs a simple dynamic cast. Will complain in case of failure. 
///==============================================
/// @tparam inType: Type of input object to receive
/// @tparam outType: Type of object to cast to 
template <class outType, class inType> class CastType final: public IPlotPopulator<outType>{
    public: 
    /// ctor. 
    /// @param inputSource: Input object to casr
    CastType(const IPopulatorSource<inType> & inputSource);
    /// populator method
    std::shared_ptr<outType> populate() override  final;
    /// clone method
    std::shared_ptr<IPlotPopulator<outType>> clonePopulator() const override  final;

    private:
    std::shared_ptr<IPlotPopulator<inType>> m_inSource{nullptr};
};

/// ==============================================================================
/// Deprecated - please consider the ExtendedPostProcessing class below for much more power! 
/// ==============================================================================
template <class OutputHistoType, class InputHistoType> class GenericPostProcessing final: public IPlotPopulator<OutputHistoType>{
public:
    GenericPostProcessing(
        /// input source - of any type
        const IPopulatorSource<InputHistoType> & inputSource, 
        /// function which produces the desired output given the input
        std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<InputHistoType>)> processingStep);
    
    std::shared_ptr<OutputHistoType> populate() override  final;
    std::shared_ptr<IPlotPopulator<OutputHistoType>> clonePopulator() const override  final;

private:
    std::shared_ptr<IPlotPopulator<InputHistoType>> m_input;
    std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<InputHistoType>)> m_processingStep; 
}; 

/// ==============================================================================
/// This class is designed as a catch-all to support arbitrary post-processing, including histogram type conversion. 
/// Can cover pretty much anything
/// The user is responsible for providing the desired functionality via a function object.
/// ==============================================================================
template <class OutputHistoType, typename... inputs> class ExtendedPostProcessing final: public IPlotPopulator<OutputHistoType>{
public:
    ExtendedPostProcessing(
        /// function which produces the desired output given the input
        std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<inputs>...)> processingStep,
        /// input source - of any type. Has to match the function arguments. 
        const IPopulatorSource<inputs>&  ... inputSources);
    
    std::shared_ptr<OutputHistoType> populate() override  final;
    std::shared_ptr<IPlotPopulator<OutputHistoType>> clonePopulator() const override  final;

    // helper c-tor for clone method
    ExtendedPostProcessing(
        /// function which produces the desired output given the input
        std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<inputs>...)> processingStep,
        /// input source - of any type
        std::tuple<std::shared_ptr<IPlotPopulator<inputs>> ...> inputSources);

private:
    std::tuple<std::shared_ptr<IPlotPopulator<inputs>>...> m_inputs;
    std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<inputs>...)> m_processingStep; 
}; 

/// ==============================================================================
/// Helper class to wrap a function that can then be used to spawn a post processor by 
/// combining it with the input populators. Can save a bit of syntax :)
/// ==============================================================================
template <class OutputHistoType, typename... types> class WrapToPostProcessor{
public:
    WrapToPostProcessor(
        /// Construct by passing the function you would like to wrap 
        std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<types>...)> processingStep);
        /// use to generate a populator (for example when creating a Plot object). 
        /// Pass as arguments the populators to use as inputs to our function.
        ExtendedPostProcessing<OutputHistoType,types...> operator()(const IPopulatorSource<types> &... inputs);

private:
    std::function<std::shared_ptr<OutputHistoType>(std::shared_ptr<types>...)> m_processingStep; 
}; 


#include "PostProcessingPopulators.ixx"

#endif
