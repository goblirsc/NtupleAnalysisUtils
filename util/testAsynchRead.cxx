// test reading several files asynchronously 

#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
#include "TStopwatch.h"

/// Test program to ensure that the single- and multi-threaded 
/// histogram fill modes yield identical outcomes. 
/// Will generate 160Mevt of dummy data in 16 files 
/// and then attempt to process these once in single-, 
/// and once in multi-threaded mode. 
/// Will then compare the outcomes bin-by-bin. 

// ntuple class for our test
class TestTree : public NtupleBranchMgr{
public:
    TestTree (TTree* t):
        NtupleBranchMgr(t),
        mass("mass",t,this),
        cutVar1("cutVar1",t,this),
        vecVar1("vecVar1",t,this){

    }
    NtupleBranch<double> mass;
    NtupleBranch<int> cutVar1; 
    NtupleBranch<std::vector<int>> vecVar1; 

};

// method to fill some random dummy data
void writeTestTree(const std::string fname, double mean, double sigma, Long64_t nevt, int seed){

    std::shared_ptr<TFile> myFile = PlotUtils::Open(fname);
    if (myFile){
        TTree* found = nullptr;
        myFile->GetObject("TestTree", found);
        if (found && found->GetEntries() == nevt){
            std::cout <<" It seems input already exists - not rerunning it"<<std::endl;
            return;
        }       
    }
    else {
        std::cout <<" Test input file does not yet exist, will prepare one"<<std::endl;
    }
    myFile = std::make_shared<TFile>(fname.c_str(),"RECREATE"); 
    TTree* myTree = new TTree("TestTree","TestTree");
    myTree->SetDirectory(myFile.get());

    // note: no need for branch() etc calls, all done for us 
    TestTree tree(myTree); 

    // prepare to populate the tree with random values
    TRandom3 my_rdm;
    my_rdm.SetSeed(seed); // fix seed to get reproducible results
    std::vector<int> vec;
    for (Long64_t entry = 0; entry < nevt; ++entry){
        // almost as before, we just talk to the tree NtupleBranches
        tree.mass.set(my_rdm.Gaus(mean, sigma));
        tree.cutVar1.set(my_rdm.Uniform(-1,4));
        size_t rsize = my_rdm.Uniform(7);
        vec.clear();
        vec.reserve(rsize);
        for (size_t k = 0; k < rsize; ++k){
            vec.push_back(my_rdm.Uniform(-10,40));
        }
        tree.vecVar1.set(vec);
        tree.fill();  // here we could just as well call myTree->Fill(); 
    }
    // and write to disk
    myFile->Write();    
}

// compare two plots for consistency
bool compare (Plot<TH1D> & h1, Plot<TH1D> & h2){
    // h1.populate();
    // h2.populate();
    bool OK = true;
    for (auto ibin = 0; ibin < h1->GetNbinsX()+2; ++ibin){
        double c1 = h1->GetBinContent(ibin);
        double e1 = h1->GetBinError(ibin);
        double c2 = h2->GetBinContent(ibin);
        double e2 = h2->GetBinError(ibin);
        if (c1 != c2){
            std::cerr <<" Content disagreement of bin # "<<ibin<<" with contents of "<<c1<<" vs " <<c2<<std::endl;
            OK = false;
        }
        if (e1 != e2){
            std::cerr <<" Error disagreement of bin # "<<ibin<<" with error of "<<e1<<" vs " <<e2<<std::endl;
            OK = false;
        }
    }
    if (OK){
        std::cout <<" TH1D Comparison sees perfect agreement!"<<std::endl;
    }
    return OK;
}

// compare two plots for consistency
bool compare (Plot<TProfile> & h1, Plot<TProfile> & h2){
    // h1.populate();
    // h2.populate();
    bool OK = true;
    for (auto ibin = 0; ibin < h1->GetNbinsX()+2; ++ibin){
        double c1 = h1->GetBinContent(ibin);
        double e1 = h1->GetBinError(ibin);
        double c2 = h2->GetBinContent(ibin);
        double e2 = h2->GetBinError(ibin);
        if (c1 != c2){
            std::cerr <<" Content disagreement of bin # "<<ibin<<" with contents of "<<c1<<" vs " <<c2<<std::endl;
            OK = false;
        }
        if (e1 != e2){
            std::cerr <<" Error disagreement of bin # "<<ibin<<" with error of "<<e1<<" vs " <<e2<<std::endl;
            OK = false;
        }
    }
    if (OK){
        std::cout <<" Profile Comparison sees perfect agreement!"<<std::endl;
    }
    return OK;
}

// compare two TEfficiencies for efficiency
bool compare (Plot<TEfficiency> & h1, Plot<TEfficiency> & h2){
    // h1.populate();
    // h2.populate();
    Plot<TH1D> den1 (CopyExisting(dynamic_cast<const TH1D*>(h1->GetTotalHistogram())));
    Plot<TH1D> den2 (CopyExisting(dynamic_cast<const TH1D*>(h2->GetTotalHistogram())));
    Plot<TH1D> num1 (CopyExisting(dynamic_cast<const TH1D*>(h1->GetPassedHistogram())));
    Plot<TH1D> num2 (CopyExisting(dynamic_cast<const TH1D*>(h2->GetPassedHistogram())));
    if ( compare (den1, den2) && compare (num1,num2)){
        std::cout <<" Efficiency Comparison sees perfect agreement!" <<std::endl;
        return true;
    }
    return false;
}


int main (int, char**){

    // write dummy data...
    // prepare a sample to of 16 files 
    std::vector<Sample<TestTree>> samples;
    std::cout << " =========== Generating test inputs (may take a while) ===========" <<std::endl;
    Sample<TestTree> bigSample;
    for (size_t k = 0; k < 16; ++k){
        std::string fname = std::string("Input.")+std::to_string(k)+".root";
        writeTestTree(std::string("Input.")+std::to_string(k)+".root",10 * k + 50,5,1000000,k);
        samples.push_back(Sample<TestTree>(std::string("Input.")+std::to_string(k)+".root","TestTree"));
        bigSample.addFile(fname, "TestTree");
    }

    std::cout << " =========== Preparing analysis ===========" <<std::endl;
    
    // define a simple selection
    Selection<TestTree> sel ([](TestTree &t){
        return t.cutVar1() >=0; 
    });

    // define a plot fill instruction
    PlotFillInstruction<TH1D, TestTree> fillMass(
                 [](TH1D* h, TestTree &t){
                    h->Fill(t.mass());
     });
    // define a plot fill instruction
    PlotFillInstruction<TH1D, TestTree> fillFromVec(
                 [](TH1D* h, TestTree &t){
            for (size_t p = 0; p < t.vecVar1.size(); ++p){
               h->Fill(t.vecVar1(p));
            }
     });

    // also add an efficiency plot
    PlotFillInstruction<TEfficiency, TestTree> fillEff(
                 [](TEfficiency* h, TestTree &t){
                    h->Fill(t.cutVar1() > 1, t.mass());
     });
    // and a profile
    PlotFillInstruction<TProfile, TestTree> cutVarProf(
                 [](TProfile* h, TestTree &t){
                    h->Fill( t.mass(), t.cutVar1());
     });


    // define histogram binnings
    TH1D massplot ("massplot","dummy mass",90,70,160);
    TProfile varprof ("varprof","dummy mass",90,70,160);
    TEfficiency effPlot ("effPlot","dummy mass",90,70,160);
    TH1D vecplot ("vecPlot","dummy mass",50,-10,40);


    // set the histo filler to single thread operation 
    HistoFiller::getDefaultFiller()->setFillMode(HistoFillMode::singleThread);

    std::vector<Plot<TH1D>> plots_single; 

    Plot<TEfficiency> effSingle(RunHistoFiller(&effPlot, bigSample, sel, fillEff) );
    Plot<TProfile> profSingle(RunHistoFiller(&varprof, bigSample, sel, cutVarProf) );

    for (auto & sample : samples){
        plots_single.push_back(Plot<TH1D> (RunHistoFiller(&massplot, sample, sel, fillMass) ));
    
        plots_single.push_back(Plot<TH1D> (RunHistoFiller(&vecplot, sample, sel, fillFromVec) ));
        }

    plots_single.push_back(Plot<TH1D> (RunHistoFiller(&massplot, bigSample, sel, fillMass) ));

    plots_single.push_back(Plot<TH1D> (RunHistoFiller(&vecplot, bigSample, sel, fillFromVec) ));

    // now time the first event loop, on one thread
    TStopwatch sw;
    sw.Start();
    std::cout << " =========== Filling on 1 thread ===========" <<std::endl;
    plots_single.front().populate();
    sw.Stop();
    double T1 = sw.RealTime();
    sw.Reset();

    /// Here we will deliberately ignore best practice and define some more plots after the first set has already been filled.
    /// This will trigger a new loop for those plots which have not yet been processed. 
    /// Normally, this is likely a sign of possible CPU waste, but here we can use it to 
    /// run a multithread event loop after having run with single-threading before

    HistoFiller fillerMT; 
    /// these plots are identical to above, but have different names w.r.t the autogenerated ones, 
    /// in order for the HistoFiller to detect them as 'new' and to trigger another fill round. 
    std::vector<Plot<TH1D>> plots_multi; 
    Plot<TEfficiency> effMulti(RunHistoFiller(&effPlot, bigSample, sel, fillEff,&fillerMT));
    Plot<TProfile> profMulti(RunHistoFiller(&varprof, bigSample, sel, cutVarProf,&fillerMT) );
    for (auto & sample : samples){
        plots_multi.push_back(Plot<TH1D> (RunHistoFiller(&massplot, sample, sel, fillMass,&fillerMT)));
        plots_multi.push_back(Plot<TH1D> (RunHistoFiller(&vecplot, sample, sel, fillFromVec,&fillerMT) ));
    }
    plots_multi.push_back(Plot<TH1D> (RunHistoFiller(&massplot, bigSample, sel, fillMass,&fillerMT)));
    plots_multi.push_back(Plot<TH1D> (RunHistoFiller(&vecplot, bigSample, sel, fillFromVec,&fillerMT) ));

    // again time the event loop
    sw.Start();
    std::cout << " =========== Filling on multi threads ===========" <<std::endl;
    plots_multi.front().populate();
    sw.Stop();
    double T2 = sw.RealTime();

    std::cout <<" Filling took "<<T1<<"s on single and "<<T2<<"s on multi thread filler"<<std::endl;
    std::cout << " =========== Validating outputs ===========" <<std::endl;

    // now to the most important bit - make sure that the results do not change 
    // when switching fill mode (would be a sign of logic issues or race conditions)
    bool pass = true;
    for (size_t k =0; k < plots_single.size(); ++k){
        pass &= compare(plots_single.at(k),plots_multi.at(k));
    }
    // also compare the "big sample" plot
    // pass &= compare(plots_single.at(plots_single.size()-2),plots_multi.at(plots_multi.size()-2));
    // pass &= compare(plots_single.back(),plots_multi.back());
    pass &= compare(effSingle,effMulti);
    pass &= compare(profSingle,profMulti);

    return pass;

}
