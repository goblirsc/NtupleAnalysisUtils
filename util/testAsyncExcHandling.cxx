
#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

// ntuple class for our test
class TestTree : public NtupleBranchMgr{
public:
    TestTree (TTree* t):
        NtupleBranchMgr(t),
        mass("mass",t,this),
        cutVar1("cutVar1",t,this),
        vecVar1("vecVar1",t,this){

    }
    NtupleBranch<double> mass;
    NtupleBranch<int> cutVar1; 
    NtupleBranch<std::vector<int>> vecVar1; 

};

// method to fill some random dummy data
void writeTestTree(const std::string fname, double mean, double sigma, Long64_t nevt, int seed){

    TFile* myFile = new TFile(fname.c_str(),"READ"); 
    if (myFile){
        TTree* found = nullptr;
        myFile->GetObject("TestTree", found);
        if (found && found->GetEntries() == nevt){
            std::cout <<" It seems input already exists - not rerunning it"<<std::endl;
            myFile->Close();
            return;
        }
        myFile->Close();
    }
    else {
        std::cout <<" Test input file does not yet exist, will prepare one"<<std::endl;
    }
    myFile = new TFile(fname.c_str(),"RECREATE"); 
    TTree* myTree = new TTree("TestTree","TestTree");
    myTree->SetDirectory(myFile);

    // note: no need for branch() etc calls, all done for us 
    TestTree tree(myTree); 

    // prepare to populate the tree with random values
    TRandom3 rdm;
    rdm.SetSeed(seed); // fix seed to get reproducible results
    std::vector<int> vec;
    for (Long64_t entry = 0; entry < nevt; ++entry){
        // almost as before, we just talk to the tree NtupleBranches
        tree.mass.set(rdm.Gaus(mean, sigma));
        tree.cutVar1.set(rdm.Uniform(-1,4));
        size_t rsize = rdm.Uniform(7);
        vec.clear();
        vec.reserve(rsize);
        for (size_t k = 0; k < rsize; ++k){
            vec.push_back(rdm.Uniform(-10,40));
        }
        tree.vecVar1.set(vec);
        tree.fill();  // here we could just as well call myTree->Fill(); 
    }
    // and write to disk
    myFile->Write();
    myFile->Close();
}
int main (int, char**){

    // write dummy data...
    // prepare a sample to of 4 files 
    std::vector<Sample<TestTree>> samples;
    std::cout << " =========== Generating test inputs (may take a while) ===========" <<std::endl;
    Sample<TestTree> bigSample;
    for (size_t k = 0; k < 4; ++k){
        std::string fname = std::string("Input.")+std::to_string(k)+".root";
        writeTestTree(std::string("Input.")+std::to_string(k)+".root",10 * k + 50,5,10000,k);
        samples.push_back(Sample<TestTree>(std::string("Input.")+std::to_string(k)+".root","TestTree"));
        bigSample.addFile(fname, "TestTree");
    }

    std::cout << " =========== Preparing analysis ===========" <<std::endl;
    
    // define a simple selection
    Selection<TestTree> sel ([](TestTree &t){
        return t.cutVar1() >=0; 
    });

    // define a plot fill instruction
    PlotFillInstruction<TH1D, TestTree> fillMass(
                 [](TH1D* h, TestTree &t){
                    h->Fill(t.mass());
     });
    // define a plot fill instruction
    PlotFillInstruction<TH1D, TestTree> fillFromVec(
                 [](TH1D* h, TestTree &t){
            for (size_t p = 0; p < t.vecVar1.size(); ++p){
               h->Fill(t.vecVar1(p));
            }
     });
    // define a plot fill instruction that will throw an exception
    PlotFillInstruction<TH1D, TestTree> fillFromVecAndCrash(
                 [](TH1D* h, TestTree &t){
            for (size_t p = 0; p < t.vecVar1.size()+2; ++p){
               h->Fill(t.vecVar1(p));
            }
     });


    // define histogram binnings
    TH1D massplot ("massplot","dummy mass",90,70,160);
    TH1D vecplot ("vecPlot","dummy mass",50,-10,40);


    Plot<TH1D> regular(RunHistoFiller(&vecplot, bigSample, sel, fillFromVec));
    Plot<TH1D> boom (RunHistoFiller(&vecplot, bigSample, sel, fillFromVecAndCrash));

    boom.populate();


    return 0;

}
