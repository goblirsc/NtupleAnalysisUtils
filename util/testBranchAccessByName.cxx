
#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

int main (int, char**){

    // write some stuff 

    TFile* fout = new TFile("testout.root","RECREATE");
    TTree* tout = new TTree("outTree","aTree");

    std::vector<double> bla2 {1,2,3,4,5,6,7,8};
    float* lepton_pt2 = new float [5] {11,21,31,41,51};
    float in_flt = 42.0;
    
    // book some branches via the wrapper
    NtupleBranch<std::vector<Double_t> > b_bla2 ("doublevector_ntb",tout);
    b_bla2.set(bla2);

    NtupleBranch<float > b_float ("float_ntp",tout);
    b_float.set(in_flt);

    NtupleBranch<float*> b_lepton_pt("lepton_pt_ntb",5,tout);
    b_lepton_pt.set(lepton_pt2);

    // populate tree, write it to the file and close it 
    tout->Fill();
    fout->Write();
    fout->Close();

    TFile* fin = new TFile("testout.root","READ");
    TTree* tin = nullptr;
    fin->GetObject("outTree", tin);

    // now, can we read it correctly? 
    NtupleBranchMgr myManager(tin);
    myManager.getMissedBranches(tin);

    NtupleBranchAccessByName<std::vector<double>> vec("doublevector_ntb");
    NtupleBranchAccessByName<float> flt("float_ntp");
    NtupleBranchAccessByName<float*> arr("lepton_pt_ntb");

    Selection<NtupleBranchMgr> dummySelection([&vec,&flt,&arr](NtupleBranchMgr &t){
        float testflt = 0;
        std::vector<double> testvec; 
        if (flt.getVal(t, testflt)) std::cout <<" read float as "<<testflt<<std::endl;
        auto got = arr.getBranch(t);
        if (got) std::cout <<" read array of size "<<got->size()<<" with first ele as "<<got->get(0)<<std::endl;
        auto gotv = vec.getBranch(t);
        if (gotv) std::cout <<" read vector of size "<<gotv->size()<<" with first ele as "<<gotv->get(0)<<std::endl;
        return true;
    }
    );

    myManager.getEntry(0);
    dummySelection(myManager);


    return 0; 
}
