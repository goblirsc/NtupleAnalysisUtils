#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
#include "TProfile2D.h"


int main(){

    auto fillNum = ConstructAndFillInPlace<TProfile2D>(
        [](TProfile2D* h){
            h->Fill(1,1,1.); 
            h->Fill(1,1,2.); 
            h->Fill(1,1,3.); 
            h->Fill(1,1,4.); 
            h->Fill(1,1,5.); 
        }, "asda","asd",1,0.,2.,1,0.,2.
    );

    auto fillDen = ConstructAndFillInPlace<TProfile2D>(
        [](TProfile2D* h){
            h->Fill(1,1,1.); 
        }, "asda","asd",1,0,2,1,0,2
    );

    Plot<TProfile2D> num (fillNum); 
    Plot<TProfile2D> den (fillDen); 
    Plot ratio (CalculateRatio<TProfile2D>(fillNum,fillDen, PlotUtils::EfficiencyMode::defaultErrors)); 
    
    std::cout <<"Num "<<num->GetBinContent(1,1)<<", den "<<den->GetBinContent(1,1)<<", ratio "<<ratio->GetBinContent(1,1)<<std::endl;



    return 0; 
}