/// Program to automatically generate NtupleBranchMgr classes 
/// for user-specified input trees. 

#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
#include <iostream> 


int main (int argc, char** argv){

    if (argc < 3 || argc > 5){
        std::cerr << "Usage: generateBranchManager <Input file> <Tree name> <optional: Class name (if replacing the tree name)>" <<std::endl;
        return 1;
    }

    std::string inputFile (argv[1]);
    std::string treeName (argv[2]);
    std::string className ("");
    if (argc >= 4) className = argv[3];

    std::shared_ptr<TFile> f;
    TTree* t_in = Sample<NtupleBranchMgr>::getObjectFromFile<TTree>(inputFile, treeName,f);
    if (!t_in){
        std::cerr << " Failed to load input tree "<<treeName<<" from file "<<inputFile<<std::endl;
        return 1;
    }
    NtupleBranchMgr myMgr(t_in);
    myMgr.getMissedBranches(t_in);
    myMgr.GenerateManagerClass(className);

    f->Close();

    return 0;
}
