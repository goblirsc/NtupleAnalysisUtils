#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

// example how to rebin a histo

std::shared_ptr<TH1D> rebin (std::shared_ptr<TH1D> in, int ntimes){
    auto out = PlotUtils::CloneHist(in); 
    out->Rebin(ntimes);
    return out; 
}

int main(int, char**){

    Plot<TH1D> myInitial(
        ConstructAndFillInPlace<TH1D>(
            [](TH1D* h){
                h->Fill(1);
                h->Fill(3);
                h->Fill(3);
                h->Fill(7);
            }, 
            "h","h",10,0.5,10.5
        )
    );

    WrapToPostProcessor<TH1D,TH1D> reBinBy2(std::bind(rebin,std::placeholders::_1,2)); 
    Plot<TH1D> halfTheBins(reBinBy2(myInitial)); 

    std::cout << "initial has "<<myInitial->GetNbinsX()<<" Bins and rebinned has "<<halfTheBins->GetNbinsX()<<" bins"<<std::endl;
    return 0; 
}
