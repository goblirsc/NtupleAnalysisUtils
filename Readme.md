# Overview

NtupleAnalysisUtils is a toolkit designed to help with the final stages of an analysis. 

Main historic use cases include 
*  Fast NTuple parsing on user resources (laptop / desktop) for physics analysis
*  NTuple post-processing (writing of an extended copy of an input Ntuple) in analysis ntuple production 
*  Fast semi-automatic drawing of large numbers of plots in physics validation  

The components of the package are intended to work nicely together while being sufficiently independent that users can pick and use only those useful to them. 

They broadly include: 
- A flexible `Plot` class that allows interaction with and on-the-fly population of ROOT objects 
- A series of classes intended to make it easy to read and write ROOT trees without boilerplate code 
- A multi-threaded, optimised ntuple event loop implementation capable of processing arbitrary trees, with support classes allowing for complex event selections, histogram filling and input samples to be configured up-front in a modular way. 
- A set of visualisation tools allowing for automatic creation of high-quality figures, including multi-page overview PDF files 
- A set of helper methods in a dedicated namespace that make the day-to-day work with ROOT easier. 

# How to set up

## Standard local build with CMake 

This package is recommended to be built using CMake within the ATLAS analysis releases.
There are no dependencies apart from ROOT and g++, so any release (AnalysisBase/AthAnalysis/ full Athena) is supported. 
It can be cloned (with a Kerberos token) via 

```shell
git clone --recursive https://:@gitlab.cern.ch:8443/goblirsc/NtupleAnalysisUtils.git
```

Please see https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorial for instructions on how to compile ATLAS packages in CMake. 

## Docker image 

There is also a docker image available which you can use for testing or building your own production images upon. 

To obtain access, you need to run 
```
docker login gitlab-registry.cern.ch  
```
once per computer, if you haven't already done this in the past. 

Then, you can  pull the image via 
``` 
docker pull gitlab-registry.cern.ch/goblirsc/ntupleanalysisutils:master
```

And run it via 
``` 
docker run --it gitlab-registry.cern.ch/goblirsc/ntupleanalysisutils:master
```

It will by default set up the latest AnalysisBase release for you. 

# Documentation 

A set of tutorials is provided as a submodule of this project.

They can also be found directly on gitlab at https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils_tutorial. 

If you recursively clone the project, you will find the tutorials in a NtupleAnalysisUtils_tutorial subfolder.

Please see the [submodule readme file](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils_tutorial/-/blob/master/README.md) for detailed instructions.  

Please also check the growing [Wiki](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils/-/wikis/home) for further up-to-date documentation.

In case of questions, please contact the maintainer for information. 
