################################################################################
# Package: NtupleAnalysisUtils
################################################################################

# Declare the package name:
atlas_subdir( NtupleAnalysisUtils )

# External dependencies:
find_package( ROOT COMPONENTS Core MathCore Hist Graf Gpad File Tree Graf2d RIO )
find_package (Boost REQUIRED)


atlas_add_library( NtupleAnalysisUtils
  NtupleAnalysisUtils/*.h Root/*.cxx
  PUBLIC_HEADERS NtupleAnalysisUtils
  INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
  PRIVATE_INCLUDE_DIRS Root 
  LINK_LIBRARIES ${ROOT_LIBRARIES} ${Boost_LIBRARIES} pthread
  # ${extra_libs}
  )


# Build all executables in util/ 
file(GLOB_RECURSE files "util/*.cxx")

foreach(_exeFile ${files})
    get_filename_component(_theExec ${_exeFile} NAME_WE)
    get_filename_component(_theLoc ${_exeFile} DIRECTORY)
    # we specify a folder for programs we do not want to compile. Useful during release transition...
    if(${_theLoc} MATCHES "DoNotBuild" OR ${_theLoc} MATCHES "ToUpdate")
    else()
      # build an executable program matching the cxx file name, 
      # linked against NTAU
      atlas_add_executable( ${_theExec}
      ${_exeFile}
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} 
      LINK_LIBRARIES ${ROOT_LIBRARIES} NtupleAnalysisUtils )
    endif()
endforeach()

# we also add unit tests. 
# Build them from the files in test/ 
file(GLOB_RECURSE tests "test/*.cxx")

foreach(_theTestSource ${tests})
    get_filename_component(_theTest ${_theTestSource} NAME_WE)

    atlas_add_test( ${_theTest} SOURCES ${_theTestSource}
    INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} 
    LINK_LIBRARIES ${ROOT_LIBRARIES} NtupleAnalysisUtils 
    POST_EXEC_SCRIPT nopost.sh 
    )
    

endforeach()
