#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"


class myTestTree: public NtupleBranchMgr{
    public: 
    myTestTree(TTree* t) : NtupleBranchMgr(t){}
    std::string color = "red"; 
};

int main(int, char**){

    std::shared_ptr<TFile> fout ( new TFile("blubb.root","RECREATE"));
    TTree* tout = new TTree("outTree","aTree");
    tout->Write(); 

    TreeRefinement<myTestTree> paintBlue{
        [](myTestTree &t){
            t.color = "blue"; 
        }
    }; 
    Sample<myTestTree> s1 ("blubb.root","outTree"); 
    Sample<myTestTree> s2 ("blubb.root","outTree", Selection<myTestTree>{}, paintBlue); 

    auto t1 = std::dynamic_pointer_cast<InputFile<myTestTree>>(s1.getInputFiles().front())->getTreeFromFile(fout); 
    std::dynamic_pointer_cast<InputFile<myTestTree>>(s1.getInputFiles().front())->getRefinement()(t1); 
    auto t2 = std::dynamic_pointer_cast<InputFile<myTestTree>>(s1.getInputFiles().front())->getTreeFromFile(fout); 
    std::dynamic_pointer_cast<InputFile<myTestTree>>(s2.getInputFiles().front())->getRefinement()(t2); 

    if (t1.color != "red"){
         std::cerr << "default case without refinement failed "<<std::endl;
        return 1;
    }
    if (t2.color != "blue"){
         std::cerr << "case with refinement failed "<<std::endl;
        return 1;
    }


    return 0; 
}
