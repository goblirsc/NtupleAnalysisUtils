#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

int main (int, char**){

    constexpr int nBins = 2; 
    constexpr double fillVal = 0.5;
    constexpr double maxVal = (double)nBins;
    constexpr double fillWeight = 10; 
    constexpr int fillBin = nBins / maxVal * fillVal + 1; 

    /// we start with a new plot, constructed in-place
    Plot<TH1D> myPlot{ConstructInPlace<TH1D>("hi","hi;x;y",nBins,0,maxVal)}; 
    myPlot->Fill(fillVal,fillWeight); 
    /// check that this worked 
    if (myPlot->GetXaxis()->GetNbins() != nBins) return 1;
    if (myPlot->GetBinContent(fillBin) != fillWeight) return 1;

    std::cout << "Test of ConstructInPlace: OK! "<< std::endl; 

    /// check the version which also fills the plot 
    Plot<TH1D> secondPlot{ConstructAndFillInPlace<TH1D>([](TH1D*h){h->Fill(fillVal,fillWeight);}, 
                                                        "hi","hi;x;y",nBins,0,maxVal)}; 
    if (secondPlot->GetXaxis()->GetNbins() != nBins) return 1;
    if (secondPlot->GetBinContent(fillBin) != fillWeight) return 1;

    std::cout << "Test of ConstructAndFillInPlace: OK! "<< std::endl; 

    /// test the copy constructor

    Plot<TH1D> copyOfSecond{CopyExisting<TH1D>(secondPlot())}; 
    /// this should match the second plot at the time of copying.
    /// Confirm by now modifying the copied plot and making sure our copy keeps the *old* content
    secondPlot->Fill(fillVal,fillWeight*10); 
    if (copyOfSecond->GetXaxis()->GetNbins() != nBins) return 1;
    if (copyOfSecond->GetBinContent(fillBin) != fillWeight) return 1;

    std::cout << "Test of CopyExisting: OK! "<< std::endl; 

    /// test loading from a file 
    TFile* ftest = new TFile("Test.root","RECREATE"); 
    ftest->WriteTObject(copyOfSecond(), "MyPlot"); 
    ftest->Close(); 

    Plot<TH1D> readFromFile{LoadFromFile<TH1D>("Test.root","MyPlot")}; 
    if (readFromFile->GetXaxis()->GetNbins() != nBins) return 1;
    if (readFromFile->GetBinContent(fillBin) != fillWeight) return 1;

    std::cout << "Test of LoadFromFile: OK! "<< std::endl; 

    /// test assignment operator - should result in a copy 
    Plot<TH1D> p2 = readFromFile;
    p2->SetBinContent(fillBin,42); 
    /// the original should not be affected
    if (readFromFile->GetBinContent(fillBin) != fillWeight) return 1;
    /// but the copy should change
    if (p2->GetBinContent(fillBin) != 42) return 1;


    return 0; 
}