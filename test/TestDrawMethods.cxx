#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
using  namespace std::placeholders;

void fillGraphErrors(TGraphErrors* g, double baseVal){
    g->SetPoint(g->GetN(), 0., baseVal);
    g->SetPoint(g->GetN(), 1., 1.* baseVal);
    g->SetPoint(g->GetN(), 2., 2.* baseVal);
}
void fillGraphAsymmErrors(TGraphAsymmErrors* g, double baseVal){
    g->SetPoint(g->GetN(), 0., baseVal);
    g->SetPoint(g->GetN(), 1., 1.* baseVal);
    g->SetPoint(g->GetN(), 2., 2.* baseVal);
}

void fillmyProfile(TProfile* g, double baseVal){
    g->Fill(1.,baseVal * 4.);
    g->Fill(2.,baseVal * 1.);
    g->Fill(3.,baseVal * 2.);
    g->Fill(4.,baseVal * 5.);
}

void fillMyEfficiency(TEfficiency* e){
    e->Fill(true, 1.);
    e->Fill(false,2.);
    e->Fill(true, 2.);
    e->Fill(true, 3.);
    e->Fill(false,4.);
    e->Fill(false,4.);
    e->Fill(true, 4.);
}

int main(int, char**){
    CanvasOptions opt; 
    opt.OutputDir("./");
    auto mpc = PlotUtils::startMultiPagePdfFile("multipage",opt); 
    PlotContent<TGraphErrors> pc_TGE{
        {
            Plot{ConstructAndFillInPlace<TGraphErrors>(std::bind(fillGraphErrors,_1,2.)), PlotFormat().LegendOption("PL").LegendTitle("1")},
            Plot{ConstructAndFillInPlace<TGraphErrors>(std::bind(fillGraphErrors,_1,3.)), PlotFormat().LegendOption("PL").LegendTitle("2")},
        },
        {},"testGraphErrs", mpc, opt
    };
    PlotContent<TGraphAsymmErrors> pc_TGAE{
        {
            Plot{ConstructAndFillInPlace<TGraphAsymmErrors>(std::bind(fillGraphAsymmErrors,_1,2.)), PlotFormat().LegendOption("PL").LegendTitle("1")},
            Plot{ConstructAndFillInPlace<TGraphAsymmErrors>(std::bind(fillGraphAsymmErrors,_1,3.)), PlotFormat().LegendOption("PL").LegendTitle("2")},
        },
        {},"testGraphAsymmErrs", mpc, opt
    };
    PlotContent<TProfile> pc_TP{
        {
            Plot{ConstructAndFillInPlace<TProfile>(std::bind(fillmyProfile,_1,2.),"prof","prof",5,0.5,5.5), PlotFormat().LegendOption("PL").LegendTitle("1")},
            Plot{ConstructAndFillInPlace<TProfile>(std::bind(fillmyProfile,_1,3.),"prof","prof",5,0.5,5.5), PlotFormat().LegendOption("PL").LegendTitle("2")},
        },
        {},"testProfile", mpc, opt
    };
    PlotContent<TEfficiency> pc_eff{
        {
            Plot{ConstructAndFillInPlace<TEfficiency>(fillMyEfficiency,"eff","eff",5,0.5,5.5), PlotFormat().LegendOption("PL").LegendTitle("1")},
            Plot{ConstructAndFillInPlace<TEfficiency>(fillMyEfficiency,"eff","eff",5,0.5,5.5), PlotFormat().LegendOption("PL").LegendTitle("2")},
        },
        {},"testTEfficiency", mpc, opt
    };

    DefaultPlotting::draw1D(pc_TGE);
    std::cout << "succesfully draw TGraphErrors "<<std::endl; 
    DefaultPlotting::draw1D(pc_TGAE);
    std::cout << "succesfully draw TGraphAsymmErrors "<<std::endl; 
    DefaultPlotting::draw1D(pc_TP);
    std::cout << "succesfully draw TProfile "<<std::endl; 
    DefaultPlotting::draw1D(pc_eff);
    std::cout << "succesfully draw TEfficiency "<<std::endl; 


    return 0; 
};
