#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

std::shared_ptr<TH1D> getCommonMax(std::shared_ptr<TH1D> h1, std::shared_ptr<TH1D> h2, std::shared_ptr<TH1D> h3){
    auto out = std::make_shared<TH1D>(*h1);
    out->Reset();
    for (int k =0 ; k < h1->GetNbinsX()+1; ++k){
        out->SetBinContent(k, std::max(h1->GetBinContent(k), std::max(h2->GetBinContent(k), h3->GetBinContent(k))));
    }   
    return out; 
}


int main (int, char**){

    /// Let's try some post-processing! 

    /// book a num and den
    /// In practice, we don't need to do this in separate lines, but more readable this way
    Plot<TH1D> myNumerator{ConstructAndFillInPlace<TH1D>([](TH1D*h){h->Fill(0.5,10);}, 
                                                        "h1","h1",1,0,1)};
    Plot<TH1D> myDenominator{ConstructAndFillInPlace<TH1D>([](TH1D* h){ h->Fill(0.5,5);}, "h2","h2",1,0,1)};

    /// now book the ratio. 
    Plot myRatio {CalculateRatio(myNumerator, myDenominator)};

    /// to prove we are not just using copies, let's mess up the original histos a bit 
    myNumerator->Fill(0.5, 123131213445); 
    myDenominator->Fill(0.5, 115151551); 
    /// The ratio between the two will no longer be two unless we correctly clone
    /// the *populators* rather than the content when defining the ratio!


    /// and check that the result is correct
    if (myRatio->GetBinContent(1) != 2) return 1; 

    std::cout << "Ratio histogram post-processing OK! "<<std::endl;

    /// let's try extracting an efficiency! 
    /// For this, we first need an efficiency histo

    Plot<TEfficiency> theEff{ConstructAndFillInPlace<TEfficiency>([](TEfficiency* eff){
        eff->Fill(true,  0.5); 
        eff->Fill(false, 0.5); 
        eff->Fill(true,  0.5); 
        eff->Fill(false, 0.5); 
    }, "Eff","my eff", 2,0,2)}; 
    /// this should give us a numerator of 2 and denominator of 4 --> eff = 0.5 +/- 0.25


    Plot<TH1D> extractedEfficiency{ExtractEfficiency(theEff)}; 
    
    /// again, demonstrate that randomly polluting the Eff after population does NOT break the 
    /// populator and the extractor chain! 
    theEff->Fill(false,0.5);
    theEff->Fill(false,0.5);
    theEff->Fill(false,0.5);
    theEff->Fill(false,0.5);

    /// and check that the result is correct
    if (extractedEfficiency->GetBinContent(1) != 0.5) return 1; 

    /// also try a 2D one 

    Plot<TEfficiency> theEff2D{ConstructAndFillInPlace<TEfficiency>([](TEfficiency* eff){
        eff->Fill(true,  0.5, 0.5); 
        eff->Fill(false, 0.5, 0.5); 
        eff->Fill(true,  0.5, 0.5); 
        eff->Fill(false, 0.5, 0.5); 
    }, "Eff2","my eff", 2,0,2,2,0,2)}; 
    /// this should give us a numerator of 2 and denominator of 4 --> eff = 0.5 +/- 0.25

    Plot<TH2D> extractedEfficiency2D{ExtractEfficiency2D(theEff2D)}; 

    /// again, demonstrate that randomly polluting the Eff after population does NOT break the 
    /// populator and the extractor chain! 
    theEff2D->Fill(false,0.5,0.5);
    theEff2D->Fill(false,0.5,0.5);
    theEff2D->Fill(false,0.5,0.5);
    theEff2D->Fill(false,0.5,0.5);

    /// and check that the result is correct
    if (extractedEfficiency2D->GetBinContent(1,1) != 0.5) return 1; 

    std::cout << "2D Effi histogram post-processing OK! "<<std::endl;

    /// now we try to normalise a histogram to bin width
    
    // two bins - first is 1, the second 2 units wide
    std::array<double, 3> theBinning{1, 2, 4}; 
    Plot<TH1D> forNorm{
        NormaliseBinsToWidth<TH1D>(ConstructAndFillInPlace<TH1D>([](TH1D* h){h->SetBinContent(1,2);h->SetBinContent(2,4);},
        "MyFunHistogram", "Hi", 2, &theBinning[0]))
    };
    /// after normalisation, both bins should have a content of 2 
    if (forNorm->GetBinContent(1) != forNorm->GetBinContent(2)) return 1; 

    std::cout << "Normalisation to bin width OK!"<<std::endl;

    /// Now we will also demonstrate how we can also pass an existing Plot as arg - the populator
    /// will then automatically be extracted for us!
    /// Now scale the guy by a constant. 
    Plot<TH1D> scaledByConst{
        ScaleByConstant<TH1D>(forNorm, 5.0)
    };

    /// To demonstrate that we are really copying the populator and not the histogram, 
    /// let's mess up the bin contents of the 
    /// original plot a bit - this should NOT affect the new plot below! 
    forNorm->Scale(1337); 

    /// after normalisation, both bins should have a content of 5 times the original (2x5=10).
    if (scaledByConst->GetBinContent(1) != 10 || scaledByConst->GetBinContent(2)!= 10) return 1; 

    // un-normalise for later tests
    forNorm->Scale(1./1337); 
    std::cout << "Scaling by a constant OK!"<<std::endl;

    /// normalisation to integrals is fun. Let's keep daisy-chaining populators! 

    Plot<TH1D> unitIntegral{ NormaliseToIntegral(scaledByConst,1,true)}; 
    /// By now, we have a chain of four steps!
    /// a) create in-place and populate 
    /// b) normalise to bin width
    /// c) scale by 5
    /// d) rescale to unit integral

    if (unitIntegral->Integral("WIDTH") != 1) return 1; 

    /// Let's also test the case where we do not account for the width
    Plot<TH1D> unitIntegral2{ NormaliseToIntegral(scaledByConst,1)}; 
    if (unitIntegral2->Integral() != 1) return 1; 

    std::cout << "Normalisation to integral OK!"<<std::endl;

    /// Test addition - add 0.5 times the plot to itself 
    Plot<TH1D> unitIntegral2WithAdd{
        LinearCombination<TH1D>{scaledByConst, scaledByConst, 0.5}
    }; 
    /// Now, both bins should have a content of 7.5 times the original (2x5x1.5 = 15).
    if (unitIntegral2WithAdd->GetBinContent(1) != 15 || unitIntegral2WithAdd->GetBinContent(2)!= 15) return 1; 

    std::cout << "Addition OK!"<<std::endl;


    /// Test the sum of histograms. For this, we will add our "for norm" hist to itself 
    /// a few times... 
    Plot<TH1D> theSum{SumContributions<TH1D>(forNorm,forNorm,forNorm,forNorm,forNorm)};
    if (theSum->GetBinContent(1) != 10 || theSum->GetBinContent(2)!= 10) return 1; 


    /// Finally, test a generic post-processing step - turn our TH1 into a TGraph!  
    Plot<TGraph> mangleTheThing{
        GenericPostProcessing<TGraph,TH1D>(unitIntegral,[](std::shared_ptr<TH1D> h){
            std::shared_ptr<TGraph> theGraph = std::make_shared<TGraph>(h.get()); 
            theGraph->SetPoint(1, theGraph->GetX()[1], theGraph->GetY()[1] * 4); 
            return theGraph; 
        })
    };
    if (mangleTheThing->GetY()[1] != unitIntegral->GetBinContent(2) * 4) return 1; 

    std::cout << "generic postprocessing into a graph OK!"<<std::endl;

    /// Finally, test the user-customised binding
    Plot<TH1D> h1 {ConstructAndFillInPlace<TH1D>(
        [](TH1D* h){h->SetBinContent(1,3); h->SetBinContent(2,0);  h->SetBinContent(3,0); }, 
        "h1","h",3,0.5,3.5)
    }; 
    Plot<TH1D> h2 {ConstructAndFillInPlace<TH1D>(
        [](TH1D* h){h->SetBinContent(1,0); h->SetBinContent(2,3);  h->SetBinContent(3,0); }, 
        "h2","h",3,0.5,3.5)
    }; 
    Plot<TH1D> h3 {ConstructAndFillInPlace<TH1D>(
        [](TH1D* h){h->SetBinContent(1,0); h->SetBinContent(2,0);  h->SetBinContent(3,3); }, 
        "h3","h",3,0.5,3.5)
    }; 
    
    WrapToPostProcessor<TH1D,TH1D,TH1D,TH1D> wrapper(getCommonMax); 
    Plot<TH1D> hMax{wrapper(h1,h2,h3)};
    hMax.populate(); 
    if (hMax->GetBinContent(1) !=  3) return 1; 
    if (hMax->GetBinContent(2) !=  3) return 1; 
    if (hMax->GetBinContent(3) !=  3) return 1; 
    std::cout << "fancy postprocessing OK!"<<std::endl;



    /// can we do a TGraph ratio? 
    Plot<TGraphErrors> tge_01{ConstructAndFillInPlace<TGraphErrors>([](TGraphErrors* g){
        g->SetPoint(g->GetN(),0,1);
        g->SetPoint(g->GetN(),1,2);
        g->SetPoint(g->GetN(),2,3);
    } )}; 
    Plot<TGraphErrors> tge_02{ConstructAndFillInPlace<TGraphErrors>([](TGraphErrors* g){
        g->SetPoint(g->GetN(),0,2);
        g->SetPoint(g->GetN(),1,4);
        g->SetPoint(g->GetN(),2,6);
    } )}; 
    auto r = PlotUtils::getRatio(tge_01,tge_02);
    r.populate(); 
    if(r->GetY()[0] != 0.5 || 
       r->GetY()[1] != 0.5 || 
       r->GetY()[2] != 0.5 || 
       r->GetX()[0] != 0 || 
       r->GetX()[1] != 1 || 
       r->GetX()[2] != 2
    ) return 1; 
    std::cout << "graph ratio processing OK!"<<std::endl;



    return 0; 
}

