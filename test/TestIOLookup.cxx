#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
#include <assert.h>

void test(bool expression, const std::string & stage){
    if (!expression){
        std::cerr << "FAIL: "<<stage << std::endl;
        exit(1);
    }
}

int main(int, char**){

    // place some dummy files 
    int dummyBranch = 0;
    for (size_t k =0; k < 8; ++k){
        auto fout = TFile::Open(std::string("NTAUIO_TestFile_"+std::to_string(k)+".root").c_str(),"RECREATE"); 
        TTree* dummy = new TTree("myTree","title"); 
        dummy->SetDirectory(fout); 
        dummy->Branch("dummyBranch",&dummyBranch); 
        dummy->Fill(); 
        fout->Write();
        fout->Close(); 
    }
    for (size_t k =8; k < 11; ++k){
        auto fout = TFile::Open(std::string("NTAUIO_TestFileOther_"+std::to_string(k)+".root").c_str(),"RECREATE"); 
        TTree* dummy = new TTree("otherTree","title"); 
        dummy->SetDirectory(fout); 
        dummy->Branch("dummyBranch",&dummyBranch); 
        dummy->Fill(); 
        fout->Write();
        fout->Close(); 
    }

    // try to read them back 
    test( PlotUtils::findFilesInDir(".",R"(NTAUIO_.*\.root)").size() == 11,"total N files found");
    test( PlotUtils::findFilesInDir(".",R"(NTAUIO_TestFile_.*\.root)").size() == 8,"restricted N files found");
    test( PlotUtils::findFilesInDir(".",R"(NTAUIO_TestFile.*\.root)","myTree").size() == 8," N files found with a tree name");
    test( PlotUtils::findFilesInDir(".",R"(NTAUIO_TestFile.*\.root)","myTree",2).size() == 2,"test restricted N files found");
    Sample<NtupleBranchMgr> testSample(".",R"(NTAUIO_.*\.root)","myTree"); 
    test(testSample.getInputFiles().size() == 8, "Sample constructor");
    return EXIT_SUCCESS;
}