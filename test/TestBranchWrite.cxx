
#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include <string>
#include <vector> 

int main (int , char** ){

    /// write a small Tree to an output file 

    TFile* fout = new TFile("blubb.root","RECREATE");
    TTree* tout = new TTree("outTree","aTree");

    /// a plain float
    float in_flt = 42.0;
    /// plus a string (gasp)
    std::string in_str = "pneumonoultramicroscopicsilicovolcanoconiosis";
    /// a vector
    std::vector<double> in_vec{1,2,3};
    /// and an array 
    double in_arr[3]={4.,5.,6.}; 

    

    NtupleBranch<std::string> b_str ("lovelyString",tout);
    b_str.set(in_str); 

    NtupleBranch<float > b_float ("lovelyFloat",tout);
    b_float.set(in_flt);

    NtupleBranch<std::vector<double> > b_vec ("lovelyVec",tout);
    b_vec.set(in_vec);

    NtupleBranch<double* > b_arr ("evilArray",3,tout);
    b_arr.set(in_arr);

    std::cout << "=== Checking auto-generated code for the float ==="<<std::endl;
    std::cout << b_float.generateMemberString()<<std::endl;
    std::cout << b_float.generateInitString()<<std::endl;

    std::cout << "=== Checking auto-generated code for the string ==="<<std::endl;
    std::cout << b_str.generateMemberString()<<std::endl;
    std::cout << b_str.generateInitString()<<std::endl;

    std::cout << "=== Checking auto-generated code for the vec ==="<<std::endl;
    std::cout << b_vec.generateMemberString()<<std::endl;
    std::cout << b_vec.generateInitString()<<std::endl;

    std::cout << "=== Checking auto-generated code for the array ==="<<std::endl;
    std::cout << b_arr.generateMemberString()<<std::endl;
    std::cout << b_arr.generateInitString()<<std::endl;

    // populate tree, write it to the file and close it 
    tout->Fill();
    fout->Write();
    fout->Close();

    ///////////////////////////////////////////////
    // now read it back in 
    ///////////////////////////////////////////////

    TFile* fin = TFile::Open("blubb.root","READ");
    TTree* tr = nullptr;
    fin->GetObject("outTree",tr);
    if (!tr){
        std::cerr << "unable to load tree"<<std::endl;
        return 1;
    }

    std::string* readStr=nullptr;
    std::vector<double>* readVec=nullptr;
    double* readArray= new double[3];
    float readFloat=0;

    tr->SetBranchAddress("lovelyFloat",&readFloat);
    tr->SetBranchAddress("lovelyVec",&readVec);
    tr->SetBranchAddress("lovelyString",&readStr);
    tr->SetBranchAddress("evilArray",readArray);

    tr->GetEntry(0); 
    
    if (readFloat != in_flt) return 1; 
    std::cout << "SUCCESS reading back a float branch "<<std::endl;
    if (*readStr != in_str) return 1; 
    std::cout << "SUCCESS reading back a string branch "<<std::endl;
    if (*readVec != in_vec) return 1; 
    std::cout << "SUCCESS reading back a vector branch "<<std::endl;
    if (readArray[0] != in_arr[0]) return 1; 
    if (readArray[1] != in_arr[1]) return 1; 
    if (readArray[2] != in_arr[2]) return 1; 
    std::cout << "SUCCESS reading back a array branch "<<std::endl;
    return EXIT_SUCCESS;
}
