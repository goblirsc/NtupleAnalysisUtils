#include "NtupleAnalysisUtils/Helpers/Concepts.h"
#include <TH1D.h> 
#include <TGraph.h> 
#include <iostream>

class myTesterNoAdd{
public:
    myTesterNoAdd(){}
    void Reset(){}
    void SetDirectory(TDirectory* ){}
};

class myTesterWithAdd{
public:
    myTesterWithAdd(){}
    void Add(myTesterWithAdd* ){}
};

int main(int, char**){

    if (hasAddMethod<myTesterNoAdd>::value){
        std::cerr <<"hasAddMethod is returning true but should not "<<std::endl;
        return 1; 
    };  

    if (!hasAddMethod<TH1D>::value){
        std::cerr <<"hasAddMethod is returning false for TH1D "<<std::endl;
        return 1; 
    };  
    if (hasAddMethod<TGraph>::value){
        std::cerr <<"hasAddMethod is returning true for TGraph "<<std::endl;
        return 1; 
    };  
    if (!hasAddMethod<myTesterWithAdd>::value){
        std::cerr <<"hasAddMethod is returning false but should return true "<<std::endl;
        return 1; 
    };  

    
    if (hasReset<TGraph>::value){
        std::cerr <<"hasReset is returning true for TGraph "<<std::endl;
        return 1; 
    };  
    if (!hasReset<TH1D>::value){
        std::cerr <<"hasReset is returning false for TH1D "<<std::endl;
        return 1; 
    };  

    if (hasSetDirectory<TGraph>::value){
        std::cerr <<"hasSetDirectory is returning true for TGraph "<<std::endl;
        return 1; 
    };  
    if (!hasSetDirectory<TH1D>::value){
        std::cerr <<"hasSetDirectory is returning false for TH1D "<<std::endl;
        return 1; 
    };  

    return 0; 
}
