
#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include <string>
#include <vector> 

int main (int , char** ){

    /// write a small Tree to an output file 

    TFile* fout = new TFile("blubbRead.root","RECREATE");
    TTree* tout = new TTree("outTree","aTree");

    /// a plain float
    float in_flt = 42.0;
    /// an int 
    int in_int = 44212312;
    /// plus a string (gasp)
    std::string in_str = "pneumonoultramicroscopicsilicovolcanoconiosis";
    /// a vector
    std::vector<double> in_vec{1,2,3};
    /// and an array 
    double in_arr[3]={4.,5.,6.}; 

    tout->Branch("lovelyFloat",&in_flt); 
    tout->Branch("lovelyInt",&in_int); 
    tout->Branch("lovelyString",&in_str); 
    tout->Branch("lovelyVec",&in_vec); 
    tout->Branch("evilArray",&(in_arr[0]),"evilArray[3]/D",3.2e6); 

    // populate tree, write it to the file and close it 
    tout->Fill();
    fout->Write();
    fout->Close();

    ///////////////////////////////////////////////
    // now read it back in 
    ///////////////////////////////////////////////

    TFile* fin = TFile::Open("blubbRead.root","READ");
    TTree* tr = nullptr;
    fin->GetObject("outTree",tr);
    if (!tr){
        std::cerr << "unable to load tree"<<std::endl;
        return 1;
    }
    NtupleBranch<float> b_float("lovelyFloat",tr);
    NtupleBranch<int> b_int("lovelyInt",tr);
    NtupleBranch<std::vector<double>> b_vec("lovelyVec",tr);
    NtupleBranch<double*> b_arr("evilArray",3,tr);
    NtupleBranch<std::string> b_str("lovelyString",tr);

    /// usually we would use a manager here, but for this example keep it minimal
    b_float.getEntry(0); 
    b_int.getEntry(0); 
    b_vec.getEntry(0); 
    b_arr.getEntry(0); 
    b_str.getEntry(0); 
    
    if (b_float() != in_flt) return 1; 
    std::cout << "SUCCESS reading back a float branch "<<std::endl;
    if (b_int() != in_int) return 1; 
    std::cout << "SUCCESS reading back a int branch "<<std::endl;
    if (b_str() != in_str) return 1; 
    std::cout << "SUCCESS reading back a string branch "<<std::endl;
    if (b_vec() != in_vec) return 1; 
    std::cout << "SUCCESS reading back a vector branch "<<std::endl;
    if (b_arr(0) != in_arr[0]) return 1; 
    if (b_arr(1) != in_arr[1]) return 1; 
    if (b_arr(2) != in_arr[2]) return 1; 
    std::cout << "SUCCESS reading back a array branch "<<std::endl;

    /// test access as floating value 
    if (b_float.asDouble() != in_flt) return 1; 
    std::cout << "SUCCESS reading a float branch via float interface "<<std::endl;
    if (b_int.asDouble(0) != (double)in_int) return 1; 
    std::cout << "SUCCESS reading a ulong branch via float interface "<<std::endl;
    NtupleBranchBase* vecAsBase = &b_vec; 
    bool vecOK = true;
    for (size_t i=0; i < vecAsBase->size(); ++i){
        vecOK &= vecAsBase->asDouble(i)==(double)in_vec.at(i);
    } 
    std::cout << "SUCCESS reading an int branch via float interface "<<std::endl;
    return EXIT_SUCCESS;
}
