#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"
#include <cmath>
/// Test the histo filler

class myCoolTree: public NtupleBranchMgr{
public:
    myCoolTree(TTree* t);
    NtupleBranch<int> var1; 
    NtupleBranch<int> var2; 
    NtupleBranch<int> var3; 
};

myCoolTree::myCoolTree(TTree* t): 
    NtupleBranchMgr(t), 
    var1("var1",t,this),
    var2("var2",t,this),
    var3("var3",t,this){
}




int main(int, char**){

    /// Let's write ourselves a small MiniTree
    TFile* fout = TFile::Open("HistoFillerTestTree.root","RECREATE"); 

    /// book our new tree
    TTree* tout = new TTree{"CoolTree","WhyDoesROOTInsistOnAnEffingTitle?"}; 
    myCoolTree coolTree(tout); 

    /// fill 10k entries with not-very-creative content
    for(int k = 0; k < 10000; ++k){
        coolTree.var1.set(k); 
        coolTree.var2.set(k*k); 
        coolTree.var3.set(-k*k); 
        tout->Fill();
    } 
    tout->Write();
    fout->Close();

    //////////////////////////
    /// Now we parse the tree using the HistoFiller 
    //////////////////////////

    /// First, define a set of selections  
    Selection<myCoolTree> firstSelection{[](myCoolTree &t){return t.var1() < 1000;}}; 
    Selection<myCoolTree> secondSelection{[](myCoolTree &t){return t.var1() > 1000;}}; 
    Selection<myCoolTree> neither = firstSelection && secondSelection; /// should accept nothing
    Selection<myCoolTree> either =  firstSelection || secondSelection; /// should accept everything

    /// test if the ID is preserved on copying
    Selection<myCoolTree> alsoNeither = neither; 

    /// and a few fill instructions
    PlotFillInstructionWithRef<TH1D,myCoolTree> fillMe1{[](TH1D* h, myCoolTree &t){h->Fill(t.var2() + t.var3());}, "firstBinning","blurp...", 5,-2.5,2.5}; 
    PlotFillInstructionWithRef<TH1D,myCoolTree> fillMe2{[](TH1D* h, myCoolTree &t){h->Fill(t.var1());}, "secondBinning","blurp...", 1000,-0.5,1000.5}; 

    /// and we also need a sample 
    Sample<myCoolTree> theInput{"HistoFillerTestTree.root","CoolTree"}; 

    /// define a few plots, re-using selections and inputs to test 
    /// the factorisation logic 
    Plot<TH1D> plot1{RunHistoFiller{theInput, firstSelection, fillMe1}};
    Plot<TH1D> plot2{RunHistoFiller{theInput, neither, fillMe1}};
    Plot<TH1D> plot3{RunHistoFiller{theInput, either, fillMe1}};
    Plot<TH1D> plot4{RunHistoFiller{theInput, firstSelection, fillMe2}};
    Plot<TH1D> plot5{RunHistoFiller{theInput, alsoNeither, fillMe2}};
    Plot<TH1D> plot6{RunHistoFiller{theInput, either, fillMe2}};

    /// print the setup of the histo filler.
    /// It should now have one queue with 1 file, 
    /// for which 3 selections with 2 plots each are connected. 
    HistoFiller::getDefaultFiller()->print();

    /// ==============================
    /// validate the content 
    /// ==============================
    if (plot2->Integral() != 0) return 1; 
    std::cout << "Empty plot: OK"<<std::endl;

    if (plot1->Integral() != 1000 && plot1->GetBinContent(plot1->GetXaxis()->FindBin(0.)) != 1000)  return 1; 
    std::cout << "==0 plot: OK "<<std::endl;

    if (plot6->Integral() != 10000 && plot6->GetBinContent(5) != 1)  return 1; 
    std::cout << "==1 plot: OK "<<std::endl;

    return 0;
}