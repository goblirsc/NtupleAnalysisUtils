#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

/// here, we try to access branches by name 

int main(int, char**){


    /// write a pair of small Trees to an output file 

    TFile* fout = new TFile("blubbReadByName.root","RECREATE");
    TTree* tout1 = new TTree("outTree1","aTree");
    TTree* tout2 = new TTree("outTree2","anotherTree");

    /// a plain float
    int in_int1 = 1;
    int in_int2 = 2;
    /// plus a string (gasp)

    tout1->Branch("myInt",&in_int1); 
    tout2->Branch("myInt",&in_int2); 

    
    // populate tree, write it to the file and close it 
    tout1->Fill();
    tout2->Fill();
    fout->Write();
    fout->Close();

    /// 
    ///////////////////////////////////////////////
    // now read it back in 
    ///////////////////////////////////////////////

    TFile* fin = TFile::Open("blubbReadByName.root","READ");
    TTree* t1 = nullptr;
    TTree* t2 = nullptr;
    fin->GetObject("outTree1",t1);
    fin->GetObject("outTree2",t2);

    /// create two completely featureless branch managers
    
    NtupleBranchMgr br1 (t1); 
    NtupleBranchMgr br2 (t2); 

    // auto-detect the available branches
    br1.getMissedBranches(t1);
    br2.getMissedBranches(t2);


    /// create a branch accessor by name 
    NtupleBranchAccessByName<int> acc_int{"myInt"}; 

    NtupleBranchAccessByName_asDouble acc_int1AsDouble("myInt");

    /// now, attempt to read
    int myInt = 0; 
    double myDouble1 = 0; 

    /// note how we can now access both branches using 
    /// the same accessor. Yay!
    if (!acc_int.getVal(br1,myInt) || myInt != in_int1) return 1; 
    if (!acc_int.getVal(br2,myInt) || myInt != in_int2) return 1; 
    if (!acc_int1AsDouble.getVal(br1,0,myDouble1) || myDouble1 != (double)in_int1) return 1; 

    std::cout << "branch access by name: OK!"<<std::endl;

    return 0; 
}