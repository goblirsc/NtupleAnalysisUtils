#include "NtupleAnalysisUtils/Plot/Generator.h"

#include "TGraph.h"
#include "TH1D.h"
#include "TH2D.h"

#include <iostream>

int main(int, char**){

    auto genTH1 = PlotUtils::bookGenerator<TH1D>("Hi","asdf",200,0,2); 
    auto genGraph = PlotUtils::bookGenerator<TGraph>(); 
    auto genTH2D = PlotUtils::bookGenerator<TH2D>("Hi again","blaah",20,-2,2,2,std::vector<double>({-1.,0.,1.}).data()); 

    if (genTH1->generateShared()->GetNbinsX() != 200) {
        std::cerr << "TH1 instantiation failed! "<<std::endl; 
        return 1;
    }
    if (genGraph->generateShared()->GetN() != 0) {
        std::cerr << "TGraph instantiation failed! "<<std::endl; 
        return 1;
    }
    if (genTH2D->generateShared()->GetNbinsX() != 20) {
        std::cerr << "TH2 instantiation failed! "<<std::endl; 
        return 1;
    } 
    // now test the clone-generators, using the outputs from our first iteration
    auto myGenFromShared = PlotUtils::bookGenerator<TH2D>(genTH2D->generateShared());
    TH2D local = genTH2D->generate();
    auto myGenFromRaw = PlotUtils::bookGenerator<TH2D>(&local);
    
    if (myGenFromShared->generateShared()->GetNbinsX() != 20) {
        std::cerr << "TH2 instantiation from shared failed! "<<std::endl; 
        return 1;
    } 
    
    if (myGenFromRaw->generateShared()->GetNbinsX() != 20) {
        std::cerr << "TH2 instantiation from ptr failed! "<<std::endl; 
        return 1;
    } 
    return 0;

}
