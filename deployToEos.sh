#!/bin/bash

# get a kerberos token 
if [[ -z "${NTAU_SERVICE_CI_PW}" ]] || [[ -z "${CERN_USER}" ]]; then
  echo "Could not find the environment variable NTAU_SERVICE_CI_PW."
  echo "If you are on a non-protected branch, this is expected and not a problem."
  echo "Otherwise, it may mean you need to set the variable in the CI config."
    exit 0 # exit successfully to avoid a failed CI on protected branches
else
  echo "${NTAU_SERVICE_CI_PW}" | kinit ${CERN_USER}@CERN.CH
fi

# overwrite the PW variable 
export NTAU_SERVICE_CI_PW=""

if [[ -z "${NTAU_CI_DEPLOY_DEST}" ]] 
then 
    echo "Please make sure NTAU_CI_DEPLOY_DEST is set!"
    exit 0 
fi 



### Upload the files to the destination
for F in $(find source/NtupleAnalysisUtils/NtupleAnalysisUtils/ -type f)
do 
    if [[ $F == *".git"* ]]
    then continue
    fi 
    Dest=${F/source\/NtupleAnalysisUtils\//include\/}
    echo "xrdcp  -rpf $F  ${NTAU_CI_DEPLOY_DEST}$Dest"
    xrdcp -rpf  $F  ${NTAU_CI_DEPLOY_DEST}$Dest
done 

for F in $(find build/x*/lib -type f)
do 
    if [[ $F == *".git"* ]]
    then continue
    fi 
    G=$(basename $F)
    echo "xrdcp -rpf $F  ${NTAU_CI_DEPLOY_DEST}/lib/$G"
    xrdcp -rpf $F  ${NTAU_CI_DEPLOY_DEST}/lib/$G
done 
