# To build an image locally,
# go into NtupleAnalysisUtils (top level)
# and call docker build .

FROM  gitlab-registry.cern.ch/atlas/athena/analysisbase:25.2.22

ADD . /ntau/source/NtupleAnalysisUtils
WORKDIR /ntau/build

RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /ntau && \
    mv ../source/NtupleAnalysisUtils/CMakeLists.forCI.txt ../source/CMakeLists.txt && \
    cmake ../source && \
    make -j4 && \
    echo "source /ntau/source/NtupleAnalysisUtils/SetupInDocker.sh" >> /home/atlas/.bashrc
