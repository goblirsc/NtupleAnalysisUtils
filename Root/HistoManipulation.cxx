#include "NtupleAnalysisUtils/Helpers/HistoManipulation.h" 
#include <TH2Poly.h>
#include <TMath.h>

double  PlotUtils::normaliseToBinWidth(TH1* h, double target_width){
    if (h == 0){
        return 0;
    }
    if (target_width<0){
        target_width = h->GetXaxis()->GetBinWidth(1);
    }
    double min = std::numeric_limits<double>::max(); 
    double max = std::numeric_limits<double>::min(); 
    for (int k = 1; k < h->GetNbinsX()+1; ++k){
        double width = h->GetXaxis()->GetBinWidth(k);
        double SF = target_width/width; 
        h->SetBinContent(k, SF*h->GetBinContent(k));
        h->SetBinError(k, SF*h->GetBinError(k));
        max = std::max(max, h->GetBinContent(k));
        min = std::min(min, h->GetBinContent(k));
    }
    // update minmax
    h->SetMaximum(max);
    h->SetMinimum(min);
    return target_width;
}

void PlotUtils::normaliseToIntegral(TH1* h, double target_integral, bool multiplyByWidth, bool includeOverflow){
    double integral = h->Integral( (includeOverflow ? 0 : 1), 
                                    h->GetNbinsX()+ (includeOverflow ? 1 : 0),(multiplyByWidth ? "WIDTH" : "")); 
    if (integral!=0) h->Scale(target_integral/integral);
}

bool PlotUtils::isAlphaNumeric(const TH1 *histo){
    return isAlphaNumeric(histo->GetXaxis()) || 
           (histo->GetDimension() >= 2 && isAlphaNumeric(histo->GetYaxis())) || 
           (histo->GetDimension() == 3 && isAlphaNumeric(histo->GetZaxis()));
}
 bool PlotUtils::isAlphaNumeric(const TAxis* axis){
    for (int b = 1; b < axis->GetNbins(); ++b){
        std::string b_label{axis->GetBinLabel(b)};
        if (b_label.size()) return true;
    }
    return false;
 }    
bool PlotUtils::isOverflowBin(const TH1 *Histo, int bin) {
    int x{-1}, y{-1}, z{-1};
    Histo->GetBinXYZ(bin, x, y, z);
    if (isOverflowBin(Histo->GetXaxis(), x)) return true;
    if (Histo->GetDimension() >= 2 && isOverflowBin(Histo->GetYaxis(), y)) return true;
    if (Histo->GetDimension() == 3 && isOverflowBin(Histo->GetZaxis(), z)) return true;
    return false;
}
bool PlotUtils::isOverflowBin(const TAxis *a, int bin) { return bin <= 0 || bin >= a->GetNbins() + 1; }

 unsigned int PlotUtils::getNbins(const TH1 *Histo) {
    if (!Histo) return 0;
    if (Histo->GetDimension() == 2) {
        const TH2Poly* poly = dynamic_cast<const TH2Poly *>(Histo);
        if (poly) return poly->GetNumberOfBins() + 1;
    }
    unsigned int N = Histo->GetNbinsX() + 2;
    if (Histo->GetDimension() >= 2) N *= (Histo->GetNbinsY() + 2);
    if (Histo->GetDimension() == 3) N *= (Histo->GetNbinsZ() + 2);
    return N;
}

void PlotUtils::shiftOverflows(TH1* H){
    if (isAlphaNumeric(H)) return;
    /// Small helper function to shift the bin along the axis
    std::function<int(TAxis*, int)> shift_bin = [](TAxis* A, int bin) {
        if (bin == 0) return 1;
        return bin - isOverflowBin(A, bin);
    };
    /// backup the Entries as any SetBinContent operation
    /// increments this variable by one unit
    const Long64_t ent = H->GetEntries();
    for (int b = getNbins(H) - 1; b >= 0; --b) {
        /// We do not need to take care of the visible bins
        if (!isOverflowBin(H, b)) continue;
        int x{0}, y{0}, z{0};
        H->GetBinXYZ(b, x, y, z);
        /// Determine the target X. In case of 1D y and z are zero        
        int t_x(shift_bin(H->GetXaxis(), x)), t_y(y), t_z(z);
        /// 2D or 3D histograms
        if (H->GetDimension() > 1) t_y = shift_bin(H->GetYaxis(), y);
        if (H->GetDimension() > 2) t_z = shift_bin(H->GetZaxis(), z);
        /// By construction this bin cannot be the same 
        int target_bin = H->GetBin(t_x, t_y, t_z);
        /// Add the bin contents and errors
        H->SetBinContent(target_bin, H->GetBinContent(target_bin) + H->GetBinContent(b));
        H->SetBinError(target_bin, std::hypot(H->GetBinError(target_bin), H->GetBinError(b)));
        /// Remove the contents from the histogram
        H->SetBinContent(b, 0);
        H->SetBinError(b, 0);
    }
    H->SetEntries(ent);
}



    /// helper for the ratio between two point values 
double PlotUtils::calcRatioError(double num, double den, double dNum, double dDen, PlotUtils::EfficiencyMode errMode){
    switch (errMode){
        case EfficiencyMode::defaultErrors: 
            return std::sqrt((dNum*dNum)/(den*den) + (dDen*dDen*num*num/(den*den*den*den)));
            break; 
        case EfficiencyMode::numeratorOnlyErrors: 
            return dNum/den;
            break;
        case EfficiencyMode::denominatorOnlyErrors: 
            return dDen*num/(den*den);
            break;
        case EfficiencyMode::efficiencyErrors: 
            /// case 1: eff < 1. Interpret as a standard efficiency ("matched / (matched + nonMatched)")
            if (num <= den){
                double m = den - num; 
                double dm = std::sqrt(std::fabs(dDen*dDen-dNum*dNum));
                return std::sqrt((num*num*dm*dm + dNum*dNum*m*m)/(den*den*den*den));
            }
            /// case 2: eff > 1. Interpret as an efficiency ratio ("(both + gained) / both") 
            else{
                double g = num - den;
                double dg = std::sqrt(std::fabs(dNum*dNum-dDen*dDen));
                return std::sqrt((g*g*dDen*dDen/(den*den*den*den))+(dg*dg/(den*den)));
            }
            break;
        default: 
            std::cerr << "Warning: You may have missed defining an error mode in calcRatioError! Please complain to Max. "<<std::endl; 
            return 0.; 
    }
}
void PlotUtils::populateRatio(TH1* ratio, const TH1* num, const TH1* den,  PlotUtils::EfficiencyMode errmode){
   bool is_alpha_num = PlotUtils::isAlphaNumeric(num) || PlotUtils::isAlphaNumeric(den);  
    for (int k = 0; k < num->GetNcells(); ++k){
         /// If the histogram has alpha numeric bin labels and the
        /// overflow is accessed then the bin content doubles
        if (is_alpha_num && PlotUtils::isOverflowBin(num,k)) continue;
        double n = num->GetBinContent(k);
        double d = den->GetBinContent(k);
        double dNum = num->GetBinError(k);
        double dDen = den->GetBinError(k);
        if (d == 0){
            ratio->SetBinContent(k,0);
            ratio->SetBinError(k,0); 
        }
        else {
            ratio->SetBinContent(k, n/d);
            ratio->SetBinError(k, calcRatioError(n,d,dNum,dDen,errmode));
        }
    }
}


std::pair<int, int> PlotUtils::findNeighbours(const TGraph* g, double x){
    auto ret = std::make_pair(-1,-1); 
    for (int k = 0; k < g->GetN(); ++k){
        if (g->GetX()[k] <= x){
            ret.first = k; 
            if (k < g->GetN()-1){
                ret.second = k + 1;
            }
        }
        else {
            return ret;   
        } 
    }
    return ret; 
}
double PlotUtils::linearInterpolate(double xTarget, double x1, double x2, double y1, double y2){
    if (x1 == x2) return y1; 
    return (y1 + (y2 - y1) / (x2 - x1) * (xTarget - x1));
}
void PlotUtils::helpGraphRatio_central(TGraph* ratio, const TGraph* num, const TGraph* den){
    if (!num || !den){
        std::cerr << "nullptr handed to PlotUtils::getRatio"<<std::endl; 
        return; 
    }
    auto numClone = std::make_unique<TGraph>(*num); 
    auto denClone = std::make_unique<TGraph>(*den); 
    numClone->Sort(); 
    denClone->Sort();
    if (!numClone->GetN() || !denClone->GetN()){
        std::cerr << "empty graph handed to PlotUtils::getRatio"<<std::endl;
        return;
    }
    double xmin = std::max(numClone->GetX()[0], denClone->GetX()[0]);
    double xmax = std::min(numClone->GetX()[numClone->GetN()-1], denClone->GetX()[denClone->GetN()-1]);

    std::vector<double> testPoints; 
    for (int k =0; k < num->GetN(); ++k){
        double x = num->GetX()[k]; 
        if (x >= xmin && x <= xmax) testPoints.push_back(x); 
    }
    for (int k =0; k < den->GetN(); ++k){
        double x = den->GetX()[k]; 
        if (x >= xmin && x <= xmax && std::find(testPoints.begin(), testPoints.end(),x) == testPoints.end()){
            testPoints.push_back(x); 
        }
    }
    std::sort(testPoints.begin(), testPoints.end());
    for (auto x : testPoints){
        double numVal = numClone->Eval(x); 
        double denVal = denClone->Eval(x); 
        if (denVal){
            ratio->SetPoint(ratio->GetN(), x, numVal/denVal);
        }
    }
}
std::shared_ptr<TH1D> PlotUtils::extractEfficiency(const TEfficiency* eff){
    
    std::shared_ptr<TH1D> effHisto{dynamic_cast<TH1D*>(eff->GetTotalHistogram()->Clone())};
    if (!effHisto){
        std::cerr << "Failed to extract 2D eff as a 1D hist "<<std::endl;
        return std::make_shared<TH1D>("bla","bla",1,0,1);
    }
    // now we populate the effi histo 
    effHisto->Reset();
    for (int k = 1; k < effHisto->GetNbinsX()+1; ++k){
        effHisto->SetBinContent(k,eff->GetEfficiency(k)); 
        /// for now, we symmetrise the errors... probably there is a nicer approach 
        effHisto->SetBinError(k,0.5 * (eff->GetEfficiencyErrorUp(k) + eff->GetEfficiencyErrorLow(k))); 
        /// catch empty bins
        if (eff->GetTotalHistogram()->GetBinContent(k) == 0){
            effHisto->SetBinContent(k,0); effHisto->SetBinError(k,0);
        }
    }
    return effHisto; 
}

std::shared_ptr<TH1D> PlotUtils::graphToHist(std::shared_ptr<TGraph> in){
    in->Sort();  
    std::vector<double> bins; 
    double* x = in->GetX(); 
    if (in->GetN() < 2){
        auto hist = std::make_shared<TH1D>("asdfasd","ASddsa",1,x[0]*0.999,x[0]*1.001);
        hist->SetBinContent(1, in->GetY()[0]); 
        return hist; 
    } 
    for (int i = 0; i < in->GetN() - 1; ++i){
        bins.push_back(x[i] + 0.5 * x[i+1] - x[i]); 
    }
    bins.push_back( x[in->GetN()-1] + 0.5 * (x[in->GetN()-1] - x[in->GetN()-2])); 
    auto hist = std::make_shared<TH1D>("sadadsd","SADASDdsa",bins.size()-1, &(bins[0]));
    for (int i = 0; i < hist->GetNbinsX();++i){
        hist->SetBinContent(i+1, in->GetY()[i]); 
        hist->SetBinError(i+1, 0); 
    }
    return hist;
}


TH1* PlotUtils::toCDF (const TH1* differential){
    if (!differential){
        return 0;
    }
    TH1* theCDF = dynamic_cast<TH1*>(differential->Clone(Form("CDF_%s",differential->GetName())));
    const double OneOverTotal = 1./(differential->Integral(0, differential->GetNbinsX()+1));
    const double OneOverTotalSq = OneOverTotal*OneOverTotal;
    double y=0;
    double dysquare=0;
    for (int k = 0; k < differential->GetNbinsX()+1; ++k){
        y += differential->GetBinContent(k)*OneOverTotal;
        dysquare+= differential->GetBinError(k)*differential->GetBinError(k)*OneOverTotalSq;
        theCDF->SetBinContent(k,y);
        theCDF->SetBinError(k,sqrt(dysquare));
    }
    return theCDF;
}

TGraphAsymmErrors* PlotUtils::toTGAE(const TH1* h_data){
    TGraphAsymmErrors* tgae_out = new TGraphAsymmErrors; 
    for (int iBin = 1; iBin < h_data->GetNbinsX()+1;++iBin){
        // veto empty 
        if (h_data->GetBinContent(iBin)< 0.0001) continue;
        double X = h_data->GetXaxis()->GetBinCenter(iBin); 
        double Y = h_data->GetBinContent(iBin); 
        double sigmaUp = 0.5*TMath::ChisquareQuantile(1.-0.1586555,2.*(Y+1))-Y;
        double sigmaDown = Y-0.5*TMath::ChisquareQuantile(0.1586555,2.*Y); 
        tgae_out->SetPoint(tgae_out->GetN(), X,Y);
        tgae_out->SetPointError(tgae_out->GetN()-1, 0., 0., sigmaDown, sigmaUp); 
    }
    return tgae_out; 
}

float PlotUtils::getQualityTest(const TEfficiency* hother, const TEfficiency* href, std::string testname){
    std::shared_ptr<TH1> href_eff {getRatio(href->GetPassedHistogram(), href->GetTotalHistogram(), PlotUtils::efficiencyErrors)};
    std::shared_ptr<TH1> hother_eff {getRatio(hother->GetPassedHistogram(), hother->GetTotalHistogram(), PlotUtils::efficiencyErrors)};
    return getQualityTest(hother_eff.get(), href_eff.get(), testname);

}

float PlotUtils::getQualityTest(const TProfile* hother, const TProfile* href, std::string testname){

  std::shared_ptr<TH1> h_ref { dynamic_cast<TH1*>(href->ProjectionX()->Clone())};
  std::shared_ptr<TH1> h_other { dynamic_cast<TH1*>(hother->ProjectionX()->Clone())};

  return getQualityTest(h_other.get(), h_ref.get(), testname);
}

float PlotUtils::getQualityTest(const TH1* hother, const TH1* href, std::string testname){
    if (hother == 0 || href == 0){
        std::cerr << " Comparison between null histograms requested! "<<std::endl; 
        return 0;
    }
    if (testname == "chi2"){
      /// need to tell the test we are comparing weighted values (e.g. profiles) here! 
      return  Chi2Test(href,hother, "WW");
    }
    if (testname == "KS"){
      return KSTest(href,hother);
    }
    std::cerr << " Unknown test '"<<testname<<"' requested! "<<std::endl; 
    return -1.0;
}

float PlotUtils::Chi2Test(const TH1* h1, const TH1* h2, const char* mode){
      return h1->Chi2Test(h2, mode); 
}

float PlotUtils::KSTest(const TH1* h1, const TH1* h2, const char* mode){
      return h1->KolmogorovTest(h2, mode);
}

template <> void PlotUtils::setPointError<TGraphErrors>(TGraphErrors* g, int i, double ex, double ey){
    g->SetPointError(i,ex,ey); 
}
template <> void PlotUtils::setPointError<TGraphAsymmErrors>(TGraphAsymmErrors* g, int i, double ex, double ey){
    g->SetPointError(i,ex,ex,ey,ey); 
}        /// Stolen from IDPVM for consistency. Sorry!
std::pair<double,double> PlotUtils::getIterRMS1D(std::shared_ptr<TH1D> input){
// evaluate  mean and RMS using iterative converfgence:
    double mean=input->GetMean();
    double RMS = input->GetRMS();
    
    // iteration parameters:
    // max iteration steps
    unsigned int ntries_max = 100;
    // width of cutting range in [RMS]
    double nRMS_width = 3.0;
    
    // iteration counters and helpers: 
    // min and max range of the histogram:
    double xmin=0.;
    double xmax=0.;
    // min and max bins of the histogram in previous iteration
    // 0-th iteration: range of the original histogram
    int binmin_was = 1;
    int binmax_was = input->GetNbinsX();
    // initial number of iteration steps
    unsigned int ntries = 0;

    // iteratively cut tails untill the RMS gets stable about mean
    // RMS stable: when input histogram range after cutting by 
    // +- 3*RMS is same as the range before cutting
    while ( ntries<ntries_max ) {    
    ++ntries;
    RMS = input->GetRMS();
    mean = input->GetMean();
    xmin = -1.0*nRMS_width*RMS + mean;
    xmax = nRMS_width*RMS + mean;
    // find bins corresponding to new range, disregard underflow
    int binmin=std::max(1,input->GetXaxis()->FindFixBin(xmin));
    // find bins corresponding to new range, disregard overflow
    int binmax=std::min(input->GetNbinsX(),input->GetXaxis()->FindFixBin(xmax));
    // end iteration if these are same bins as in prev. iteration
    if ( binmin_was==binmin && binmax_was==binmax ) {
    break;
    }
    else {
    // else set the new range and continue iteration 
    input->GetXaxis()->SetRange(binmin,binmax);
    binmin_was=binmin;
    binmax_was=binmax;
    }
    } // end of ( ntries<ntries_max ) ; iterative convergence loop
    
    // set the iteration results that are accessible to clients: 
    return std::make_pair(RMS, input->GetRMSError());    
}

std::shared_ptr<TH1D> PlotUtils::getIterRMS(std::shared_ptr<TH2D> input){
    
    std::shared_ptr<TH1D> h1D {input->ProjectionX()}; 
    h1D->Reset(); 
    for (int bin = 1; bin < h1D->GetNbinsX()+1; ++bin){
        std::shared_ptr<TH1D> hY {input->ProjectionY(Form("py_%i",bin),bin,bin)}; 
        auto res = getIterRMS1D(hY); 
        h1D->SetBinContent(bin, res.first); 
        h1D->SetBinError(bin, res.second); 
    }
    return h1D; 
}

std::shared_ptr<TH2D> PlotUtils::getIterRMS2D(std::shared_ptr<TH3D> input){
    
    std::shared_ptr<TH2D> h2D {dynamic_cast<TH2D*>(input->Project3D("yx"))}; 
    h2D->Reset(); 
    for (int binX = 1; binX < h2D->GetNbinsX()+1; ++binX){
        for (int binY = 1; binY < h2D->GetNbinsY()+1; ++binY){
            std::shared_ptr<TH1D> hZ {input->ProjectionZ(Form("py_%i_%i",binX,binY),binX,binX,binY,binY)}; 
            auto res = getIterRMS1D(hZ); 
            h2D->SetBinContent(binX,binY, res.first); 
            h2D->SetBinError(binX,binY, res.second); 
        }
    }
    h2D->GetYaxis()->SetTitle(input->GetYaxis()->GetTitle()); 
    h2D->GetXaxis()->SetTitle(input->GetXaxis()->GetTitle()); 
    return h2D; 
}

std::pair<double,double> PlotUtils::getRMS1D(std::shared_ptr<TH1D> input){
    return std::make_pair(input->GetRMS(), input->GetRMSError());    
}

std::shared_ptr<TH1D> PlotUtils::getRMS(std::shared_ptr<TH2D> input){
    
    std::shared_ptr<TH1D> h1D {input->ProjectionX()}; 
    h1D->Reset(); 
    for (int bin = 1; bin < h1D->GetNbinsX()+1; ++bin){
        std::shared_ptr<TH1D> hY {input->ProjectionY(Form("py_%i",bin),bin,bin)}; 
        auto res = getRMS1D(hY); 
        h1D->SetBinContent(bin, res.first); 
        h1D->SetBinError(bin, res.second); 
    }
    return h1D; 
}

std::shared_ptr<TH2D> PlotUtils::getRMS2D(std::shared_ptr<TH3D> input){
    
    std::shared_ptr<TH2D> h2D {dynamic_cast<TH2D*>(input->Project3D("yx"))}; 
    h2D->Reset(); 
    for (int binX = 1; binX < h2D->GetNbinsX()+1; ++binX){
        for (int binY = 1; binY < h2D->GetNbinsY()+1; ++binY){
            std::shared_ptr<TH1D> hZ {input->ProjectionZ(Form("py_%i_%i",binX,binY),binX,binX,binY,binY)}; 
            auto res = getRMS1D(hZ); 
            h2D->SetBinContent(binX,binY, res.first); 
            h2D->SetBinError(binX,binY, res.second); 
        }
    }
    h2D->GetYaxis()->SetTitle(input->GetYaxis()->GetTitle()); 
    h2D->GetXaxis()->SetTitle(input->GetXaxis()->GetTitle()); 
    return h2D; 
}
std::shared_ptr<TH1D> PlotUtils::profToTH1(std::shared_ptr<TProfile> input){
    std::shared_ptr<TH1D> ret(input->ProjectionX()); 
    ret->GetYaxis()->SetTitle(input->GetYaxis()->GetTitle()); 
    return ret; 
}
/// Specialisation for graphs
TGraph* PlotUtils::runSuperSmoother(const TGraph* theHist, 
                    double smoothness,
                    double span,
                    bool isPeriodic){

    TGraph* output (dynamic_cast<TGraph*>(theHist->Clone())); 
    TGraphSmooth tgs; 
    TGraph tmp(*theHist);
    auto smooth = tgs.SmoothSuper(&tmp,"",smoothness,span,isPeriodic); 

    output->Clear(); 
    for (int i = 0; i < theHist->GetN(); ++i){
        output->SetPoint(i, theHist->GetX()[i], smooth->Eval(theHist->GetX()[i]));
    }
    return output; 
}
