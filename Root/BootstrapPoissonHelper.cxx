#include "NtupleAnalysisUtils/Ntuple/BootstrapPoissonHelper.h"
#include <array>

BootstrapPoissonHelper::BootstrapPoissonHelper() = default;
void BootstrapPoissonHelper::reseed(ULong_t seed){
    m_random3.SetSeed(seed);
}
void BootstrapPoissonHelper::populateRandom(std::vector<double>& fillMe){

    /// Cache the quantiles of the poisson CDF for x = 0..18 
    static constexpr std::array<double, 18> steps { 0.36787944117144,
                                             0.73575888234288,
                                             0.91969860292861,
                                             0.98101184312385,
                                             0.99634015317266,
                                             0.99940581518242,
                                             0.99991675885071,
                                             0.99998975080333,
                                             0.9999988747974,
                                             0.99999988857452,
                                             0.99999998995223,
                                             0.99999999916839,
                                             0.9999999999364,
                                             0.99999999999548,
                                             0.9999999999997,
                                             0.9999999999999813,
                                             0.9999999999999989,
                                             0.9999999999999999,
    }; 
    /// temporarily fill uniform random numbers into the vector (fast) 
    m_random3.RndmArray(fillMe.size(), &(fillMe.at(0))); 

    /// then translate to poisson CDF
    std::for_each(fillMe.begin(),fillMe.end(),[&](double &f){
        /// We save another ~15% CPU by exploiting the high probability
        /// of the first three cases via explicit branches before 
        /// launching into a full-fledged array search.
        /// This addresses 90% of all values.
        if (f < steps[0]) f = 0.; 
        else if (f < steps[1]) f = 1.; 
        else if (f < steps[2]) f = 2.; 
        else{
            f = std::lower_bound(steps.begin()+3,steps.end(),f) - steps.begin(); 
        }
        return f;
    }); 
}
