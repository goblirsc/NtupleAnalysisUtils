#include "NtupleAnalysisUtils/Helpers/StringHelpers.h"

#include <stdexcept> 
#include <algorithm>

std::string PlotUtils::ResolveEnviromentVariables(std::string str) {
    while (str.find("${") != std::string::npos) {
        std::string VarName = str.substr(str.find("${") + 2, str.find("}") - 2);
        std::string EnvVar(std::getenv(VarName.c_str()) ? std::getenv(VarName.c_str()) : "");
        str = ReplaceExpInString(str, str.substr(str.find("${"), str.find("}") + 1), EnvVar);
    }
    return str;
}

std::string PlotUtils::ReplaceExpInString(std::string str, const std::string &exp, const std::string &rep) {
    size_t ExpPos = str.find(exp);
    if (ExpPos == std::string::npos) return str;
    size_t ExpLen = exp.size();
    if (rep.find(exp) != std::string::npos){
        throw std::runtime_error("Clever decision to replace everything in "+rep+" by " +exp+".... Look at it that's an endless recursion");
    }
        
    str = str.substr(0, ExpPos) + rep + str.substr(ExpPos + ExpLen, std::string::npos);
    if (str.find(exp) != std::string::npos) return ReplaceExpInString(str, exp, rep);
    return str;
}


std::string PlotUtils::pruneUnwantedCharacters(const std::string & input, const std::vector<std::string> & unwanted){

    std::string out = input; 
    for (auto & removeMe : unwanted){
        out = ReplaceExpInString(out, removeMe, "");
    }
    return out;
}   
std::string PlotUtils::RandomString(size_t length) {
    auto randchar = []() -> char {
        const char charset[] =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[rand() % max_index];
    };
    std::string str(length, 0);
    std::generate_n(str.begin(), length, randchar);
    return str;
}