#include "NtupleAnalysisUtils/Configuration/DrawObjectFormat.h"

DrawObjectFormat::DrawObjectFormat(const DrawObjectFormat & other):
    Size(other.Size,this),
    Font(other.Font,this),
    Align(other.Align,this),
    Color(other.Color,this),
    NDC(other.NDC,this),
    Style(other.Style,this),
    Width(other.Width,this){
} 
void DrawObjectFormat::operator= (const DrawObjectFormat & other){
    Size.copyFrom(other.Size, this); 
    Font.copyFrom(other.Font, this); 
    Align.copyFrom(other.Align, this); 
    Color.copyFrom(other.Color, this); 
    NDC.copyFrom(other.NDC, this); 
    Style.copyFrom(other.Style, this); 
    Width.copyFrom(other.Width, this); 
} 

