#include "NtupleAnalysisUtils/Configuration/PlotFormat.h" 
#include "NtupleAnalysisUtils/Helpers/DrawFunctions.h" 

ColorPalette::ColorPalette(Color_t color_code, unsigned int color_contours):    
    m_contour{color_contours},
    m_color_choice{color_code} {}
ColorPalette::ColorPalette(const std::vector<ColorPoint>& coloring,
                            unsigned int color_gradient):  
    m_contour{color_gradient} {       
        std::vector<double> red{}, blue{}, green{}, step_size{};
        for (const ColorPoint& col : coloring){
            red.emplace_back(col.red);
            blue.emplace_back(col.blue);
            green.emplace_back(col.green);
            step_size.emplace_back(col.location);
        }
        const Int_t FI = TColor::CreateGradientColorTable(coloring.size(), 
                                                          step_size.data(), 
                                                          red.data(), 
                                                          green.data(), 
                                                          blue.data(), color_gradient);
        m_color_code.resize(color_gradient);
        for (unsigned int i = 0; i < m_color_code.size(); ++i) m_color_code[i] = FI + i;        
}    
bool ColorPalette::isValid() const {
    return m_contour > 0 && (!m_color_code.empty()|| m_color_choice);
}
void ColorPalette::setPalette() {
    if(!m_color_code.empty()) gStyle->SetPalette(m_color_code.size(), m_color_code.data());
    else if (m_color_choice) gStyle->SetPalette(m_color_choice);
}  
unsigned int ColorPalette::getContour() const  {return m_contour;}
Color_t ColorPalette::getColorCode() const {return m_color_choice;}




PlotFormat PremadePlotFormats::DataMarkers(){
    return PlotFormat().MarkerStyle(kFullDotLarge).Color(kBlack);
}

PlotFormat PremadePlotFormats::Markers1(){
    return PlotFormat().Color(kRed).MarkerStyle(kFullDotLarge);

}
PlotFormat PremadePlotFormats::Markers2(){
    return PlotFormat().Color(kBlue+1).MarkerStyle(kFullSquare);
}
PlotFormat PremadePlotFormats::Markers3(){
    return PlotFormat().Color(kOrange-3).MarkerStyle(kOpenCircle);
}
PlotFormat PremadePlotFormats::Markers4(){
    return PlotFormat().Color(kGreen+2).MarkerStyle(kOpenDiamond);
}
PlotFormat PremadePlotFormats::Markers5(){
    return PlotFormat().Color(kViolet-2).MarkerStyle(kFullTriangleDown).MarkerScale(1.3);
}
PlotFormat PremadePlotFormats::Markers6(){
    return PlotFormat().Color(kBlue-6).MarkerStyle(kOpenTriangleUp).MarkerScale(1.3);
}

PlotFormat PremadePlotFormats::Lines1(){
    return PlotFormat().Color(kBlue+1).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines2(){
    return PlotFormat().Color(kRed).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines3(){
    return PlotFormat().Color(kOrange-3).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines4(){
    return PlotFormat().Color(kGreen-2).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines5(){
    return PlotFormat().Color(kViolet-2).LineWidth(2).MarkerStyle(kDot);
}
PlotFormat PremadePlotFormats::Lines6(){
    return PlotFormat().Color(kBlue-6).LineWidth(2).MarkerStyle(kDot);
}


PlotFormat PremadePlotFormats::AtlasMark(size_t i){
    if (i > 9){
        std::cerr << "AtlasMark is only available up to i = 9! "<<std::endl;
        return PlotFormat();
    }
    static const std::array<Marker_t,10> markers{
        kFullDotLarge,
        kFullTriangleUp,
        kFullSquare,
        kFullDiamond,
        kFullCrossX,
        kOpenCircle,
        kOpenTriangleUp,
        kOpenSquare,
        kOpenDiamond,
        kOpenCrossX
    };
    return PlotFormat().Color(PlotUtils::getAtlasColor(i)).MarkerStyle(markers.at(i)).LegendOption("PL");
} 
PlotFormat PremadePlotFormats::AtlasLine(size_t i){
    if (i > 9){
        std::cerr << "AtlasLine is only available up to i = 9! "<<std::endl;
        return PlotFormat();
    }
    return PlotFormat().Color(PlotUtils::getAtlasColor(i)).LineWidth(2).MarkerStyle(kDot).LegendOption("L").ExtraDrawOpts("HIST");
}
PlotFormat PremadePlotFormats::AtlasFill(size_t i){
    if (i > 9){
        std::cerr << "AtlasFill is only available up to i = 9! "<<std::endl;
        return PlotFormat();
    }
    return PlotFormat().Color(PlotUtils::getAtlasColor(i)).FillStyle(1001).MarkerStyle(kDot).LegendOption("F").ExtraDrawOpts("HIST");  
} 

PlotFormat::PlotFormat(const PlotFormat & other):
    Color(other.Color, this),
    paletteFor2DPlotting(other.paletteFor2DPlotting,this),
    FillColor(other.FillColor, this),
    MarkerColor(other.MarkerColor, this),
    LineColor(other.LineColor, this),
    FillAlpha(other.FillAlpha, this),
    MarkerAlpha(other.MarkerAlpha, this),
    LineAlpha(other.LineAlpha, this),
    FillStyle(other.FillStyle, this),
    MarkerStyle(other.MarkerStyle, this),
    LineStyle(other.LineStyle, this),
    MarkerSize(other.MarkerSize, this),
    MarkerScale(other.MarkerScale, this),
    LineWidth(other.LineWidth, this),
    ExtraDrawOpts(other.ExtraDrawOpts, this),
    LegendOption(other.LegendOption, this),
    LegendTitle(other.LegendTitle, this),
    ShouldGoOnStack(other.ShouldGoOnStack, this),
    CustomInt(other.CustomInt,this), 
    CustomFloat(other.CustomFloat,this), 
    CustomString(other.CustomString,this) {
}

    /// ============================================

PlotFormat& PlotFormat::operator=(const PlotFormat & other){
    if (this == &other) return *this;
    Color.copyFrom(other.Color,this);
    paletteFor2DPlotting.copyFrom(other.paletteFor2DPlotting,this);
    FillColor.copyFrom(other.FillColor,this);
    MarkerColor.copyFrom(other.MarkerColor,this);
    LineColor.copyFrom(other.LineColor,this);
    FillAlpha.copyFrom(other.FillAlpha,this);
    MarkerAlpha.copyFrom(other.MarkerAlpha,this);
    LineAlpha.copyFrom(other.LineAlpha,this);
    FillStyle.copyFrom(other.FillStyle,this);
    MarkerStyle.copyFrom(other.MarkerStyle,this);
    LineStyle.copyFrom(other.LineStyle,this);
    MarkerSize.copyFrom(other.MarkerSize,this);
    MarkerScale.copyFrom(other.MarkerScale,this);
    LineWidth.copyFrom(other.LineWidth,this);
    ExtraDrawOpts.copyFrom(other.ExtraDrawOpts,this);
    LegendOption.copyFrom(other.LegendOption,this);
    LegendTitle.copyFrom(other.LegendTitle,this);
    ShouldGoOnStack.copyFrom(other.ShouldGoOnStack,this);
    CustomInt.copyFrom(other.CustomInt,this); 
    CustomFloat.copyFrom(other.CustomFloat,this);
    CustomString.copyFrom(other.CustomString,this);
    return *this;
}
