#include "NtupleAnalysisUtils/Ntuple/NtupleBranch.h"
#include "NtupleAnalysisUtils/HistoFiller/HistoFiller.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TFile.h"
#include <future>
#include <thread>
#include <map>
#include <cstdlib>

 void HistoFiller::enableImplictMT(unsigned int num_threads) {
    num_threads = std::min(num_threads, std::thread::hardware_concurrency());
    ROOT::EnableImplicitMT(num_threads);
    NtupleBranchMgr::setBranchLoopMechanism(NtupleBranchMgr::BranchLoopMechanism::accessViaTTree);
    std::cout<<"HisoFiller() -- INFO Enable MT TTree access"<<std::endl;
      
 }


HistoFiller::HistoFiller(HistoFillMode mode){
    m_fillMode = mode; 
    m_ranFill=false;
    m_canAdd=true;
    m_nThreads = std::thread::hardware_concurrency();
    ROOT::EnableThreadSafety();    
}
void HistoFiller::setNThreads(unsigned int n){
    if (n == 0) return;
    m_nThreads = std::min(n, std::thread::hardware_concurrency());
}
HistoFiller*  HistoFiller::getDefaultFiller() {
    static HistoFiller my_instance; 
    return &my_instance;
} 

void HistoFiller::setFillMode(HistoFillMode mode){
    m_fillMode = mode;
}

void HistoFiller::setValidateInputs(bool doIt){
    m_validateInputs = doIt;
}

void HistoFiller::setAllowEmptyInputs(bool allow){
    m_allowEmptyFiles = allow; 
}

HistoFillMode HistoFiller::getFillMode(){
    return m_fillMode;
}
bool HistoFiller::attach(const IFillable & plot){
    if(!hasPlot(plot)){  
        m_connected_plots.push_back(plot.clone());
        m_connected_IDs.emplace(plot.getID(), m_connected_plots.size()-1);
        if (!plot.canAdd()){        // also check if the plot has an Add method
            m_canAdd = false; 
        }
        return true;
    }
    return false;
}
void HistoFiller::ResetFillResults(){
    // for (auto & p : m_connected_plots2){
    for (auto & p : m_connected_plots){
        p->Reset(); 
    }
    m_ranFill = false; 
}
void HistoFiller::DisconnectAll(){
    ResetFillResults();
    m_connected_plots.clear();
    m_connected_IDs.clear();
    m_canAdd = true;
}
 
bool HistoFiller::hasPlot(const IFillable & plot){
    return (m_connected_IDs.count(plot.getID())!=0);
}

 void HistoFiller::fillPlots(){
    // split things into a list of tasks (one per file)  
    std::vector<MyHistoFillTask> tasks = generateTasks();
    // and then process all of them
    if (m_canAdd && m_fillMode == HistoFillMode::multiThread){
        fillPlotsParallel(tasks);
    }
    else {
        fillPlotsSequentially(tasks);
    }
    // pass a line ending to the out stream 
    std::cout << std::endl;
}

// attempt to find the requested plot by name. 
// will trigger an event loop if needed
HistoFiller::MyFillable HistoFiller::getOutcome (const IFillable & plot){
    if (!m_ranFill) fillPlots();            // did not yet fill any plots? Then time for the loop
    if (!hasPlot(plot)){
        std::cerr <<" Careful! You requested a non-available plot "<<std::endl;
        return nullptr;         
    }
    auto thePlotPtr = m_connected_plots.at(m_connected_IDs[plot.getID()]); 
    if (!thePlotPtr->hasBeenFilled()) {             // warn the user that what they are doing is likely not optimal. 
                                                    // here, we actually need an additional event loop. 
        std::cout <<"    Found a plot that has not yet been filled "<<std::endl;
        std::cout <<"    after running the main event loop."<<std::endl;
        std::cout <<"    Will run a new loop, filling only previously missing items. "<<std::endl;
        std::cout <<"    This may mean that you can improve efficiency by defining this plot "<<std::endl;
        std::cout <<"    before the first call to Plot::populate()"<<std::endl;
        fillPlots();                    // loop again with the histos not yet filled. 
    }
    // finally, return a clone of the provided plot - ensures that subsequent getOutput calls 
    // return the same thing and not something messed up by previous clients. 
    return thePlotPtr->clone();
}

HistoFiller::MyHistoFillTaskList HistoFiller::generateTasks(){
    
    /// here, we have to convert the 
    /// Sample + Selection + FillInstruction + Plot flat hierarchy 
    /// into a tree-like File --> [ Selection ---> [FillInstruction+Plot] x n ] x m structure.  

    MyHistoFillTaskList tasks; 

    // This is a nested map - map - vector construct. 
    // Uses a custom sort operation checking the name of the objects 
    // represented by the shared pointers used as keys 
    IInputFile::FileToSelectionListMap  ConstructionMap; 

    bool splitAndMerge = (m_fillMode==HistoFillMode::multiThread && m_canAdd);
    for (auto plot : m_connected_plots){
        // don't fill again if already done
        if (plot->hasBeenFilled()) continue; 
        // the plot will now be filled, so update the flag
        plot->setHasBeenFilled();       
        // add an entry for our plot to the clone association map

        // loop over all input files for this plot
        for (auto  & inputFile : plot->getSampleClone()->getInputFiles()){

            // exploit that emplace returns an iterator to the existing element 
            // if a key is already present in the map
            auto emplaceResultFile = ConstructionMap.emplace(inputFile, 
                     IInputFile::SelectionToFillableMap()
             );
            // if the file already exists, update the BS toys if needed
            if (!emplaceResultFile.second){
                if (inputFile->getNBootstrapWeights() > emplaceResultFile.first->first->getNBootstrapWeights()){
                    emplaceResultFile.first->first->setNBootstrapWeights(inputFile->getNBootstrapWeights());
                }
            }
            // this is the map of selections to fillable objects for this input file. 
            IInputFile::SelectionToFillableMap & subMap = emplaceResultFile.first->second;

            // now we need to check the selection.
            // again, we use the emplace trick. 
            auto emplaceResultSele = subMap.emplace(plot->getSelectionClone(), std::vector<MyFillable>{});

            // this is the vector of fillable objects for the file and sample belonging to this plot
            std::vector<MyFillable> & plotSet = emplaceResultSele.first->second;
            plotSet.push_back(plot);

        }
    }
    // that was ugly, I know ;-) 
    // Now let's assemble the task list and get out of here before someone sees us...
    for (auto & KV : ConstructionMap){
        tasks.push_back(KV.first->spawnTask(KV.second, splitAndMerge));
    }
    return tasks;
}

size_t HistoFiller::getNthreads() const{   
    
    size_t batchSize = m_nThreads;

    // allow the user to force the max number of threads via an environment variable: 
    const char* env_nthreads = std::getenv("NTAU_NTHREADS"); 
    if (env_nthreads){
        size_t aux_nthreads = (size_t)std::atoi(env_nthreads);
        // do not allow the user to force more threads than available on the system 
        if (aux_nthreads < batchSize) batchSize = aux_nthreads; 
    } 
    std::cout <<"HistoFiller will run with up to "<<batchSize<<" threads. "<<std::endl;
    return batchSize;

}

void HistoFiller::silenceProgress(bool doIt){
    m_silenceProgress = doIt; 
}

bool HistoFiller::doSilenceProgress() const{
    return m_silenceProgress || (std::getenv("NTAU_SILENCE_PROGRESS")!=nullptr); 
}


bool HistoFiller::runProcessingThread(std::shared_ptr<ProcessingQueue> theQueue) const{
    // get the next thing to do 
    do {
        auto theTask = theQueue->getNext(); 
        if (!theTask) return true; 
        theTask->run(theQueue);

    } while(true); 
    return true; 
}

// parallel event loop - spawn N threads and have each of them start 
// processing files until all are finished.  
 void HistoFiller::fillPlotsParallel(MyHistoFillTaskList & tasks){

    
    auto theQueue = std::make_shared<ProcessingQueue>(tasks);
    if (m_validateInputs && !theQueue->performInputValidation(m_allowEmptyFiles)){
        std::cerr << "Failed input validation, not entering event loop. "<<std::endl;
        return; 
    }
    if (!m_validateInputs){
        std::cout << "Input validation disabled by the user - unknown total event count. E.T.A estimation in the loop will not be available." <<std::endl;
    }
    if (doSilenceProgress()){
        std::cout << "Progress printout disabled by the user - will not print progress bar. "<<std::endl; 
        theQueue->setPrintInterval(ProcessingQueue::noPrintout); 
    }
    const size_t batchSize = std::min(getNthreads(), theQueue->numberOfTasks());
    // spawn a set of workers 
    std::vector<std::future<bool> > myTasks;
    myTasks.reserve(batchSize); 
    for (size_t i_task = 0; i_task < batchSize; ++i_task){
        myTasks.emplace_back(std::async(std::launch::async, &HistoFiller::runProcessingThread, this,  theQueue));
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    for (auto & task: myTasks){
        task.get(); 
    }
    // remember that we now ran the loop! 
    m_ranFill = true;
 }

// sequential event loop - use same infrastructure as above, but directly 
// process files in the parent thread
void HistoFiller::fillPlotsSequentially(MyHistoFillTaskList & tasks){
    auto theQueue = std::make_shared<ProcessingQueue>(tasks);
    if (m_validateInputs && !theQueue->performInputValidation(m_allowEmptyFiles)){
        std::cerr << "Failed input validation, not entering event loop. "<<std::endl;
        return; 
    }
    if (!m_validateInputs){
        std::cout << "Input validation disabled by the user - unknown total event count. E.T.A estimation in the loop will not be available." <<std::endl;
    }
    if (doSilenceProgress()){
        std::cout << "Progress printout disabled by the user - will not print progress bar. "<<std::endl; 
        theQueue->setPrintInterval(ProcessingQueue::noPrintout); 
    }
    runProcessingThread(theQueue); 
    m_ranFill = true; 
}


void HistoFiller::print(){
    int nthr = getNthreads(); 
    std::vector<MyHistoFillTask> tasks = generateTasks();
    /// undo the side effects of generateTasks, so that the print method
    /// will not prevent a future event loop 
    ResetFillResults();
    for (auto plot : m_connected_plots){
        plot->setHasBeenFilled(false); 
    }
    auto theQueue = std::make_shared<ProcessingQueue>(tasks);
    const std::string spacer = "    "; 
    std::cout << "===== HistoFiller instance ==================================== "<<std::endl;
    std::cout << spacer<< "Number of threads: "<<nthr<<std::endl;
    std::cout << spacer<< "Splittable inputs: " <<m_canAdd<<std::endl;
    std::cout << spacer<< "Connected plots (total): " <<m_connected_plots.size()<<std::endl;
    std::cout << spacer<< "Fill Mode: " <<(m_fillMode==HistoFillMode::singleThread ? "Single Threaded" : "Multi-Threaded")<<std::endl;
    theQueue->print(spacer); 
    std::cout << "============================================================================== "<<std::endl;

}
