#include "NtupleAnalysisUtils/Helpers/PdgRounding.h"
#include <cmath> 

// The basic rule states that if the three highest order digits of the error lie between 100
// and 354, we round to two significant digits. If they lie between 355 and 949, we round
// to one significant digit. Finally, if they lie between 950 and 999, we round up to 1000
// and keep two significant digits.

PlotUtils::valWithAsymmErr PlotUtils::pdgRound(const valWithAsymmErr & v){
    // find the larger error 
    double mainError = (v.error_down > v.error_up ? v.error_down : v.error_up);
    // shift until we have an error in 0.XXX format 

    // null error: do not do any rounding
    if (mainError == 0) return v;

    double shiftFactor = 1.;
    while (mainError < 0.100 || mainError >= 1.00){
        if (mainError < 0.1){
            shiftFactor = 10. * shiftFactor;
            mainError = 10. * mainError;
        }
        if (mainError >= 1.00){
            shiftFactor = 0.1 * shiftFactor;
            mainError = 0.1 * mainError;
        }
    }

    double aux_cen = shiftFactor * v.cen; 
    double aux_up = shiftFactor * v.error_up; 
    double aux_down = shiftFactor * v.error_down; 
    if (mainError < 0.355){
        // 2 digit rounding
        aux_cen = 0.01 * std::roundf(100. * aux_cen);
        aux_up = 0.01 * std::roundf(100. * aux_up); 
        aux_down = 0.01 * std::roundf(100. * aux_down); 
    }
    else{
        // 1 digit rounding - I *think* this covers both the < 0.95 and > 0.95 cases(?)
        aux_cen = 0.1 * std::roundf(10. * aux_cen);
        aux_up = 0.1 * std::roundf(10. * aux_up); 
        aux_down = 0.1 * std::roundf(10. * aux_down); 
    }

    aux_cen /= shiftFactor;
    aux_up /= shiftFactor;
    aux_down /= shiftFactor;

    return valWithAsymmErr{aux_cen, aux_up, aux_down};
}

// internally leverage the asymm version
std::pair<double,double> PlotUtils::pdgRound(const std::pair<double,double> & v){
    valWithAsymmErr vwA{v.first,v.second,v.second};
    auto rounded = pdgRound(vwA);
    return std::make_pair(rounded.cen, rounded.error_down);
}

// int PlotUtils::getPower(double centralVal){
//     auto cen = std::abs(centralVal);
//     int power = 0; 
//     if (!cen) return power;
//     while (cen < 1){
//         --power; 
//         cen *= 10.; 
//     }
//     while (cen > 10.){
//         ++power;
//         cen /= 10.; 
//     }
//     return power;
// }
