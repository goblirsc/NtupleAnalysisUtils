#include "NtupleAnalysisUtils/Ntuple/NtupleBranchMgr.h"
#include "NtupleAnalysisUtils/Ntuple/NtupleBranch.h"
#include <algorithm>
#include <mutex>
#include <fstream>
#include <iostream>
#include <ctime>

using std::string; 
namespace{
    static std::mutex g_mutex_branchDisConnect;  
}

NtupleBranchMgr::BranchLoopMechanism NtupleBranchMgr::s_br_loop = NtupleBranchMgr::BranchLoopMechanism::directBranchAccess;
void NtupleBranchMgr::setBranchLoopMechanism(NtupleBranchMgr::BranchLoopMechanism m) {s_br_loop = m;}
NtupleBranchMgr::NtupleBranchMgr(TTree* t):
    m_tree{t} { 
    if (m_tree && !m_tree->GetListOfBranches()->IsEmpty()){
        m_tree->SetBranchStatus("*", 0);
    }    
}
void NtupleBranchMgr::attach(NtupleBranchBase * ntb){
    if (!ntb) return;
    m_mapBranches.insert(std::make_pair(ntb->getName(), ntb)); 
    m_vecBranches.push_back(ntb);
}
void NtupleBranchMgr::detachActive(NtupleBranchBase* ntb){
    std::lock_guard<std::mutex> guard(g_mutex_branchDisConnect);
    std::vector<NtupleBranchBase*>::iterator itr = std::find(m_vecActive.begin(), m_vecActive.end(), ntb);
    if (itr != m_vecActive.end()) m_vecActive.erase(itr);        
}
NtupleBranchMgr::~NtupleBranchMgr() = default;
Long64_t NtupleBranchMgr::getEntry(Long64_t entry){
    Long64_t read = 0;
    m_currEntry = entry;
    // here, we only loop over the active branches.
    // a branch will enter itself into this list when connecting for the first read access.
    // the first getEntry call is done via a call to getCurrEntry in connect(). 
    if (s_br_loop == BranchLoopMechanism::directBranchAccess) {
        for (NtupleBranchBase* b : m_vecActive) {
            read += b->getEntry(entry);
        }
    } else {
        read = m_tree->GetEntry(entry);   
    }
    return read;
}

void NtupleBranchMgr::fill(){
    if (m_tree) m_tree->Fill();
}
std::vector<NtupleBranchBase *> NtupleBranchMgr::getActiveBranches() const{
    return m_vecActive;
}
NtupleBranchBase* NtupleBranchMgr::getBranch (const std::string& name) const{
    auto found = m_mapBranches.find(name); 
    if (found != m_mapBranches.end()){
        return found->second;
    }
    else return nullptr;
}
bool NtupleBranchMgr::hasBranchInTree (const std::string& name) const{
    NtupleBranchBase* found = getBranch(name);
    return (found && found->existsInTree());
}
std::map<std::string, NtupleBranchBase*> NtupleBranchMgr::getBranchesStartingWith(const std::string & start) const {
    std::map<std::string, NtupleBranchBase*> out; 
    for (auto& br : m_mapBranches){
        const std::string& name = br.first; 
        if (name.find(start) == 0){
            out.insert(br);
        }
    }
    return out;
} 

std::map<std::string, NtupleBranchBase*> NtupleBranchMgr::getBranchesEndingWith(const std::string & end) const{
    std::map<std::string, NtupleBranchBase*> out; 
    size_t nend = end.size();
    for (auto& br : m_mapBranches){
        const std::string& name = br.first; 
        if (name.rfind(end) == name.size()-nend){
            out.insert(br);
        }
    }
    return out;
} 


void NtupleBranchMgr::registerActive(NtupleBranchBase * ntb){
    if (std::find(m_vecActive.begin(),m_vecActive.end(),ntb) != m_vecActive.end() ){
        std::cerr <<" Careful, branch double activation? This smells like a bug"<<std::endl;
    }
    else {m_vecActive.push_back(ntb);  }
}


NtupleBranchBase* NtupleBranchMgr::generateBranch(TLeaf* leaf, TTree* t, const std::string & prefix){
    if (!leaf) return nullptr;
    TString typeName   = leaf->GetTypeName();
    std::string brName = prefix+leaf->GetTitle();
    TString titleName  = leaf->GetTitle();
    std::string branchName = prefix+leaf->GetName();
    // don't duplicate anything
    if (m_mapBranches.find(branchName) != m_mapBranches.end()){
        return  nullptr;
    }
    NtupleBranchBase* theBranch= nullptr;
    // cumbersome but meeeh : We have to create the appropriate branch type based on what we find! 
    bool isArray = (titleName.Contains("[") && titleName.Contains("]"));
    bool isVector = (typeName.Contains("vector<"));
    bool isMatrix = (typeName.Contains("vector<vector"));   
    const size_t size = isArray ? TString(titleName(titleName.First("[")+1, titleName.First("]") - titleName.First("[")-1)).Atoi(): 0;
    // std::cout << typeName << "  " << brName <<" "<< titleName << "  "<<branchName << std::endl;

    // bad style, but really saves a lot of typing! 
    #define SETUP_FORTYPE(theType) if (typeName.Contains(#theType)) {\
                if (isArray) theBranch = new NtupleBranch<theType *>(branchName, size, t, this); \
                else if (isMatrix) theBranch = new NtupleBranch<std::vector<std::vector<theType>>>(branchName, t, this); \
                else if (isVector) theBranch = new NtupleBranch<std::vector<theType> >(branchName,t,this); \
                else theBranch = new NtupleBranch<theType>(branchName, t, this); }

    SETUP_FORTYPE(Float_t)
    else SETUP_FORTYPE(Double_t)
    else SETUP_FORTYPE(ULong64_t)
    else SETUP_FORTYPE(Long64_t)
    else SETUP_FORTYPE(UShort_t)
    else SETUP_FORTYPE(UInt_t)
    else SETUP_FORTYPE(Bool_t)
    else SETUP_FORTYPE(Int_t)
    else SETUP_FORTYPE(Short_t)
    else SETUP_FORTYPE(UInt_t)
    else SETUP_FORTYPE(double)
    else SETUP_FORTYPE(float)
    else SETUP_FORTYPE(int)
    else SETUP_FORTYPE(bool)
    else SETUP_FORTYPE(long)
    else SETUP_FORTYPE(short)
    else SETUP_FORTYPE(UChar_t)
    else SETUP_FORTYPE(Char_t)
    else SETUP_FORTYPE(string)
    else if (typeName.Contains("char*")){   
        theBranch = new NtupleBranch<char*>(branchName, size, t, this);
    }
    else SETUP_FORTYPE(char)
    if (theBranch){
        // attachment is already done in branch c-tor
        m_cleanUp.emplace_back(theBranch);
    }  
    else {
        std::cout << "NOTE: Branch "<<branchName << " of type "<<typeName<<" could not be automatically added to the NtupleBranchMgr instance. Please manually declare an NtupleBranch instance to work with it"<<std::endl;
    }
    return theBranch; 
}


void NtupleBranchMgr::getMissedBranches(TTree* t, const std::string& prefix ){
    if (!t) return;
    TObjArray* listLf = t->GetListOfLeaves();
    for (Int_t i = 0; i < listLf->GetEntries(); i++){
        TLeaf* lf = (TLeaf*) listLf->At(i);
        generateBranch(lf, t, prefix);       
    }
}

void NtupleBranchMgr::addSkippedBranch(const std::string & skipMe){
    m_skipBranchList.push_back(skipMe);
}
void NtupleBranchMgr::addSkippedBranches(std::vector<std::string> & skipThose){
    m_skipBranchList.insert(m_skipBranchList.end(), skipThose.begin(), skipThose.end());
}
void NtupleBranchMgr::populateCopy(NtupleBranchMgr & tree){
    auto ttree = tree.getTree();
    if (!m_loadedCopyables){
        // collect all branches that are *NOT* defined in our class 
        // these will be copied over unless vetoed below. 
        getMissedBranches(ttree);
        int nVetoed = 0;
        // determine which of our own branches we can copy from the input
        for (auto & branch : m_vecBranches){
            std::string bname = branch->getName();
            if (std::find(m_skipBranchList.begin(), m_skipBranchList.end(), bname) != m_skipBranchList.end()){
                // std::cout << "Note: Copying of "<<bname<<" manually suppressed "<<std::endl;
                nVetoed++;
                continue;
            }
            if (tree.getBranch(bname) != 0 && tree.getTree() != 0  && tree.getTree()->GetBranch(bname.c_str()) != 0){
                // point the auto-detected branches to our output tree
                branch->setTree(m_tree);
                m_branchesToCopy.insert(std::make_pair(bname, branch));
                tree.getTree()->GetBranch(bname.c_str())->LoadBaskets();
            }
        }
        m_loadedCopyables=true;
    }
    // now we direct all our to-copy branches to read from their friends in the input tree   
    for (auto & thing : m_branchesToCopy){
        thing.second->set(tree, thing.first);
    }
}

void NtupleBranchMgr::setNBootstrapWeights(size_t N){
    m_n_bootstrap = N;
    if (m_bootstrapWeights.size()!=0) m_bootstrapWeights.clear();
    m_bootstrapWeights.resize(N);
}
void NtupleBranchMgr::generateBootstrapWeights(){
    m_bootstrapHelper.populateRandom(m_bootstrapWeights);
}
double NtupleBranchMgr::getBootstrapWeight(size_t i){
    if (i >= m_n_bootstrap){
        std::cerr << "you asked for an invalid bootstrap weight "<<i<<" - have "<<m_n_bootstrap<<" stored weights "<<std::endl;
        return 0;
    }
    return m_bootstrapWeights[i];
}

void NtupleBranchMgr::GenerateManagerClass(const std::string & className){

    if (!m_tree) return;
    std::string theClass = (className.size()==0 ? m_tree->GetName() : className);

    std::ofstream Header(theClass+".h");

    std::string classForIncGuard = theClass;
    std::transform(theClass.begin(), theClass.end(),classForIncGuard.begin(), ::toupper);
    std::string inclueguard = "NTAU__"+classForIncGuard+"__H";
    // Step 1: Include guards etc 
    std::string tab = "    ";
    Header << "#ifndef "<<inclueguard<<std::endl;
    Header << "#define "<<inclueguard<<std::endl;  
    auto t = time(0);
    Header <<std::endl;
    Header << "/// Autogenerated code by NtupleBranchMgr on "<<ctime(&t) <<std::endl;
    Header <<std::endl;
    Header << "#include \"NtupleAnalysisUtils/NTAUTopLevelIncludes.h\""<<std::endl;
    Header << "#include <vector>"<<std::endl;
    Header << "#include \"TTree.h\""<<std::endl;
    Header <<std::endl;
    Header << "class "<<theClass<<": public NtupleBranchMgr{"<<std::endl; 
    Header << tab<<"public:"<<std::endl;
    Header << tab<<tab<<theClass<<"(TTree* t): NtupleBranchMgr(t){"<<std::endl;
    Header << tab << tab << tab << "if(t) getMissedBranches(t);"<<std::endl;
    Header << tab << tab << "}" << std::endl;
    Header << tab<<" /// List of branch members" <<std::endl;

    // Dump the branches 
    for (auto branch : m_vecBranches){
        Header << tab << tab << branch->generateMemberString()<<branch->generateInitString()<<";" <<std::endl;
    }
    Header << "};"<<std::endl;

    // Finally: Close ifdef 
    Header << "#endif // "<<inclueguard<<std::endl;
    Header.close();

}
