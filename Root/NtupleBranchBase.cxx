#include "NtupleAnalysisUtils/Ntuple/NtupleBranchBase.h"
#include "NtupleAnalysisUtils/Ntuple/NtupleBranch.h"
#include "NtupleAnalysisUtils/Ntuple/NtupleBranchMgr.h"
#include <thread>
namespace {
   constexpr unsigned int MBtoByte = 1024*1024;
}

Long64_t NtupleBranchBase::s_treeCacheSize = 50.*MBtoByte;
Int_t NtupleBranchBase::s_write_basketsize = 3.2*MBtoByte;
 
/// setters for the cache and write basket
void NtupleBranchBase::setTreeCacheSize(float size_in_MB){
    s_treeCacheSize = size_in_MB * MBtoByte;
}
void NtupleBranchBase::setBasketSize(float size_in_MB){
    s_write_basketsize = size_in_MB * MBtoByte;
}
    
/// constructor
NtupleBranchBase::NtupleBranchBase(const std::string & branchName, 
                                   TTree* tree, 
                                   NtupleBranchMgr* mgr):
    m_branchName(branchName),
    m_tree(tree){
    /// if we have a manager instance,
    /// we attach ourselves to it
    if (mgr){
        m_mgr = mgr;
        m_isManaged = true;
        mgr->attach(this);
    }
}

/// set ourselves up to copy a branch from an existing tree in write mode
void NtupleBranchBase::set (NtupleBranchMgr & in, const std::string & name_to_copy){
    /// First case: The local variable for the branch to copy
    /// already points to the correct manager  
    /// TODO: Is this really correct??? 
    if (m_copyFrom && m_copyFrom->getTree() == in.getTree()){
        set(m_copyFrom);
    }
    else {
        m_copyFrom = in.getBranch(name_to_copy);
        set(m_copyFrom);
    }
}

void NtupleBranchBase::warnReadFailedConnect(){
    if (m_n_warnings++ <= m_n_maxWarnings){           
        std::cerr << " ================================================================== "<<std::endl;
        std::cerr << "Trying to access invalid tree branch "<<m_branchName<<std::endl; 
        std::cerr << "    Error happened in "<<( m_tree ? std::string(m_tree->GetName()) : " invalid tree ")<<" - will return dummy results "<<std::endl;
        std::cerr << " ================================================================== "<<std::endl;
    }
}

void NtupleBranchBase::warnWriteFailedConnect(){
    if (m_n_warnings++ <= m_n_maxWarnings){           
        std::cerr << " ================================================================== "<<std::endl;
        std::cerr << "Trying to write to unconnected tree branch "<<m_branchName<<" - content will not show up in output tree "<<std::endl;
        std::cerr << " ================================================================== "<<std::endl;
    }
}

void NtupleBranchBase::attemptConnect() {
    /// to even attempt a read connection, we need to be connected to a 
    /// valid tree
    if (!m_tree) {
        /// if we have no tree, complain  
        std::cerr<<"Invalid tree, can not connect branch "<<m_branchName<<std::endl;
        return;
    }
    /// get the TBranch from the tree
    m_branch = m_tree->GetBranch(m_branchName.c_str());
    /// this should exist - otherwise not readable 
    if (!m_branch) {
         std::cerr<<"Tree "<<m_tree->GetName()<<"has no branch "<<m_branchName<<std::endl;
         return;
    }
    /// activate the branch for reading 
    m_branch->SetStatus(1);
    /// add it to the tree's cache
    m_tree->AddBranchToCache(m_branch,kTRUE);
    /// connect our data member to it (defined in concrete implementations) 
    attachReadPayload();
    /// If we are managed, we tell the branch manager 
    /// to auto-switch entries from now on.
    /// This will subscribe us to "getEntry" calls received 
    /// by the manager. 
    if (m_isManaged) m_mgr->registerActive(this);
    /// Now load the current entry from the input branch to make sure 
    /// we are up to date 
    m_branch->GetEntry(getCurrEntry());
    /// and update our connection state
    m_connectionStatus = connectedRead; 
}

void NtupleBranchBase::attemptAttach(){
    /// we first need to be connected to a tree if we want
    /// to do any writing 
    if (!m_tree){
         std::cerr<<"Invalid tree, can not attach the new branch "<<m_branchName<<std::endl;
        return;
    }
    /// add our payload as a branch to the tree - 
    /// this is implemented in the concrete implementations
    m_branch = addPayloadAsBranch(); 
    /// if the branch was successfully created, update our status
    if (m_branch){
        m_connectionStatus = connectedWrite;
    }    
}


void NtupleBranchBase::disconnect(){
    /// if we have no tree... why would we disconnect? 
    if (!m_tree || !m_branch) return;   
    
    m_branch->SetStatus(0);
    m_tree->DropBranchFromCache(m_branch, kTRUE);
    
    /// update our connection status
    m_connectionStatus = disconnected;
    m_branch = nullptr;    
}

void NtupleBranchBase::detach(){
    /// disconnect
    disconnect();
    /// then also detach from the manager
    if (m_mgr) m_mgr->detachActive(this);
    /// and update our connection log
    m_attempted_connect = false;
}

void NtupleBranchBase::connectIfNeeded(){
    /// check "disconnected" here - we can also read a write-mode branch!
    if (m_connectionStatus != disconnected) {return; }
    /// if we didn't try yet, attempt to connect
    if(!m_attempted_connect) {
        attemptConnect();
        m_attempted_connect = true;
    }    
    /// if we are not connected now, warn if appropriate
    if (m_connectionStatus == disconnected) warnReadFailedConnect();
}

void NtupleBranchBase::attachIfNeeded(){
    /// for write-mode attaching, we need the connectedWrite state
    if (!isConnectedWrite() && !m_attempted_attach){
        /// try to attach
        attemptAttach();
        m_attempted_attach = true;
    }
    /// complain if it failed
    if (!isConnectedWrite()) warnWriteFailedConnect();
}

Long64_t NtupleBranchBase::getCurrEntry() const { 
    /// if we are in managed-mode, get the entry from the manager
    return  m_isManaged ? m_mgr->getCurrEntry() : m_currEntry;
}

TTree* NtupleBranchBase::getTree() const {
    return m_tree;
}
void NtupleBranchBase::setTree(TTree* t){
    m_tree = t;
}
bool NtupleBranchBase::existsInTree() {
    if (!m_exists_in_tree.has_value()){
        m_exists_in_tree = m_tree && 
            /// condition b): a branch of the desired name also exists
            m_tree->GetBranch(m_branchName.c_str())!= nullptr;
    }
    return m_exists_in_tree.value(); 
}

std::string NtupleBranchBase::generateMemberString() const{
    std::stringstream ss; 
    ss << "NtupleBranchBase* "<<std::setw(30)<< std::left << ""<<m_branchName;
    return ss.str();
}
std::string NtupleBranchBase::generateInitString(unsigned int paddingWidth) const{
    std::stringstream ss; 
    ss << std::setw(paddingWidth)<< std::left 
       << "{\""+m_branchName+"\","<<"m_tree, this}";
    return ss.str();
}
double NtupleBranchBase::floatify(const std::string & ){
    return -9999999999.;
} 

// now we need to define a ton of strings for ROOT. 
#define SET_BRANCHSTRING(theType, thestring) template <> std::string NtupleBranchBase::branchString<theType>() { return thestring;} 

SET_BRANCHSTRING(Char_t, "/B")
SET_BRANCHSTRING(std::string, "/C")
SET_BRANCHSTRING(UChar_t, "/b")
SET_BRANCHSTRING(Short_t, "/S")
SET_BRANCHSTRING(Int_t, "/I")
SET_BRANCHSTRING(Long64_t, "/L")
SET_BRANCHSTRING(long, "/I")
SET_BRANCHSTRING(UShort_t, "/s")
SET_BRANCHSTRING(UInt_t, "/i")
SET_BRANCHSTRING(ULong64_t, "/l")
SET_BRANCHSTRING(Float_t, "/F")
SET_BRANCHSTRING(Double_t, "/D")
SET_BRANCHSTRING(Bool_t, "/o")

// now we need to define a ton of strings for ROOT. 
#define SET_MEMBERTYPESTRING(theType) \
     template <> std::string NtupleBranchBase::memberTypeString<theType>(){ return #theType;} \
     template <> std::string NtupleBranchBase::memberTypeString<std::vector<theType>>() {return Form("std::vector<%s>",memberTypeString<theType>().c_str());}

SET_MEMBERTYPESTRING(Char_t)
SET_MEMBERTYPESTRING(std::string)
SET_MEMBERTYPESTRING(UChar_t)
SET_MEMBERTYPESTRING(Short_t)
SET_MEMBERTYPESTRING(Int_t)
SET_MEMBERTYPESTRING(Long64_t)
SET_MEMBERTYPESTRING(long)
SET_MEMBERTYPESTRING(UShort_t)
SET_MEMBERTYPESTRING(UInt_t)
SET_MEMBERTYPESTRING(ULong64_t)
SET_MEMBERTYPESTRING(Float_t)
SET_MEMBERTYPESTRING(Double_t)
SET_MEMBERTYPESTRING(Bool_t)

// and dummy values for every branch type 
#define SET_DUMMYVAL(theType, theVal) \
    template<> theType NtupleBranchBase::dummyVal<theType> () { return theVal;}\
    template<> std::vector<theType> NtupleBranchBase::dummyVal<std::vector<theType>> () { return std::vector<theType>{};}

SET_DUMMYVAL(char*, const_cast<char*>(""))
SET_DUMMYVAL(std::string, "")
SET_DUMMYVAL(Char_t, 0)
SET_DUMMYVAL(UChar_t,0)
SET_DUMMYVAL(Short_t,0)
SET_DUMMYVAL(Int_t,0)
SET_DUMMYVAL(Long64_t,0)
SET_DUMMYVAL(long,0)
SET_DUMMYVAL(UShort_t,0)
SET_DUMMYVAL(UInt_t,0)
SET_DUMMYVAL(ULong64_t,0)
SET_DUMMYVAL(Float_t,0)
SET_DUMMYVAL(Double_t,0)
SET_DUMMYVAL(Bool_t, false)
