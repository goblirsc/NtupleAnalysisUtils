/*
 * CutFlow.cxx
 *
 *  Created on: Nov 3, 2015
 *      Author: goblirsc
 */

#include "NtupleAnalysisUtils/HistoFiller/CutFlow.h"
#include <iostream>
#include <iomanip>
#include "TGraphAsymmErrors.h"
#include "TFile.h"

CutFlow::CutFlow(std::vector<std::string>  ordered_cuts):
    m_learn_mode(false),
    m_ordered_cuts(ordered_cuts){
    if (ordered_cuts.size() == 0){
        m_learn_mode = true;
    }
    else {
        m_learn_mode = false;
        for (auto & stage : ordered_cuts){
            m_at_step[stage] = std::make_pair(0.,0.);
        }
    }
}
CutFlow::CutFlow(const std::string & fname, const std::string & histoname, int first, int last):
    m_learn_mode(true){
    Import(fname,histoname,first,last);
}

void CutFlow::Fill(const std::string & cut, Double_t weight){
    std::map<std::string, ValWithSw2>::iterator found = m_at_step.find(cut);
    if (found == m_at_step.end()){
        if (m_learn_mode){
            m_ordered_cuts.push_back(cut);
            m_at_step[cut] = std::make_pair(0.,0.);
            found = m_at_step.find(cut);
        }
        else {
            std::cerr << "Trying to fill cut flow for stage "<<cut<<", which is not part of this cut flow!"<<std::endl;
            return;
        }
    }
    found->second.first+= weight;
    found->second.second+= weight*weight;
}

void CutFlow::Print(){
    int columnWidth = 15;
    int titleWidth  = 40;
    std::cout << std::setw(titleWidth)  << "CUT"
            << std::setw(columnWidth) << "ALL"
            << std::setw(2*columnWidth+5) << "REL %"
            << std::setw(2*columnWidth+5) << "ABS %"
            << std::endl;
    if (m_ordered_cuts.size() == 0) return;
    // here, we use the *SECOND* bin as a reference: We assume the first contains the nb of processed events and the second the number of weighted processed events
    std::string firstbin = m_ordered_cuts[1];
    ValWithSw2 first = m_at_step[firstbin];

    ValWithSw2 previous = first;
    for (auto & stage : m_ordered_cuts){

        ValWithSw2 current = m_at_step[stage];
        std::pair<Double_t,Double_t> rel_eff = Eff(current, previous);
        std::pair<Double_t,Double_t> abs_eff = Eff(current, first);

        std::cout << std::setw(titleWidth)  << stage
                  << std::setprecision(9)
                  << std::setw(columnWidth) << current.first << std::setw(5) << " \u00B1 "<< std::setw(columnWidth) << sqrt(current.second)
                  << std::setprecision(4)
                  << std::setw(columnWidth) << rel_eff.first*100. << std::setw(5) << " \u00B1 "<< std::setw(columnWidth) << rel_eff.second*100.
                  << std::setw(columnWidth) << abs_eff.first*100. << std::setw(5) << " \u00B1 "<< std::setw(columnWidth) << abs_eff.second*100.
                  << std::endl;

    // Update cache of previous bin
        previous = current;
    }
    std::cout << std::endl;

}

void CutFlow::Import(const std::string & fname, const std::string & histoname, int first, int last ){
    // import the histogram from the given file 
    TFile* fin = TFile::Open(fname.c_str(), "READ");
    if (fin){
        TH1F* hin = dynamic_cast<TH1F*>(fin->Get(histoname.c_str()));
        if (hin){
            Import(hin, first, last);
        }
        else{
            std::cerr << "Failed to read the histogram "<<histoname<<" from file "<<fname<<std::endl;
        }
        fin->Close();
    }
    else { 
        std::cerr << "Failed to open "<<fname<<std::endl;
    }
}
void CutFlow::Import(TH1F* theHist, int first, int last){  
    std::vector<std::string> aux_existing = m_ordered_cuts;
    m_ordered_cuts.clear();
    for (int k = first; (last < 0 || k <= last) && k <= theHist->GetNbinsX(); ++k){
        std::string xname = theHist->GetXaxis()->GetBinLabel(k);
        std::map<std::string, ValWithSw2>::iterator found = m_at_step.find(xname);
        if (found == m_at_step.end()){
            m_ordered_cuts.push_back(xname);
            m_at_step[xname] = std::make_pair(0,0);
            found = m_at_step.find(xname);
        }
        found->second.first+= theHist->GetBinContent(k);
        found->second.second+= theHist->GetBinError(k)*theHist->GetBinError(k);
    }
    m_ordered_cuts.insert(m_ordered_cuts.end(), aux_existing.begin(), aux_existing.end());
}


void CutFlow::WriteLatex(const std::string & ){
    std::cerr <<"Careful, CutFlow::WriteLatex is still to be implemented."<<std::endl;
    // TODO

}


std::pair<Double_t,Double_t> CutFlow::Eff (CutFlow::ValWithSw2 numerator, CutFlow::ValWithSw2 denominator){


	if (denominator.first == 0){
		return std::make_pair(0,0);
	}
	else {
		Double_t eff = numerator.first/denominator.first;
		Double_t sw2_rej = denominator.second - numerator.second;
		Double_t deff =   sqrt ( (numerator.first*numerator.first*sw2_rej +
					   numerator.second *(denominator.first-numerator.first)*(denominator.first-numerator.first))
						/ (denominator.first*denominator.first*denominator.first*denominator.first));
		return std::make_pair(eff,deff);
	}
}

TH1D* CutFlow::CutFlowHist(const std::string & name){

    TH1D* out = new TH1D(name.c_str(), name.c_str(), m_ordered_cuts.size(), 0.5,0.5+m_ordered_cuts.size());
    out->Sumw2();
    for (size_t k = 0; k < m_ordered_cuts.size();++k){
        std::string bin = m_ordered_cuts[k];
        out->GetXaxis()->SetBinLabel(k+1, bin.c_str());
        out->SetBinContent(k+1, m_at_step[bin].first);
        out->SetBinError(k+1, sqrt(m_at_step[bin].second));
    }
    return out;

}

TH1D* CutFlow::RelEffHist(const std::string & name){

    TH1D* out = new TH1D(name.c_str(), name.c_str(), m_ordered_cuts.size(), 0.5,0.5+m_ordered_cuts.size());
    out->Sumw2();
    ValWithSw2 previous = m_at_step[m_ordered_cuts[0]];
    for (size_t k = 0; k < m_ordered_cuts.size();++k){
        std::string bin = m_ordered_cuts[k];
        ValWithSw2 current = m_at_step[bin];
        std::pair<Double_t,Double_t> result = Eff(current,previous);
        previous = current;
        out->GetXaxis()->SetBinLabel(k+1, bin.c_str());
        out->SetBinContent(k+1, result.first);
        out->SetBinError(k+1, result.second);
    }
    return out;

}

TH1D* CutFlow::AbsEffHist(const std::string & name){

    TH1D* out = new TH1D(name.c_str(), name.c_str(), m_ordered_cuts.size(), 0.5,0.5+m_ordered_cuts.size());
    out->Sumw2();
    // here, we use the *SECOND* bin as a reference: We assume the first contains the nb of processed events and the second the number of weighted processed events
    ValWithSw2 first = m_at_step[m_ordered_cuts[1]];
    for (size_t k = 0; k < m_ordered_cuts.size();++k){
        std::string bin = m_ordered_cuts[k];
        ValWithSw2 current = m_at_step[m_ordered_cuts[k]];
        std::pair<Double_t,Double_t> result = Eff(current,first);
        out->GetXaxis()->SetBinLabel(k+1, bin.c_str());
        out->SetBinContent(k+1, result.first);
        out->SetBinError(k+1, result.second);
    }
    return out;

}
