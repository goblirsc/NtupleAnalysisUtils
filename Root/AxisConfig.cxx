#include "NtupleAnalysisUtils/Configuration/AxisConfig.h"

AxisConfig::AxisConfig(const AxisConfig & other): 
    Min(other.Min,this),
    MinBelow(other.MinBelow,this),
    Max(other.Max,this),
    MaxAbove(other.MaxAbove,this),
    Symmetric(other.Symmetric,this),
    SymmetrisationPoint(other.SymmetrisationPoint,this),
    Aligned(other.Aligned,this),
    Fixed(other.Fixed,this),
    Log(other.Log,this),
    TopPadding(other.TopPadding,this),
    BottomPadding(other.BottomPadding,this),
    Title(other.Title,this),
    ExtraTitleOffset(other.ExtraTitleOffset,this),
    m_parent(other.m_parent){

}

void AxisConfig::operator=(const AxisConfig& other){
    Min.copyFrom(other.Min,this); 
    MinBelow.copyFrom(other.MinBelow,this); 
    Max.copyFrom(other.Max,this); 
    MaxAbove.copyFrom(other.MaxAbove,this); 
    Symmetric.copyFrom(other.Symmetric,this); 
    SymmetrisationPoint.copyFrom(other.SymmetrisationPoint,this); 
    Aligned.copyFrom(other.Aligned,this); 
    Fixed.copyFrom(other.Fixed,this); 
    Log.copyFrom(other.Log,this); 
    TopPadding.copyFrom(other.TopPadding,this); 
    BottomPadding.copyFrom(other.BottomPadding,this); 
    Title.copyFrom(other.Title,this); 
    ExtraTitleOffset.copyFrom(other.ExtraTitleOffset,this);
    m_parent = other.m_parent;  
}
