#include "NtupleAnalysisUtils/Visualisation/MultiPadCanvas.h"
#include <iostream> 

MultiPadCanvas::MultiPadCanvas(TCanvas* c, const std::vector<TPad*>& pads): 
    m_can(c), 
    m_pads(){
    for (auto& p : pads) {
        m_pads.push_back(std::shared_ptr<TPad>(p));
    }
}
MultiPadCanvas::MultiPadCanvas(std::shared_ptr<TCanvas> c, const std::vector< std::shared_ptr<TPad>>& pads):
        m_can(c),
        m_pads(pads){
}
std::shared_ptr<TCanvas> MultiPadCanvas::getCanvas() const {
    return m_can;
} 
std::shared_ptr<TPad> MultiPadCanvas::getPad(size_t p) const {
    if (p < m_pads.size()) return m_pads.at(p); 
    std::cerr <<"Requested pad "<<p<<" out of range of multi pad canvas with "<<m_pads.size()<<" sub-pads" <<std::endl; 
    return std::shared_ptr<TPad>(); 
} 
