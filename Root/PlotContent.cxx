#include "NtupleAnalysisUtils/Visualisation/PlotContent.h" 


RatioEntry::RatioEntry(size_t num, 
                       size_t den, 
                       PlotUtils::EfficiencyMode errorStrat,
                       std::optional<PlotFormat> extraFormat):
        m_num(num),
        m_den(den),
        m_errorStrat(errorStrat),
        m_format(extraFormat){
}
size_t RatioEntry::getNumIndex(){
    return m_num;
}
size_t RatioEntry::getDenomIndex(){
    return m_den;
}
