#include  "NtupleAnalysisUtils/HistoFiller/ObjectID.h"
#include  <functional>


/// construct an identifier from an object name.
ObjectID::ObjectID(const std::string & name){
    m_theID = {static_cast<ID_t>(std::hash<std::string>{}(name))}; 
}
/// constructs an identifier as a fixed number
ObjectID::ObjectID(ID_t ID){
    m_theID = {ID}; 
}

/// generate a composite ID from several components
ObjectID::ObjectID(std::vector<ObjectID> ids){
    for (auto & ID : ids){
        m_theID.insert(m_theID.end(), ID.m_theID.begin(), ID.m_theID.end()); 
    }
}

bool ObjectID::operator<(const ObjectID & other) const{
    if (other.m_theID.size() != m_theID.size()){
        return m_theID.size() < other.m_theID.size();
    }
    else{
        for (size_t i = 0; i < m_theID.size(); ++i){
            if (m_theID[i] == other.m_theID[i]) continue; 
            return m_theID[i] < other.m_theID[i]; 
        }
    }
    return false; 
}

bool ObjectID::operator==(const ObjectID & other) const{
    if (other.m_theID.size() != m_theID.size()){
        return false;
    }
    else{
        for (size_t i = 0; i < m_theID.size(); ++i){
            if (m_theID[i] != other.m_theID[i]) return false; 
        }
    }
    return true; 
}
bool ObjectID::operator!=(const ObjectID & other) const {
    return !((*this) == other);
}

bool ObjectID::operator>(const ObjectID & other) const{
    if (other.m_theID.size() != m_theID.size()){
        return m_theID.size() > other.m_theID.size();
    }
    else{
        for (size_t i = 0; i < m_theID.size(); ++i){
            if (m_theID[i] == other.m_theID[i]) continue; 
            return m_theID[i] > other.m_theID[i]; 
        }
    }
    return false; 
}
ObjectID ObjectID::nextID(){
    static std::atomic<ID_t> g_defaultID{0}; 
    return ObjectID(g_defaultID++); 
}
const std::vector<ObjectID::ID_t> & ObjectID::allIDs() const{
    return m_theID;
}

std::ostream & operator<< (std::ostream& stream, const ObjectID & theID){
    stream << "[ "; 
    for (auto & theItem : theID.allIDs()){
        stream << theItem<<" ";
    }
    stream << "]";
    return stream;
}
