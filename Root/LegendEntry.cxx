#include "NtupleAnalysisUtils/Visualisation/LegendEntry.h"

LegendEntry::LegendEntry(TObject* o, const std::string & legtitle , const std::string & opt){
    obj = o->Clone(Form("CloneForTheLegend_%s",o->GetName()));
    title = legtitle; 
    drawOpt = opt;
}        