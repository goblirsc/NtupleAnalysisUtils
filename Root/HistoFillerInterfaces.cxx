#include "NtupleAnalysisUtils/HistoFiller/HistoFillerInterfaces.h"

/// assign a unique starting ID 
ISortableByID::ISortableByID(){
}

/// the comparison delegates to the comparison between the IDs 
bool ISortableByID::lessForSharedPtr::operator()(
                const std::shared_ptr<ISortableByID> & s1, 
                const std::shared_ptr<ISortableByID> & s2) const{
    return s1->getID() < s2->getID();
}

ObjectID ISortableByID::getID() const{
    return m_ID;
}

void ISortableByID::setID(ObjectID id){
    m_ID = id;
} 

void ISortableByID::setID(const std::string & id){
    m_ID = ObjectID{id};
} 

