//
// ATLAS Style, based on a style file from BaBar
//

#include <iostream>

#include "NtupleAnalysisUtils/Visualisation/AtlasStyle.h"

#include "TROOT.h"

void SetAtlasStyle ()
{
  gROOT->SetStyle("ATLAS"); 
  // TStyle* atlasStyle = nullptr;
  // std::cout << "\nApplying ATLAS style settings...\n" << std::endl ;
  // if ( atlasStyle==0 ) atlasStyle = AtlasStyle();
  // gROOT->SetStyle("ATLAS");
  // gROOT->ForceStyle();
}
