#include "NtupleAnalysisUtils/Helpers/HistoEvaluation.h"
#include "NtupleAnalysisUtils/Helpers/HistoManipulation.h" 
#include "NtupleAnalysisUtils/Plot/Plot.h"
#include "NtupleAnalysisUtils/Plot/BasicPopulators.h"

using namespace PlotUtils; 


PlotUtils::rawAxisRange PlotUtils::getRawRange (const TH1* p, size_t axis,const AxisConfig & axisCfg){
    if (!p) return rawAxisRange{}; 
    size_t dimensionality = p->GetDimension(); 
    double thMin = axisCfg.Min();
    double thMax = axisCfg.Max();
    if (axis < dimensionality){
        return rawAxisRange  {std::min(axisCfg.MinBelow(), std::max(thMin, getAxis(p,axis)->GetXmin())), 
                              std::max(axisCfg.MaxAbove(), std::min(thMax, getAxis(p,axis)->GetXmax())),
                              true
        }; 
    }
    else{
        /// appropriate thresholds for log axes
        if (axisCfg.Log()){
            if (thMin < 0) thMin = 0; 
            if (thMax < 0) thMax = 0; 
        } 
        double Max = axisCfg.MaxAbove();
        double Min = axisCfg.MinBelow();
        // do a stupid min/max lookup to avoid falling prey to empty bins
        for (int ib = 1; ib <= p->GetNcells(); ++ib){
            double y = p->GetBinContent(ib); 
            if (y == 0 && p->GetBinError(ib) == 0) continue; 
            if (p->IsBinOverflow(ib) || p->IsBinUnderflow(ib)) continue;
            if (y < thMax){
                 Max = std::max(Max,y);
            } 
            if (y > thMin) {
                Min = std::min(Min,y);
            } 
        }
        // double Max = std::max(axisCfg.MaxAbove(), p->GetMaximum(thMax)); 
        // double Min = std::min(axisCfg.MinBelow(), p->GetMinimum(thMin)); 
        return rawAxisRange{Min,Max,false}; 
    }
}

#define RAWAXIS_FOR_GRAPHLIKE(type)\
rawAxisRange PlotUtils::getRawRange (const type* p, size_t axis,const AxisConfig & axisCfg){ \
    double* cont = nullptr; \
    if (p) cont = (axis == 0 ? p->GetX() : p->GetY()); \
    return getRawRange(cont, p->GetN(), axisCfg); \
};

RAWAXIS_FOR_GRAPHLIKE(TGraph);
RAWAXIS_FOR_GRAPHLIKE(TGraphErrors);
RAWAXIS_FOR_GRAPHLIKE(TGraphAsymmErrors);


PlotUtils::rawAxisRange PlotUtils::getRawRange (const Double_t* cont, size_t arrSize, const AxisConfig & axisCfg){
    if (!cont || !arrSize){
        // for empty graphs: return something that will be ignored when combining with other graphs
        return rawAxisRange{std::numeric_limits<double>::max(), -std::numeric_limits<double>::max(),false}; 
    }
    std::vector<double> elements;
    elements.insert(elements.begin(), cont, cont + arrSize); 
    double thMin = axisCfg.Min();
    double thMax = axisCfg.Max();
    if (axisCfg.Log()){
        if (thMin < 0) thMin = 0; 
        if (thMax < 0) thMax = 0; 
    }
    if (!elements.size()) return rawAxisRange{0,1,false};
    std::sort(elements.begin(),elements.end()); 
    auto lower = std::upper_bound(elements.begin(),elements.end(),thMin);
    auto upper = std::lower_bound(elements.begin(),elements.end(),thMax);
    if (lower == elements.end()) --lower; 
    if (upper == elements.end()) --upper; 
    return rawAxisRange{std::min(axisCfg.MinBelow(),*lower), std::max(axisCfg.MaxAbove(),*upper),false}; 
}

PlotUtils::rawAxisRange PlotUtils::getRawRange (const TEfficiency* p, size_t axis,const AxisConfig & axisCfg){
    return getRawRange(
        PlotUtils::extractEfficiency(p).get(), 
        axis, 
        axisCfg
    );
}

std::pair<double,double> PlotUtils::updateAxisRange(PlotUtils::rawAxisRange histo_range, const AxisConfig & axisConf){
    double x1 = histo_range.min;
    double x2 = histo_range.max;
    
    /// if the range was obtained from a histo binning axis, we do not apply any further modifiers 
    if (histo_range.fromBinning){
        return std::make_pair(x1,x2);
    }
    double xSymm = axisConf.SymmetrisationPoint();

    /// now, start checking modifiers
    if (axisConf.Log()){
        x1 = std::log(x1); 
        x2 = std::log(x2);
        xSymm = std::log(xSymm); 
    }
    /// first, apply possible symmetrisation 
    if (axisConf.Symmetric()){
        xSymm = axisConf.SymmetrisationPoint();
        double dx1 = x1 - xSymm;
        double dx2 = x2 - xSymm;
        double dx = std::max(std::abs(dx1),std::abs(dx2)); 
        x1 = xSymm - dx;
        x2 = xSymm + dx; 
    }
    /// now, we add a possibly padding on top and below
    double dynRange = x2 - x1; 
    x1 -= axisConf.BottomPadding() * dynRange; 
    x2 += axisConf.TopPadding() * dynRange; 

    /// having applied all modifiers, we can go back to linear values 
    if (axisConf.Log()){
        x1 = std::exp(x1); 
        x2 = std::exp(x2); 
    }

    /// TODO: Implement snapping to pre-chosen values via "Fixed" option!

    /// overrride by user-specified settings if so requested
    if (axisConf.Fixed() && axisConf.Min.isUserSet()) x1 = axisConf.Min();  
    if (axisConf.Fixed() && axisConf.Max.isUserSet()) x2 = axisConf.Max();  
    return std::make_pair(x1,x2);

}

/// map access getters to template specialisations

#define TEMPLATE_SPEC_IMPL_WITHAXIS(theMethod, theThing, itsmethod) \
const TAxis* PlotUtils::theMethod(const theThing* p){return p->itsmethod();} 
#define TEMPLATE_SPEC_IMPL_WITHAXIS_GRAPHS(theMethod, theThing, itsmethod) \
const TAxis* PlotUtils::theMethod(const theThing* p){if (!p->GetHistogram()) return 0; return p->itsmethod();} 

TEMPLATE_SPEC_IMPL_WITHAXIS(getXaxis, TH1, GetXaxis); 
TEMPLATE_SPEC_IMPL_WITHAXIS(getYaxis, TH1, GetZaxis); 
TEMPLATE_SPEC_IMPL_WITHAXIS(getZaxis, TH1, GetZaxis); 

// TEMPLATE_SPEC_IMPL_WITHAXIS_GRAPHS(getXaxis, TGraph, GetXaxis); 
// TEMPLATE_SPEC_IMPL_WITHAXIS_GRAPHS(getYaxis, TGraph, GetYaxis); 

// TEMPLATE_SPEC_IMPL_WITHAXIS_GRAPHS(getXaxis, TGraphErrors, GetXaxis); 
// TEMPLATE_SPEC_IMPL_WITHAXIS_GRAPHS(getYaxis, TGraphErrors, GetYaxis); 

// TEMPLATE_SPEC_IMPL_WITHAXIS_GRAPHS(getXaxis, TGraphAsymmErrors, GetXaxis); 
// TEMPLATE_SPEC_IMPL_WITHAXIS_GRAPHS(getYaxis, TGraphAsymmErrors, GetYaxis); 

TEMPLATE_SPEC_IMPL_WITHAXIS(getXaxis, TEfficiency, GetPassedHistogram()->GetXaxis); 
TEMPLATE_SPEC_IMPL_WITHAXIS(getYaxis, TEfficiency, GetPassedHistogram()->GetYaxis); 

