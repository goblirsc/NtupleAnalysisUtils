#include "NtupleAnalysisUtils/Ntuple/NtupleBranchAccessByName.h" 

NtupleBranchAccessByName_asDouble::NtupleBranchAccessByName_asDouble(const std::string & branchName){
        m_branchName = branchName;
}

NtupleBranchAccessByName_asDouble::NtupleBranchAccessByName_asDouble(const NtupleBranchAccessByName_asDouble & other){
        m_branchName = other.m_branchName;
        m_branchMap = other.m_branchMap;
}

NtupleBranchAccessByName_asDouble & NtupleBranchAccessByName_asDouble::operator= (const NtupleBranchAccessByName_asDouble & other){
        m_branchName = other.m_branchName;
        m_branchMap = other.m_branchMap;
        return *this;
}

NtupleBranchAccessByName_asDouble::NtupleBranchAccessByName_asDouble(){
        /// set to a dummy name that will hopefully NEVER
        /// exist as a real branch! 
        m_branchName = "UninitializedNtupleBranchAccessByName_asDouble";   
}

void NtupleBranchAccessByName_asDouble::setBranchName(const std::string & name){
        /// set the target branch name
        m_branchName = name; 
        /// and reset our cache
        m_branchMap.clear();
}

std::string NtupleBranchAccessByName_asDouble::getBranchName(){
        return m_branchName;
}

NtupleBranchBase* NtupleBranchAccessByName_asDouble::getBranch(NtupleBranchMgr & t){
        /// we need to guard the map interaction with a mutex
        // std::lock_guard<std::mutex> guard(m_mx_loadSafe);
        /// find the file in our cache 
        auto found = m_branchMap.find(t.id());
        if (found == m_branchMap.end()){
            std::lock_guard<std::mutex> guard(m_mx_loadSafe);
            /// cache miss -> add to cache
            found = addBranch(t);
        }
        /// return the cache entry 
        return found->second; 
}

bool NtupleBranchAccessByName_asDouble::getVal (NtupleBranchMgr &t, double & ret, size_t i){   
        /// need another mutex to guard this 
        /// try to get the branch 
        auto gotIt = getBranch(t);
        /// failed to get it -> return false
        if (!gotIt) return false; 
        if (i >= gotIt->size()) return false; 
        /// otherwise retrieve the content and overwrite the ref 
        ret = gotIt->asDouble(i);
        /// and return true for success
        return true; 
}
/// legacy method - map to new signature
bool NtupleBranchAccessByName_asDouble::getVal (NtupleBranchMgr &t, size_t i, double & ret){   
        return getVal(t,ret,i); 
}

bool NtupleBranchAccessByName_asDouble::getSize (NtupleBranchMgr &t, size_t & ret){   
        /// need another mutex to guard this 
        // std::lock_guard<std::mutex> guardGetVar(m_mx_getVarSafe);
        /// try to get the branch 
        auto gotIt = getBranch(t);
        /// failed to get it -> return false
        if (!gotIt) return false; 
        /// otherwise retrieve the size and overwrite the ref 
        ret = gotIt->size();
        /// and return true for success
        return true; 
}
std::optional<double> NtupleBranchAccessByName_asDouble::operator()(NtupleBranchMgr &t, size_t i){
    std::optional<double> ret; 
    double d = 0; 
    if (getVal(t,d,i)) ret = d; 
    return ret; 
}

typename std::map<ObjectID, NtupleBranchBase*>::iterator NtupleBranchAccessByName_asDouble::addBranch(NtupleBranchMgr & t){
        /// try to get the branch 
        auto theBranch = t.getBranch(m_branchName);
        /// if we failed to get it, complain and cache the result for this file 
        if (!theBranch){
            std::cerr <<" Failed to find a valid branch for "<<m_branchName<<std::endl;
            return m_branchMap.emplace(t.id(), nullptr).first;
        }
        /// otherwise we cache the successfully found branch
        else {
            return m_branchMap.emplace(t.id(), theBranch).first;
        }
}
